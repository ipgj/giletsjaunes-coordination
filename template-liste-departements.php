<?php
/*
Template Name: Liste départements
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container">

		<div class="subnav">Portail national d'information, de discussion, de débat, et de ressources à l'usage des Gilets Jaunes.</div>

		<main class="clearfix">

			<section class="entete_page">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content-page', 'loginpage' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

			</section>

			<div class="francemap_container">
				<div class="francemap_title">SECTIONS DÉPARTEMENTALES - LISTE TEXTUELLE</div>
				<div class="liste_departements">
					<?php
					if(have_rows('liens_departements_forums', 'options')) : 
						while(have_rows('liens_departements_forums', 'options')) : the_row(); 

							$page_dep = get_sub_field('page_principale_departementale');
							$titre = $page_dep->post_title;
							$lien = $page_dep->guid;
							?>

							<a href="<?= $lien ?>" class="ligne">
								<h4><?= $titre ?></h4>
							</a><br/>

						<?php endwhile; 
					endif;
					?>
				</div>
				<div class="francemap_footer">
					<div class="phrase">Retour à la carte de France.</div>
					<a href="/#carte-departements" class="bouton_type_1">Page d'accueil</a>
				</div>
			</div>
		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




