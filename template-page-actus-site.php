<?php
/*
Template Name: Page - Actus site
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page">
		<main class="clearfix nopaddingtop">

			<div class="fullsize_head_container">
				<section class="entete_page">
					<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content-page', 'content-page' );
					endwhile; // End of the loop.
					?>
				</section>

				<section class="entete_content">
					<div class="icon"><?= get_field('fullsize_header_icon') ?></div>
					<div class="separateur_vert"></div>
					<div class="texte">
						<?= get_field('fullsize_header_text') ?>
					</div>
				</section>
			</div>

			<section class="liste_actus_site">

                <?php
                $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                $item_per_page = 10;
                $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => $item_per_page,
                    'order'          => 'DESC',
                    'orderby'        => 'date',
                    'paged' => $paged,
                 );

                $news = new WP_Query( $args );

                if ( $news->have_posts() ) :
                    while ( $news->have_posts() ) : $news->the_post();

                        $img = get_the_post_thumbnail();
                        if(!$img OR $img == '') { $img = '<img src="'.get_template_directory_uri().'/images/no_image.jpg"/>'; }
                        $date = get_the_date(get_option('date_format'));
                        $text_unformed = get_the_content();
                        $text = wpautop($text_unformed);

                        $page_link_ID = get_the_ID(); 
                        $current = '';
                        if($page_link_ID == $current_page_ID) { $current = 'active'; } ?>
                        <div class="news_container row no-gutters">
                            <div class="gauche col-lg-3">
                                <?php echo $img; ?>
                            </div>
                            <div class="droite col-lg-9">
                                <a class="titre <?php echo $current; ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
                                <div class="date"><?php echo $date; ?></div>
                                <div class="texte">
                                    <?php echo $text; ?>
                                </div>
                            </div>
                        </div>

                        <div class="separateur"></div>
                        
                    <?php endwhile;
                else :
                    echo '<p class="bloc no_result">Aucune actualité trouvée...</p>';
                endif;

                wp_reset_postdata();
                ?>

            </section>

            <?php $big = 999999999;
            echo '<section class="wp_pagination">';
                echo paginate_links( array( // Plus d'info sur les arguments possibles : https://codex.wordpress.org/Function_Reference/paginate_links
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $news->max_num_pages
                ) );
            echo '</section>'; ?>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




