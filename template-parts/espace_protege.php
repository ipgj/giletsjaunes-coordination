<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gilet_Jaune_France
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', ' <i class="fas fa-lock"></i></h1>' ); ?>
	</header><!-- .entry-header -->

	<?php gilet_jaune_france_post_thumbnail(); ?>

	

	<?php if ( post_password_required() ) { ?>
		<div class="entry-content form_espace_protege_container">
			<div class="locked_container">
				<i class="fas fa-lock"></i>
			</div>
			<div class="content">
				<?php
				$the_content = get_the_content();
				if($the_content != '') {
					the_content();
				} else {
					echo '<i>Aucune information n\'a été épinglée sur cette page par les modérateurs pour l\'instant.</i>';
				}
				?>
			</div>
		</div><!-- .entry-content -->
	<?php } ?>

</article><!-- #post-<?php the_ID(); ?> -->
