<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gilet_Jaune_France
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content pinned_info">
		<div class="pin_container">
			<img  class="pin" src="<?php echo get_template_directory_uri(); ?>/images/pin_icon.png"/>
		</div>
		<div class="content">
			<div class="img_container">
				<?php gilet_jaune_france_post_thumbnail(); ?>
			</div>
			<div class="texte_container">
				<?php
				$the_content = get_the_content();
				if($the_content != '') {
					the_content();
				} else {
					echo '<i>Aucune information n\'a été épinglée sur cette page par les modérateurs pour l\'instant.</i>';
				}
				

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gilet-jaune-france' ),
					'after'  => '</div>',
				) );
				?>
			</div>
		</div>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			/*edit_post_link(
				sprintf(
					wp_kses(
						__( 'Edit <span class="screen-reader-text">%s</span>', 'gilet-jaune-france' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);*/
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
