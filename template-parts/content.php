<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gilet_Jaune_France
 */

?>

<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header entete_page">
			<?php
			$post_type = get_post_type();

			if($post_type != 'page') {
				$post_type_object = get_post_type_object(get_post_type());
				$post_type_name = $post_type_object->labels->singular_name;
				echo '<div class="departement">'.$post_type_name.'</div>';
			}

			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
			endif;

			if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php
					gilet_jaune_france_posted_on();
					gilet_jaune_france_posted_by();
					?>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="post_columns row">

			<div class="gauche col-lg-5">
				<?php gilet_jaune_france_post_thumbnail(); 
				if(!has_post_thumbnail()) { echo '<img src="'.get_template_directory_uri().'/images/no_image.jpg"/>'; }?>
			</div>

			<div class="droite col-lg-7">
				<div class="entry-content">
					<?php
					the_content( sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'gilet-jaune-france' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gilet-jaune-france' ),
						'after'  => '</div>',
					) );
					?>
				</div><!-- .entry-content -->
			</div>

		</div>

		
	</article><!-- #post-<?php the_ID(); ?> -->
</div>