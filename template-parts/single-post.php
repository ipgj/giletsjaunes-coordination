<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gilet_Jaune_France
 */

?>

<?php if ( !post_password_required() ) { ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				gilet_jaune_france_posted_on();
				gilet_jaune_france_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php gilet_jaune_france_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'gilet-jaune-france' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'gilet-jaune-france' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php gilet_jaune_france_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->

<?php } else { ?>
	<main class="forbidden_access">
		<section>
			<i class="fas fa-lock"></i>
			<h1>Accès à ce contenu privé interdit</h1>
			<p>Vous essayez d'accéder à un contenu départemental protégé,<br/>
			veuillez valider le mot de passe départemental avant d'accéder<br/>
			à ce contenu depuis la page <a href="<?php echo get_permalink($password_protected_child_ID); ?>">Espace protégé</a>.</p>
		</section>
	</main>
<?php } ?>
