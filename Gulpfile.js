
var virtual_host = 'gj-c.local';
var gulp = require('gulp');  
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var minifycss = require('gulp-minify-css');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');
var size = require('gulp-size');
var headerComment = require('gulp-header-comment');
var order = require('gulp-order');
var paths = {
    scss: './sass/*.scss'
};


/* Sass task */
gulp.task('sass', function () {  
    gulp.src('sass/main.scss')

    .pipe(sass())
    .on('error', function(err) {
        return notify().write(err);
    })
    .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: false
    }))
    .pipe(minifycss())
    .pipe(size())
    .pipe(rename('style.css'))
    .pipe(headerComment(`
        Theme Name: Gilet Jaune France
        Author: Clem
        Author URI: https://giletjaune-france.fr/
        Description: Template wordpress développé spécifiquement pour le site Gilet Jaune France.
        Version: 1.0
        `))
    .pipe(gulp.dest('./'))
    
    /* Reload the browser CSS after every change */
    .pipe(browserSync.stream())
});

gulp.task('sass_admin', function () {  
    gulp.src('sass/admin/main.scss')

    .pipe(sass())
    .on('error', function(err) {
        return notify().write(err);
    })
    .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: false
    }))
    .pipe(minifycss())
    .pipe(size())
    .pipe(rename('style_backoffice.css'))
    .pipe(gulp.dest('./'))
    
    /* Reload the browser CSS after every change */
    .pipe(browserSync.stream())
});

gulp.task('js', function() {  
    return gulp.src('js/src/**/*.js')
        .pipe(order([ // choisir un ordre de concat pour mettre certains fichiers en premiers
            'js/src/**/_1.*.js',
            'js/src/**/_2.*.js',
            'js/src/**/_3.*.js',
            'js/src/**/*.js'
        ]))
        .pipe(concat('main_javascript.js')) // concat() : On réuni tous les fichiers js en un seul fichier
        .on('error', function(err) {
            return notify().write(err); // Si erreur, l'afficher dans la console
        })
        .pipe(gulp.dest('js')) // On exporte le fichier non minifier dans dist (pour plus de facilité à debug), il aura le nom du concat
        .pipe(browserSync.stream()) // On reload la page dans le navigateur
        .pipe(rename('main_javascript.min.js')) // rename() : On choisi le nom du fichier de sorti minifié
        .pipe(uglify()) // uglify() : On minify le fichier précedemment concatené
        
        .pipe(gulp.dest('js')) // On exporte le fichier minifié dans dist (à utiliser une fois le site fonctionnel)
        .pipe(browserSync.stream()) // On reload la page dans le navigateur
});




gulp.task('bs-reload', function () {
    browserSync.reload();
});


gulp.task('browser-sync', function() {
    browserSync.init(['css/*.css', 'js/*.js'], {
        open: true,
       
        proxy: virtual_host
       
    });
});


gulp.task('default', ['sass', 'sass_admin', 'js', 'browser-sync'], function () {

    gulp.watch(['sass/*.scss', 'sass/**/*.scss'], ['sass'])
    gulp.watch(['sass/admin/*.scss', 'sass/admin/**/*.scss'], ['sass_admin'])
    gulp.watch(['js/src/*.js', 'js/src/**/*.js'], ['js'])
    gulp.watch(['*.php', '**/*.php']).on('change', browserSync.reload) // En cas de changement sur le php, on reload la page dans le navigateur
    gulp.watch(['js/*.js', 'js/**/*.js']).on('change', browserSync.reload)
   
});