var color_haut_de_france = '#efd815';
var color_bretagne = '#e7e19c';
var color_normandie = '#e3cd67';
var color_ile_de_france = '#e3d64b';
var color_grand_est = '#ddbf14';
var color_pays_loire = '#d2ba33';
var color_centre_val_loire = '#ffd800';
var color_bourgogne_franche_comte = '#ded100';
var color_nouvelle_aquitaine = '#ede96f';
var color_occitanie = '#ffea00';
var color_auvergne_rhonealpes = '#d5c857';
var color_provence_alpes_azur = '#f8e478';
var color_corse = '#eed842';
var color_outremer = '#f1d728';

var couleurs = {

// Hauts de France
"62":color_haut_de_france,
"59":color_haut_de_france,
"80":color_haut_de_france,
"60":color_haut_de_france,
"02":color_haut_de_france,

// Bretagne
"29":color_bretagne,
"22":color_bretagne,
"56":color_bretagne,
"35":color_bretagne,

// Normandie
"50":color_normandie,
"14":color_normandie,
"61":color_normandie,
"27":color_normandie,
"76":color_normandie,

// Île-de-France
"95":color_ile_de_france,
"78":color_ile_de_france,
"91":color_ile_de_france,
"77":color_ile_de_france,
"75":color_ile_de_france,
"92":color_ile_de_france,
"93":color_ile_de_france,
"94":color_ile_de_france,

// Grand-Est
"08":color_grand_est,
"51":color_grand_est,
"55":color_grand_est,
"57":color_grand_est,
"54":color_grand_est,
"67":color_grand_est,
"10":color_grand_est,
"52":color_grand_est,
"88":color_grand_est,
"68":color_grand_est,

// Pays de la Loire
"44":color_pays_loire,
"85":color_pays_loire,
"53":color_pays_loire,
"72":color_pays_loire,
"49":color_pays_loire,

// Centre Val-de-Loire
"37":color_centre_val_loire,
"41":color_centre_val_loire,
"28":color_centre_val_loire,
"45":color_centre_val_loire,
"36":color_centre_val_loire,
"18":color_centre_val_loire,

// Bourgogne Franche-Comté
"89":color_bourgogne_franche_comte,
"58":color_bourgogne_franche_comte,
"21":color_bourgogne_franche_comte,
"71":color_bourgogne_franche_comte,
"70":color_bourgogne_franche_comte,
"25":color_bourgogne_franche_comte,
"39":color_bourgogne_franche_comte,
"90":color_bourgogne_franche_comte,

// Nouvelle-Aquitaine
"64":color_nouvelle_aquitaine,
"40":color_nouvelle_aquitaine,
"17":color_nouvelle_aquitaine,
"33":color_nouvelle_aquitaine,
"79":color_nouvelle_aquitaine,
"86":color_nouvelle_aquitaine,
"16":color_nouvelle_aquitaine,
"87":color_nouvelle_aquitaine,
"23":color_nouvelle_aquitaine,
"19":color_nouvelle_aquitaine,
"24":color_nouvelle_aquitaine,
"47":color_nouvelle_aquitaine,

// Occitanie
"32":color_occitanie,
"65":color_occitanie,
"31":color_occitanie,
"09":color_occitanie,
"66":color_occitanie,
"11":color_occitanie,
"82":color_occitanie,
"34":color_occitanie,
"81":color_occitanie,
"46":color_occitanie,
"48":color_occitanie,
"12":color_occitanie,
"30":color_occitanie,

// Auverge Rhône-Alpes
"03":color_auvergne_rhonealpes,
"15":color_auvergne_rhonealpes,
"63":color_auvergne_rhonealpes,
"43":color_auvergne_rhonealpes,
"42":color_auvergne_rhonealpes,
"69":color_auvergne_rhonealpes,
"07":color_auvergne_rhonealpes,
"26":color_auvergne_rhonealpes,
"38":color_auvergne_rhonealpes,
"01":color_auvergne_rhonealpes,
"73":color_auvergne_rhonealpes,
"74":color_auvergne_rhonealpes,

// Provence Alpes-Côte d'Azur
"13":color_provence_alpes_azur,
"84":color_provence_alpes_azur,
"04":color_provence_alpes_azur,
"05":color_provence_alpes_azur,
"06":color_provence_alpes_azur,
"83":color_provence_alpes_azur,

// Corse
"2A":color_corse,
"2B":color_corse,

// Outre-Mer
"971":color_outremer,
"972":color_outremer,
"973":color_outremer,
"974":color_outremer,
"976":color_outremer,

};