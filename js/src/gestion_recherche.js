
$(document).ready(function() {

	$('.subnav .search_link img, .subnav .search_link .txt_search').click(function(e) {
		if(e.target !== this) return; // Ne pas detecter le clic sur les enfants
		$('.subnav .search_overlay').addClass('active');
		console.log('Clicked');
	});

	$('.subnav .search_overlay .close').click(function() {
		$('.subnav .search_overlay').removeClass('active');
	});

});