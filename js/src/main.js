

// On off nav responsive
jQuery(document).ready(function() {
	jQuery('.bouton_menu_responsive').on('click', function(e) {
		if(jQuery('.nav_departement').hasClass('open')) { jQuery('.nav_departement').removeClass('open'); }
		else { jQuery('.nav_departement').addClass('open'); }
	});
});

// On off menu utilisateur
jQuery(document).ready(function() {
	jQuery('.account_button img').on('click', function(e) {
		if(jQuery('.user_menu').hasClass('open')) { jQuery('.user_menu').removeClass('open'); }
		else { jQuery('.user_menu').addClass('open'); }
	});
});

// On off menu aide
jQuery(document).ready(function() {
	jQuery('.help_button').on('click', function(e) {
		if(jQuery('.help_menu').hasClass('open')) { jQuery('.help_menu').removeClass('open'); }
		else { jQuery('.help_menu').addClass('open'); }
	});
});


// SMOOTH ANCHOR SCROLL
jQuery(document).ready(function() {
	jQuery('.js-scrollTo').on('click', function() { 
		var page = jQuery(this).attr('href'); // Page cible
		page = page.substring(page.indexOf("#"));
		var speed = 500; // Durée de l'animation (en ms)
		jQuery('html, body').animate( { scrollTop: jQuery(page).offset().top }, speed ); // Déplacement du scroll avec interpolation
		self.location.href = page;
		return false;
	});
});


// Messages d'aide
jQuery(document).ready(function() {
	jQuery('.help_button').on('click', function() { 
		help_message = jQuery(this).find('.help_message');
		if(help_message.hasClass('active')) {
			console.log('already has class "active"');
		} else {
			console.log('adding class "active"');
			help_message.addClass('active');
		}
	});
	jQuery('.close_help').on('click', function(e) { 
		e.stopPropagation();
		jQuery(this).parent().removeClass('active');
	});
});


// Pinned info
jQuery(document).ready(function() {
	jQuery('.pinned_info .pin').on('click', function(e) {
		if(jQuery('.pinned_info').hasClass('closed')) { jQuery('.pinned_info').removeClass('closed'); }
		else { jQuery('.pinned_info').addClass('closed'); }
	});
});


// JS de la carte des accès départements
function slugify(text) {
	return text.toString().toLowerCase()
	.replace(/\s+/g, '-')
	.replace('ô', 'o')
	.replace('é', 'e')
	.replace('è', 'e')
	.replace('ê', 'e')
	.replace('\'', '-')
	.replace(/[^\w\-]+/g, '')
	.replace(/\-\-+/g, '-')
	.replace(/^-+/, '')
	.replace(/-+$/, '')
	.replace('pyrenes', 'pyrenees');
}
jQuery(document).ready(function() {
	if(document.getElementById("francemap")) {
		jQuery('#francemap').vectorMap({
		    map: 'france_fr',
			hoverOpacity: 0.5,
			hoverColor: false,
			backgroundColor: "#ffffff",
			colors: couleurs,
			borderColor: "#000000",
			selectedColor: "#000000",
			enableZoom: true,
			showTooltip: true,
		    onRegionClick: function(element, code, region) {
		    	var link = '/'+code+'-'+slugify(region);
		        window.location.href = link;
		    }
		});
	}
});
