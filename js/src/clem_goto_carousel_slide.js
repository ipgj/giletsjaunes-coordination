/*
Script by : Clem
-------------------------------------------
||          Goto carousel slide          ||
-------------------------------------------

xxxxxxxx
Rien à modifier dans ce fichier.
*/

jQuery(document).ready(function() {
	jQuery('.goto-carousel-slide').on('click', function(e) {

		var target_modal = jQuery(this).attr('data-target');
		var goto_slide = parseInt(jQuery(this).attr('goto-carousel-slide'));

		console.log('Goto slide : '+goto_slide);

		jQuery(target_modal).find('.carousel').carousel(goto_slide);

	});

});
