
$(document).ready(function() {

    $('.conditions_checkbox').click(function (e) { 
        //e.preventDefault();
        checked = $(this).is(':checked');

        if(checked) {
            $('#mc-embedded-subscribe2').removeClass('disabled');
            $('#mc-embedded-subscribe2').prop("disabled", false);
        } else {
            $('#mc-embedded-subscribe2').addClass('disabled');
            $('#mc-embedded-subscribe2').prop("disabled", true);
        }
    });

});