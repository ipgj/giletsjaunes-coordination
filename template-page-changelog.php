<?php
/*
Template Name: Page - Changelog
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page">
		<main class="clearfix nopaddingtop">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>

			<section class="page_content changelog_content shadowed_box">

				<?php $i = 0;
				if(have_rows('entrees_changelog')) : 
					while(have_rows('entrees_changelog')) : the_row(); $i++; ?>

						<div class="entry">
							<h3 class="titre"><?= get_sub_field('fontawesome_icon') ?> <?= get_sub_field('titre') ?></h3>
							<p class="date"><span class="trait"></span> <?= get_sub_field('date') ?></p>
							<div class="content"><?= get_sub_field('texte') ?></div>
						</div>
						<div class="separateur"></div>

					<?php endwhile;
				endif; ?>

			</section>

			<section class="footer_content">
				<div class="texte">
					<p>Si vous aviez une question, et que vous n'en avez pas trouvé la réponse ici, vous pouvez également utiliser les forums d'aide.</p>
					<div class="text-center">
						<a href="<?= get_permalink(467) ?>" class="bouton_type_1">FORUMS D'AIDE</a>
					</div>
				</div>
			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




