<?php
/*
Template Name: Departement - Actualités
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container section_departement">

		<?php include('templatechunk-departement-nav.php'); ?>

		<main class="clearfix">

			<?php $parentId = $post->post_parent;
			$have_moderateur = get_field('have_moderateur', $parentId);
			if(!$have_moderateur) { ?>
				<div class="no_moderator">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/warning_icon_white.png"/>
					</div>
					<div class="texte">
						<?php echo get_field('message_aucun_moderateur', 'option'); ?>
					</div>
				</div>
			<?php } ?>

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title">L’agenda des actions et actualités Gilet Jaune : <?= explode(')', get_the_title($target_id))[1] ?></h1>
					</header>
				</article>
				<p><?= get_field('texte_entete_agenda_actus', 'option'); ?></p>
			</section>

			<?php
			$pinfo_image = get_field('pinned_info_image');
			$pinfo_texte = get_field('pinned_info_texte'); 
			?>

			<?php if($pinfo_texte != '') { ?>
				<div class="pinned_info">
					<div class="pin_container">
						<img class="pin" src="<?php echo get_template_directory_uri(); ?>/images/pin_icon.png"/>
					</div>
					<div class="content">
						<?php if($pinfo_image != '') { ?>
							<div class="img_container">
								<img src="<?php echo $pinfo_image['sizes']['medium']; ?>"/>
							</div>
						<?php } ?>
						<div class="texte_container">
							<?php echo $pinfo_texte; ?>
						</div>
					</div>
				</div>
			<?php } ?>

			<section class="liste_news">

				<?php
				$post_data = get_post($post->post_parent);
				$parent_slug = $post_data->post_name;
				$post_type_name = 'actu'.strstr($parent_slug, '-');

				$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

				$item_per_page = get_field('nombre_dactualites_par_page');
				$args = array(
				    'post_type'      => $post_type_name,
				    'posts_per_page' => $item_per_page,
				    'order'          => 'DESC',
				    'orderby'        => 'date',
				    'paged' => $paged,
					'meta_query'	=> array(
						'relation' => 'OR',
						array(
							'key'	  	=> 'contenu_protege',
							'value'	  	=> '',
							'compare' 	=> '=',
						),
						array(
							'key'	  	=> 'contenu_protege',
							'value'	  	=> '',
							'compare' 	=> 'NOT EXISTS',
						),
					),
				 );

				$news = new WP_Query( $args );

				if ( $news->have_posts() ) :
				    while ( $news->have_posts() ) : $news->the_post();

				    	$is_protected = get_field('contenu_protege');
				    	if($is_protected AND $is_protected[0] == 'content_is_protected') {
				    		// CETTE ACTUALITÉ EST PROTÉGÉE !
				    	} else {

					    	$img = get_the_post_thumbnail();
					    	if(!$img OR $img == '') { $img = '<img src="'.get_template_directory_uri().'/images/no_image.jpg"/>'; }
					    	$date = get_the_date(get_option('date_format'));
							$text_unformed = get_the_content();
							$text = wpautop($text_unformed);
							$type_evenement = get_field('type_devenement');
							$emplacement = get_field('emplacement_geographique');

							$comment_number = get_comments_number();
							if($comment_number == 0) { $comment_text = ''; }
							if($comment_number == 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaire'; }
							if($comment_number > 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaires'; }

					    	$page_link_ID = get_the_ID(); 
					    	$current = '';
					    	if($page_link_ID == $current_page_ID) { $current = 'active'; } ?>
					    	<div class="news_container row no-gutters shadowed_box">
					    		<div class="gauche col-lg-3">
					    			<?php echo $img; ?>
					    		</div>
					    		<div class="droite col-lg-9">
					    			<div class="date">> Publié le <?php echo $date; ?></div>
					    			<?php if($comment_number > 0) { ?>
					    				<div class="nb_comment"><?= $comment_text; ?></div>
					    			<?php } ?>
					    			<a class="titre <?php echo $current; ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
					    			<div class="entete">
					    				<?php if($type_evenement != '') { echo '<span class="type">'.$type_evenement.'</span>'; } ?>
					    				<?php if($type_evenement != '' AND $emplacement != '') { echo '<span class="tiret"> - </span>'; } ?>
					    				<?php if($emplacement != '') { echo '<span class="emplacement">'.$emplacement.'</span>'; } ?>
					    			</div>
					    			<div class="texte">
					    				<?php echo $text; ?>
					    			</div>
					    		</div>
					    	</div>

					    	<!-- <div class="news_separateur"></div> -->

					    <?php } ?>
				        
				    <?php endwhile;
				else :
					echo '<p class="bloc no_result">Aucune actualité n\'a été ajoutée dans ce département pour le moment...</p>';
				endif;

				wp_reset_postdata();
				?>

			</section>

			<?php $big = 999999999;
		    echo '<section class="wp_pagination">';
				echo paginate_links( array( // Plus d'info sur les arguments possibles : https://codex.wordpress.org/Function_Reference/paginate_links
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $news->max_num_pages
				) );
			echo '</section>'; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




