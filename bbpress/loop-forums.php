<?php

/**
 * Forums Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<?php do_action( 'bbp_template_before_forums_loop' ); ?>

<!-- <div class="fichier">FICHIER : loop-forums.php</div>  -->

<?php $current_page_ID = get_the_ID();
$current_page = get_post($current_page_ID);
$current_page_is_protected = false;
if(!empty($current_page->post_password)) { $current_page_is_protected = true; } 
$current_forum_is_protected = get_field('contenu_protege');?>

<ul id="forums-list-<?php bbp_forum_id(); ?>" class="bbp-forums">

	<li class="bbp-header">

		<ul class="forum-titles">
			<li class="bbp-forum-info"><?php _e( 'Forum', 'bbpress' ); ?></li>
			<li class="bbp-forum-topic-count"><?php _e( 'Topics', 'bbpress' ); ?></li>
			<li class="bbp-forum-reply-count"><?php bbp_show_lead_topic() ? _e( 'Replies', 'bbpress' ) : _e( 'Posts', 'bbpress' ); ?></li>
			<li class="bbp-forum-freshness"><?php _e( 'Freshness', 'bbpress' ); ?></li>
		</ul>

	</li><!-- .bbp-header -->

	<li class="bbp-body">

		<?php while ( bbp_forums() ) : bbp_the_forum(); 
			// Récupérer les IDs du forum actuel et du forum racine
			$forum_id = bbp_get_forum_id();
			$forum_ancestors = get_post_ancestors($forum_id);
			$forum_root_id = end($forum_ancestors);
			if($forum_root_id == 0) {
				$forum_root_id = $forum_id;
			}
			// Récupérer l'ID de la page d'accueil départementale liée à ce forum
			$departement_mainpage_ID = get_corresponding_main_dep_page($forum_root_id);
			?>

			<?php 
			$is_protected = get_field('contenu_protege');
			if($is_protected AND $is_protected[0] == 'content_is_protected') {
				//echo 'DEBUG : Field "contenu_protege" sur ON.<br/>';

				// Si on est sur une page protégée
				if($current_page_is_protected) {
					//echo 'DEBUG : ON EST SUR UNE PAGE PROTÉGÉE<br/>';
					// Récupérer l'ID de la protected page de ce département
					if(isset($departement_mainpage_ID)) {
						$args = array(
						    'post_type'      => 'any',
						    'posts_per_page' => -1,
						    'post_parent'    => $departement_mainpage_ID,
						    'order'          => 'ASC',
						    'orderby'        => 'menu_order'
						 );
						$souspages = new WP_Query( $args );
						if ($souspages->have_posts()) :
						    while ( $souspages->have_posts() ) : $souspages->the_post();
						    	$page_link_ID = get_the_ID(); 
						    	if(!empty($post->post_password)){ $password_protected_child_ID = $page_link_ID; }
						    endwhile;
						endif;
					}

					// Si le mot de passe départemental est valide, afficher le forum
					if (!post_password_required($password_protected_child_ID)) {
						//echo 'DEBUG : PASS VALIDE, AFFICHER FORUM PROTEGE<br/>';
						bbp_get_template_part( 'loop', 'single-forum' );
					}
				} else {
					//echo 'DEBUG : La page n\'est pas protégée.<br/>';
					if($current_forum_is_protected AND $current_forum_is_protected[0] == 'content_is_protected') {
						//echo 'DEBUG : Le forum actuel est protégé, il faut afficher les enfants protégés !!!<br/>';
						bbp_get_template_part( 'loop', 'single-forum' );
					}
				}
				
			} else {
				bbp_get_template_part( 'loop', 'single-forum' );
			}
			?>

		<?php endwhile; ?>

	</li><!-- .bbp-body -->

	<li class="bbp-footer">

		<div class="tr">
			<p class="td colspan4">&nbsp;</p>
		</div><!-- .tr -->

	</li><!-- .bbp-footer -->

</ul><!-- .forums-directory -->

<?php do_action( 'bbp_template_after_forums_loop' ); ?>
