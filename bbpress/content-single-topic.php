<?php

/**
 * Single Topic Content Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div id="bbpress-forums" class="content-single-topic_container">

	<!-- <div class="fichier">FICHIER : content-single-topic.php</div>  -->

	<div class="breadcrumb_container">
		<i class="fas fa-map-marker-alt"></i>
		<?php bbp_breadcrumb(); ?>
	</div>

	<?php if(is_user_logged_in()) { ?>
		<div class="text-right">
			<div class="help_button text-left">
				<i class="fas fa-question-circle"></i>
				<div class="help_message left">
					<div class="close_help"><i class="fas fa-times"></i></div>
					<p>Mettre ce sujet en favoris vous permettra de le retrouver facilement dans votre menu <a href="<?php echo bbp_user_profile_url(bbp_get_current_user_id()); ?>">Mes discussions</a>.</p>
					<p>Vous abonner à ce sujet vous permettra de recevoir un e-mail à chaque fois qu'une nouvelle réponse est postée dans celui-ci.</p>

				</div>
			</div>
		</div>
	<?php } ?>

	<?php do_action( 'bbp_template_before_single_topic' ); ?>

	<?php if ( post_password_required() ) : ?>

		<?php bbp_get_template_part( 'form', 'protected' ); ?>

	<?php else : ?>

		<?php bbp_topic_tag_list(); ?>

		<?php //bbp_single_topic_description(); ?>

		<?php if ( bbp_show_lead_topic() ) : ?>

			<?php bbp_get_template_part( 'content', 'single-topic-lead' ); ?>

		<?php endif; ?>

		<?php if ( bbp_has_replies() ) : ?>

			<?php bbp_get_template_part( 'pagination', 'replies' ); ?>

			<?php bbp_get_template_part( 'loop',       'replies' ); ?>

			<?php bbp_get_template_part( 'pagination', 'replies' ); ?>

		<?php endif; ?>

		<?php bbp_get_template_part( 'form', 'reply' ); ?>

	<?php endif; ?>

	<?php do_action( 'bbp_template_after_single_topic' ); ?>

</div>
