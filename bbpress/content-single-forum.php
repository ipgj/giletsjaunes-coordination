<?php

/**
 * Single Forum Content Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div id="bbpress-forums">

	<!-- <div class="fichier">FICHIER : content-single-forum.php</div>  -->

	<div class="breadcrumb_container">
		<i class="fas fa-map-marker-alt"></i>
		<?php bbp_breadcrumb(); ?>
	</div>

	<div class="subscription_container">
		<?php bbp_forum_subscription_link(); ?>
		<div class="help_button">
			<i class="fas fa-question-circle"></i>
			<div class="help_message left">
				<div class="close_help"><i class="fas fa-times"></i></div>
				<p>Vous abonner à ce forum vous permettra de recevoir un e-mail à chaque fois qu'un nouveau sujet est créé dans celui-ci.</p>
				<p>Vous pouvez retrouver la liste de vos abonnements dans le menu <a href="<?php echo bbp_user_profile_url(bbp_get_current_user_id()); ?>">Mes discussions</a>.</p>
			</div>
		</div>
	</div>

	<?php if (!bbp_has_forums() AND is_user_logged_in()) { ?>
		<div class="new_topic_link_container">
			<a href="#new-post" class="bouton_type_2 b js-scrollTo">+ Nouveau sujet</a>
		</div>
	<?php } ?>

	<?php do_action( 'bbp_template_before_single_forum' ); ?>

	<?php if ( post_password_required() ) : ?>

		<?php bbp_get_template_part( 'form', 'protected' ); ?>

	<?php else : ?>

		<?php //bbp_single_forum_description(); ?>

		<?php if ( bbp_has_forums() ) : ?>

			<?php bbp_get_template_part( 'loop', 'forums' ); ?>

		<?php endif; ?>

		<?php if ( !bbp_is_forum_category() && bbp_has_topics() ) : ?>

			<?php bbp_get_template_part( 'pagination', 'topics'    ); ?>

			<?php bbp_get_template_part( 'loop',       'topics'    ); ?>

			<?php bbp_get_template_part( 'pagination', 'topics'    ); ?>

			<?php bbp_get_template_part( 'form',       'topic'     ); ?>

		<?php elseif ( !bbp_is_forum_category() ) : ?>

			<?php bbp_get_template_part( 'feedback',   'no-topics' ); ?>

			<?php bbp_get_template_part( 'form',       'topic'     ); ?>

		<?php endif; ?>

	<?php endif; ?>

	<?php do_action( 'bbp_template_after_single_forum' ); ?>

</div>
