<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div id="post-<?php bbp_reply_id(); ?>" class="bbp-reply-header">

	<div class="bbp-meta">

		<span class="bbp-reply-post-date"><?php bbp_reply_post_date(); ?></span>

		<?php if ( bbp_is_single_user_replies() ) : ?>

			<span class="bbp-header">
				<?php _e( 'in reply to: ', 'bbpress' ); ?>
				<a class="bbp-topic-permalink" href="<?php bbp_topic_permalink( bbp_get_reply_topic_id() ); ?>"><?php bbp_topic_title( bbp_get_reply_topic_id() ); ?></a>
			</span>

		<?php endif; ?>

		<a href="<?php bbp_reply_url(); ?>" class="bbp-reply-permalink">#<?php bbp_reply_id(); ?></a>

		<?php do_action( 'bbp_theme_before_reply_admin_links' ); ?>

		<?php 
		$moderator_dep_num = false;
		if(get_current_user_main_role() == 'admin') {

			// Trouver le num du département modérable par l'admin' connecté actuel
			$user_role = get_current_user_role();
			if (($pos = strrpos($user_role, "_")) !== FALSE) { 
			    $moderator_dep_num = substr($user_role, $pos+1); 
			}

			// Trouver le num dep correspondant au root forum du forum actuel
			$forum_id = bbp_get_forum_id();
			$forum_ancestors = get_post_ancestors($forum_id);
			$forum_root_id = end($forum_ancestors);

			$current_dep_num = get_dep_num_from_forum_root_id($forum_root_id);

			// Le modérateur dep est dans son propre département
			if($moderator_dep_num == $current_dep_num) {
				$reply_id = bbp_get_reply_id();
				if(bbp_is_topic($reply_id)) {
					$args = array('links' => apply_filters( 'bbp_topic_admin_links', array(
		                'edit'  => bbp_get_topic_edit_link(),
		                'close' => bbp_get_topic_close_link(),
		                'stick' => bbp_get_topic_stick_link(),
		                'merge' => bbp_get_topic_merge_link(),
		                'trash' => bbp_get_topic_trash_link(),
		                'spam'  => bbp_get_topic_spam_link(),
		                'reply' => bbp_get_topic_reply_link()
		            ), $reply_id));
					bbp_reply_admin_links($args);
				} else {
					$args = array('links' => apply_filters( 'bbp_reply_admin_links', array(
		                'edit'  => bbp_get_reply_edit_link(),
		                'move'  => bbp_get_reply_move_link(),
		                'split' => bbp_get_topic_split_link(),
		                'trash' => bbp_get_reply_trash_link(),
		                'spam'  => bbp_get_reply_spam_link(),
		                'reply' => bbp_get_reply_to_link()
		            ), $reply_id));
					bbp_reply_admin_links($args);
				} 

			// Le modérateur dep est dans un autre département
			} else {
				$reply_id = bbp_get_reply_id();
				if(bbp_is_topic($reply_id)) {
					$args = array('links' => apply_filters( 'bbp_topic_admin_links', array(
		                'reply' => bbp_get_topic_reply_link()
		            ), $reply_id));
					bbp_reply_admin_links($args);
				} else {
					$args = array('links' => apply_filters( 'bbp_reply_admin_links', array(
		                'reply' => bbp_get_reply_to_link()
		            ), $reply_id));
					bbp_reply_admin_links($args);
				}
			}

		// Le user n'est pas un Admin dep, utiliser la gestion de base des admin links
		} else {
			bbp_reply_admin_links(); 
		}
		?>

		<?php do_action( 'bbp_theme_after_reply_admin_links' ); ?>

	</div><!-- .bbp-meta -->

</div><!-- #post-<?php bbp_reply_id(); ?> -->

<div <?php bbp_reply_class(); ?>>

	<div class="single_forum_post_container">

		<?php 
		$user_id = bbp_get_reply_author_id();
		$user_rolename = get_user_role_name($user_id);
		if($user_rolename != 'Subscriber' AND $user_rolename != '') {
			$role_class = ' has_role';
		} else  {
			$role_class = '';
		} ?>
		<div class="bbp-reply-author<?= $role_class ?>">

			<?php do_action( 'bbp_theme_before_reply_author_details' ); ?>

			<?php bbp_reply_author_link( array( 'sep' => '', 'show_role' => false ) ); ?>

			<?php // Affichage du département de l'utilisateur si il en a un
			$departement_user = get_user_departement($user_id);
			if($departement_user != 'Aucun' AND $departement_user != '') { ?>
				<div class="reply_author_dep">(<?= $departement_user ?>)</div>
			<?php } ?>

			<?php // Affichage du rôle de l'utilisateur si il en a un
			if($user_rolename != 'Subscriber' AND $user_rolename != '') { 
				$user_role = get_user_role($user_id);
				if (($pos = strrpos($user_role, "_")) !== FALSE) { 
				    $replier_dep_num = substr($user_role, $pos+1); 
				}
				if(isset($replier_dep_num)) {
					$dep_name = get_dep_name_from_num($replier_dep_num);
					$user_rolename = 'Admin '.$dep_name;
				}

				?>
				<div class="reply_author_role">[<?= $user_rolename ?>]</div>
			<?php } ?>

			<?php if ( bbp_is_user_keymaster() ) : ?>

				<?php do_action( 'bbp_theme_before_reply_author_admin_details' ); ?>

				<div class="bbp-reply-ip"><?php bbp_author_ip( bbp_get_reply_id() ); ?></div>

				<?php do_action( 'bbp_theme_after_reply_author_admin_details' ); ?>

			<?php endif; ?>

			<?php do_action( 'bbp_theme_after_reply_author_details' ); ?>

		</div><!-- .bbp-reply-author -->

		<div class="bbp-reply-content">

			<?php do_action( 'bbp_theme_before_reply_content' ); ?>

			<?php bbp_reply_content(); ?>

			<?php do_action( 'bbp_theme_after_reply_content' ); ?>

		</div><!-- .bbp-reply-content -->

	</div>

</div><!-- .reply -->
