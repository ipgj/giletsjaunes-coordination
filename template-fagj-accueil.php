<?php
/*
Template Name: FAGJ - Accueil
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container section_departement">

		<?php include('templatechunk-fagj-nav.php'); ?>

		<main class="clearfix container">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= do_shortcode(get_field('texte_intro')); ?>
			</section>

			<section class="content_page">

				<div class="corps_accueildep">

					<?php $pinfo_image = get_field('pinned_info_image');
					$pinfo_texte = get_field('pinned_info_texte'); 
					$sidebar_zone_class = '';
					if($pinfo_texte != '') { $sidebar_zone_class = ' has_pinned_info'; } ?>

					<div class="sidebar_zone row no-gutters<?= $sidebar_zone_class ?>">

						<div class="main_content col-lg-9">
							
							<?php if($pinfo_texte != '') { ?>
								<div class="pinned_info">
									<div class="pin_container">
										<img class="pin" src="<?= get_template_directory_uri(); ?>/images/pin_icon.png"/>
									</div>
									<div class="content">
										<?php if($pinfo_image != '') { ?>
											<div class="img_container">
												<img src="<?= $pinfo_image['sizes']['medium']; ?>"/>
											</div>
										<?php } ?>
										<div class="texte_container">
											<?= do_shortcode($pinfo_texte); ?>
										</div>
									</div>
								</div>
							<?php } ?>

							<section class="liste_news last_news">

								<h3 class="titre_section">Dernières actualités de la FAGJ</h3>

								<?php
								$nb_to_display = get_field('nombre_actus');

								$args = array(
									'post_type'      => 'actu-fagj',
									'posts_per_page' => $nb_to_display,
									'order'          => 'DESC',
									'orderby'        => 'date',
									'paged' => $paged,
								);

								$news = new WP_Query( $args );

								if ( $news->have_posts() ) {
									while ( $news->have_posts() ) : $news->the_post();

										$img = get_the_post_thumbnail();
										if(!$img OR $img == '') { $img = '<img src="'.get_template_directory_uri().'/images/no_image.jpg"/>'; }
										$date = get_the_date(get_option('date_format'));
										$text_unformed = get_the_content();
										$text = wpautop($text_unformed);
										$type_evenement = get_field('type_devenement');
										$emplacement = get_field('emplacement_geographique');

										$comment_number = get_comments_number();
										if($comment_number == 0) { $comment_text = ''; }
										if($comment_number == 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaire'; }
										if($comment_number > 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaires'; }

										$page_link_ID = get_the_ID(); 
										$current = '';
										if($page_link_ID == $current_page_ID) { $current = 'active'; } ?>
										<div class="news_container row no-gutters">
											<div class="gauche col-lg-3">
												<?= $img; ?>
											</div>
											<div class="droite col-lg-9">
												<div class="date">> Publié le <?= $date; ?></div>
												<?php if($comment_number > 0) { ?>
													<div class="nb_comment"><?= $comment_text; ?></div>
												<?php } ?>
												<a class="titre <?= $current; ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
												<div class="entete">
													<?php if($type_evenement != '') { echo '<span class="type">'.$type_evenement.'</span>'; } ?>
													<?php if($type_evenement != '' AND $emplacement != '') { echo '<span class="tiret"> - </span>'; } ?>
													<?php if($emplacement != '') { echo '<span class="emplacement">'.$emplacement.'</span>'; } ?>
												</div>
												<div class="texte">
													<?= $text; ?>
												</div>
											</div>
										</div>

										<div class="news_separateur"></div>
										
									<?php endwhile;
								} else {
									echo '<p class="no_result">Aucune actualité FAGJ...</p>';
								}

								wp_reset_postdata();
								?>

								<div class="text-center"><a href="<?= get_permalink(125421) ?>" class="bouton_type_1">TOUTES LES ACTUALITÉS</a></div>

							</section>

						</div>

						<div class="sidebar_container col-lg-3">

							<div class="widget last_compterendu liste_fichiers">

								<h4 class="sidebar_title">ADMINS ESPACE FAGJ</h4>

								<?php 
								$numero_departement = get_corresponding_num(get_the_ID());
								$args = array(
									'role'    => 'admin_fagj',
									'orderby' => 'user_nicename',
									'order'   => 'ASC'
								);
								$users = get_users( $args );
								foreach ( $users as $user ) { ?>
									<a class="nom_admin lien_texte" href="<?= esc_html(bbp_user_profile_url($user->ID)); ?>"><div class="ligne"><?= esc_html($user->display_name); ?></div></a>
								<?php } ?>

								<div class="separateur"></div>

							</div>

							<div class="widget last_compterendu liste_fichiers">

								<h4 class="sidebar_title">OUTILS FAGJ</h4>

								<?php 
								if(have_rows('liste_outils_fagj', 103933)) : while(have_rows('liste_outils_fagj', 103933) AND ($i < $nb_to_display)) : the_row(); 
									$img = get_sub_field('image');
									$titre = get_sub_field('titre');
									$lien = get_sub_field('lien_bouton');
									?>

									<a href="<?= $lien; ?>" target="_blank" class="ligne">
										<div class="file_icon"><img src="<?= $img['sizes']['medium']; ?>"/></div>
										<div class="texte">
											<p class="titre"><?= $titre; ?></p>
										</div>
									</a>

								<?php $i++; endwhile; else :
									echo '<p class="no_result">Aucun outil FAGJ n\'a été ajouté.</p>';
								endif; ?>

								<div class="widget_footer">
									<a href="<?= get_permalink(103933); ?>" class="bouton_sidebar">DÉTAIL DES OUTILS</a>
								</div>

								<div class="separateur"></div>

							</div>

						</div>

					</div>

				</div>

			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




