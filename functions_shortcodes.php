<?php

function declare_collapse_shortcode($params = array(), $content = null) {

	// default parameters
	extract(shortcode_atts(array(
		'title' => 'Titre de l\'accordéon',
		'opened' => false,
    ), $params));
    
    $slug = slugify($title);
    if($opened == 'true') {
        $collapsed = '';
        $show = 'show';
    } else {
        $collapsed = 'collapsed';
        $show = '';
    }

    $output = '';
    $output .= '<div class="module_bloc_shortcode shadowed_box">';
        $output .= '<a data-toggle="collapse" href="#accordeon_'.$slug.'" class="module_titre '.$collapsed.'"><div>'.$title.'</div><i class="fas fa-chevron-up"></i></a>';
        $output .= '<div id="accordeon_'.$slug.'" class="module_content_container collapse '.$show.'">';
            $output .= '<div class="module_content">';
                $output .= do_shortcode($content);
            $output .= '</div>';
        $output .= '</div>';
    $output .= '</div>';

    return $output;
}
add_shortcode('collapse', 'declare_collapse_shortcode');
?>