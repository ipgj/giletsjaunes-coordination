<?php 
if (isset($ReverseproxyArgs)) {
	$currentPage = $ReverseProxyArgs['page'];
} else {
	$currentPage = $_SERVER['REQUEST_URI'];
}
$currentHost = $_SERVER['HTTP_HOST'];
$currentProtocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']) ? 'https' : 'http';

$withHeader = true;

// Pas de header pour certaines formes d'URL
if (strpos($currentPage, '/outils/css/') === 0) $withHeader = false;
if (strpos($currentPage, '/outils/img/') === 0) $withHeader = false;
if (strpos($currentPage, '/outils/js/') === 0) $withHeader = false;
if (substr($currentPage, -3) == '.js') $withHeader = false;
if (substr($currentPage, -4) == '.css') $withHeader = false;
if (substr($currentPage, -4) == '.jpg') $withHeader = false;
if (substr($currentPage, -4) == '.img') $withHeader = false;
if (isset($_GET['embed'])) $withHeader = false;


// Pas de header pour certaines URL (liste de regexps)
$pageNoArgs = $currentPage;
if ($pos = strpos($currentPage, '?'))
	$pageNoArgs = substr($currentPage, 0, $pos);
$NoHeaderRegexps = array(
    '/^document-.*-([0-9]+)\.[a-z0-9]+$/',
    '/^information-.*-([0-9]+)\.[a-z0-9]+$/',
    '/^.*-d([0-9]+)\.[a-z0-9]+$/'
);
foreach ($NoHeaderRegexps as $re)
	if (preg_match($re, $pageNoArgs))
		$withHeader = false;



$url = constant('DATA_HOST').$currentPage;
if (strpos($url, '?') === false) $url .= '?'; else $url .= '&';
$url .= 'include=' . $currentProtocol . '://' . $currentHost;

$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $url);

if (count($_POST) > 1) {
	curl_setopt($ch, CURLOPT_POST, 1);
	$vars = "";
	foreach($_POST as $key => $value) {
		$vars .= $key."=".$value."&";
	}
	$vars = substr($vars,0,-1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
}

$out = curl_exec($ch);

$headers_end = strpos($out, "\r\n\r\n");
if( $headers_end !== false ) { 
    $headersRaw = substr($out, 0, $headers_end);
    $headersRaw = str_replace("\r", "", $headersRaw);
    $content = substr($out, $headers_end+4);
} else {
    $headersRaw = $out;
    $content = '';
}

$headers = explode("\n", $headersRaw);
foreach($headers as $header) {
    if( substr($header, 0, 10) == "Location: " ) { 
        $target = substr($header, 10);
        echo "[$url] redirects to [$target]<br>";
	exit;
    }   
    if( substr($header, 0, 14) == "Content-Type: " ) { 
	header($header);
	$type = substr($header, 14);
	if (substr($type, 0, 9) != 'text/html') $withHeader = false;
    }
}   


if ($withHeader) get_header();
if ($withHeader) {
?>
        <div class="main_content_container">
                <main class="clearfix container">

                        <section class="entete_page">
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <header class="entry-header">
                                    <h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
                                </header>
                            </article>
                            <?= get_field('texte_intro'); ?>
                        </section>

                        <section class="data_content">

<?php
}
	
echo $content;

if ($withHeader) { 
?>

                        </section>

                        <section class="data_bas">
                            <?= get_field('texte_bas') ?>
                        </section>

                </main>
        </div>

<?
}

if ($withHeader) get_footer();

?>
