<?php
/*
Template Name: Page - Statistiques
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page">
		<main class="clearfix nopaddingtop">
				
			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>


			<section class="stats_deps shadowed_box">
				<h3 class="titre_bloc">STATISTIQUES PAR DÉPARTEMENT</h3>
				<div id="liste_deps">

					<table class="table_stat">
						<thead>
							<tr>
								<td class="admin">Adm.</td>
								<td>Département</td>
								<td>Nb utilisateurs</td>
								<td>Nb admins</td>
								<td>Nb actus</td>
							</tr>
						</thead>
						<tbody>
							<?php 

							// LOOP USERS
							$all_users = get_users();
							$users_roles = Array();
							$users_deps = Array();
							foreach($all_users as $user) {
								// Roles
								$roles = $user->caps;
								$roles = array_keys($roles);
								$main_role = $roles[0];
								$admin_of_dep = str_replace('admin_dpt_', '', $main_role);
								$users_roles[$admin_of_dep]++;

								// Département
								$user_dep = get_user_departement($user->ID);
								$user_dep = substr($user_dep, 0, strpos($user_dep, ' -'));
								$users_deps[$user_dep]++;						
							}

							// LOOP DÉPARTEMENTS
							$number_of_dep = 0; $nb_moderated_dep = 0; 
							if(have_rows('liens_departements_forums', 'options')) : while(have_rows('liens_departements_forums', 'options')) : the_row(); 

								$number_of_dep++;

								$num = get_sub_field('num');
								$nom = get_sub_field('nom');
								$slug = get_sub_field('slug');
								$main_dep_page = get_sub_field('page_principale_departementale');
								$root_forum = get_sub_field('forum_departemental_racine');

								// Initialisations
								$admindeps_class = '';
								$users_class = '';
								$actus_class = '';

								// STAT - Nombre de départements pris en charge
								$have_moderateur = get_field('have_moderateur', $main_dep_page);
								if($have_moderateur[0] == 'have_moderateur') { 
									$nb_moderated_dep++; 
									$have_admin = '<i class="fas fa-check-circle"></i>';
								} else { $have_admin = ''; }

								// STAT - Nb utilisateurs / admins
								$nb_admindeps = $users_roles[$num];
								if($nb_admindeps == '') { $nb_admindeps = 0; $admindeps_class = 'zero'; }
								$nb_users = $users_deps[$num];
								if($nb_users == '') { $nb_users = 0; $users_class = 'zero'; }

								// STAT - Nb d'actus
								$nb_actus = wp_count_posts('actu-'.$slug);
								$nb_actus = $nb_actus->publish;
								$total_actus = $total_actus + $nb_actus;
								if($nb_actus == 0) { $actus_class = 'zero'; }

								// Classes pour chiffres à 0

								?>
								<tr>
									<td class="admin"><?= $have_admin ?></td>
									<td><a href="<?= get_permalink($main_dep_page); ?>"><?= $num.' - '.$nom ?></a></td>
									<td class="<?= $users_class; ?>"><?= $nb_users; ?></td>
									<td class="<?= $admindeps_class; ?>"><?= $nb_admindeps; ?></td>
									<td class="<?= $actus_class; ?>"><?= $nb_actus; ?></td>
								</tr>

							<?php
							endwhile; endif; ?>
						</tbody>
					</table>

				</div>
			</section>

			
			<section class="equipe">
				<div class="row">
					<div class="col-lg-6">
						<div class="shadowed_box">
							<h3 class="titre_bloc">L'ÉQUIPE</h3>
							<h4 class="sidebar_subtitle">Administration</h4>
							<div class="userlist_container">
								<?php
								$args = array(
								    'role__in' => ['administrateur','administrator'],
								    'orderby' => 'user_nicename',
								    'order'   => 'ASC'
								);
								$users = get_users($args);
								foreach($users as $user) {
									$profile_link = bbp_get_user_profile_edit_url($user->ID);
									//print_r($users);
								    echo '<a href="'.$profile_link.'">'.esc_html($user->display_name).'</a>';
								}
								?>
							</div>
							<div class="separateur"></div>
							<h4 class="sidebar_subtitle">Modération</h4>
							<div class="userlist_container">
								<?php
								$args = array(
								    'role'    => 'moderateur',
								    'orderby' => 'user_nicename',
								    'order'   => 'ASC'
								);
								$users = get_users($args);
								foreach($users as $user) {
									$profile_link = bbp_get_user_profile_edit_url($user->ID);
									//print_r($users);
								    echo '<a href="'.$profile_link.'">'.esc_html($user->display_name).'</a>';
								}
								?>
							</div>
							<div class="separateur"></div>
							<h4 class="sidebar_subtitle">Administration départementale</h4>
							<p class="infosmall">Voir page d'accueil de chaque département.</p>
						</div>
					</div>
					<div class="stats_generales col-lg-6">
						<div class="shadowed_box">
							<h3 class="titre_bloc">STATISTIQUES GÉNÉRALES</h3>
							<?php // STAT - Total d'utilisateurs
							$usercount = count_users();
							$total_users = $usercount['total_users'];
							$total_adminteam = $users_roles['administrator'] + $users_roles['administrateur'] + $users_roles['moderateur'];
							$total_admindep = $total_users - $total_adminteam - $users_roles['subscriber'];

							// STATS FORUMS
							$stats_forums = bbp_get_statistics('count_users');
							?>

							<table class="table_stat">
								<tr>
									<td class="label">Utilisateurs</td>
									<td class="valeur"><?= number_format($total_users, 0, ',', ' '); ?></td>
								</tr>
								<tr>
									<td class="label">- administrateurs / modérateurs</td>
									<td class="valeur"><?= number_format($total_adminteam, 0, ',', ' '); ?></td>
								</tr>
								<tr>
									<td class="label">- administrateurs départementaux</td>
									<td class="valeur"><?= number_format($total_admindep, 0, ',', ' '); ?></td>
								</tr>
								<tr>
									<td class="label">Total de forums</td>
									<td class="valeur"><?= $stats_forums['forum_count']; ?></td>
									<!-- <td class="valeur">1 216</td> -->
								</tr>
								<tr>
									<td class="label">Total de sujets</td>
									<td class="valeur"><?= number_format($stats_forums['topic_count']+$stats_forums['topic_count_hidden'], 0, ',', ' ') ?></td>
								</tr>
								<tr>
									<td class="label">Total de réponses</td>
									<td class="valeur"><?= number_format($stats_forums['reply_count']+$stats_forums['reply_count_hidden'], 0, ',', ' ') ?></td>
								</tr>
								<tr>
									<td class="label">Total d'actualités</td>
									<td class="valeur"><?= number_format($total_actus, 0, ',', ' ') ?></td>
								</tr>
								<tr>
									<td class="label">Départements administrés</td>
									<td class="valeur"><?= $nb_moderated_dep; ?> / <?= $number_of_dep; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




