<?php
/*
Template Name: Page - FAQ
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page">
		<main class="clearfix nopaddingtop">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>

			<section class="page_content faq_content shadowed_box">

				<?php $i = 0;
				if(have_rows('liste_questions_reponses')) : 
					while(have_rows('liste_questions_reponses')) : the_row(); $i++; ?>

						<div class="faq_container">
							<a class="question collapsed" data-toggle="collapse" href="#reponse_<?=$i?>" role="button" aria-expanded="false" aria-controls="reponse_1">
								<i class="fas fa-arrow-right"></i> <?= get_sub_field('question'); ?>
							</a>
							<div id="reponse_<?=$i?>" class="reponse collapse">
								<div class="txt_container">
									<?= get_sub_field('reponse'); ?>
								</div>
							</div>
						</div>

					<?php endwhile;
				endif; ?>

			</section>

			<section class="footer_content">
				<div class="texte">
					<p>Vous n'avez pas trouvé réponse à votre question dans la liste ci-dessus ?<br/>
					Vous pouvez passer par les forums d'aide pour la poser. Elle sera probablement ajoutée à cette liste par la suite.</p>
					<div class="text-center">
						<a href="<?= get_permalink(467) ?>" class="bouton_type_1">FORUMS D'AIDE</a>
					</div>
				</div>
			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




