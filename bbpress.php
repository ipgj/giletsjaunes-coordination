<?php
/*
Template Name: Base Template
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); 

$post_status = get_post_status();

if($post_status == 'private') {
	$user_role = get_current_user_main_role();
	if($user_role == '' OR $user_role == 'subscriber') {
		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 ); exit();
	}
}
?>

	<div class="main_content_container">

		<?php 
		$main_class = '';

		// SI on est dans les pages "perso" du user
		if(bbp_is_single_user()) {
			// NAV DEP du département de l'utilisateur
			$departement_user = get_current_user_departement();
			if($departement_user != 'Aucun' AND $departement_user != '') {
				$numdep_user = strtok($departement_user,  ' ');
				$departement_mainpage_ID = get_main_dep_page_from_num($numdep_user);
			}

			// Ajouter une class au main
			$main_class = ' bbpress_userpage';

		} else {
			// Récupérer les IDs du forum actuel et du forum racine
			$forum_id = bbp_get_forum_id();
			$forum_ancestors = get_post_ancestors($forum_id);
			$forum_root_id = end($forum_ancestors);
			//$forum_root_id = wp_get_post_parent_id($forum_id);
			if($forum_root_id == 0) {
				$forum_root_id = $forum_id;
			}

			// Vérifier si ce forum est présent dans l'array de forums racines départementaux
			$dep_forum_links_array = get_dep_forum_links_array();
			$is_dep_forum = array_search($forum_root_id, array_column($dep_forum_links_array, 'forum_dep_root'));

			if($is_dep_forum) {
				// Récupérer l'ID de la page d'accueil départementale liée à ce forum
				$departement_mainpage_ID = get_corresponding_main_dep_page($forum_root_id);
			}

			// Ajouter une class au main
			$main_class = ' bbpress_dep_forum';
		}

		// Si une page d'accueil départementale a été trouvée 
		if(isset($departement_mainpage_ID)) {

			// Afficher la nav départementale
			include('templatechunk-departement-nav.php'); 

			// Vérifier si le mot de passe de la page protégée départementale a été validé
			if (!post_password_required($password_protected_child_ID)) {
				$departemental_password_is_valid = true;
			} else {
				$departemental_password_is_valid = false;
			}

			
		}

		

		$protected_icon = '';

		// Vérifier si le forum à afficher est un forum protégé
		$is_protected = get_field('contenu_protege', $forum_id);
		if($is_protected AND $is_protected[0] == 'content_is_protected') {
			$forum_is_protected = true;
			$main_class .= ' protected_forum';
			$protected_icon = ' <i class="fas fa-lock"></i>';
		} else {
			$forum_is_protected = false;
		}

		// Si le forum est public OU qu'il est privé mais que le mot de passe est valide, afficher la page
		if(!$forum_is_protected OR ($forum_is_protected AND $departemental_password_is_valid)) {
		?>

		<?php // Récuperation ID en-tête fullwidth si nécessaire
		$fullwidth_header_page = get_field('fullsize_header_to_get');
		if(isset($fullwidth_header_page) AND $fullwidth_header_page != '') { $has_fullwidth_header = true; } else { $has_fullwidth_header = false; }
		if($has_fullwidth_header) { $main_class .= ' nopaddingtop'; }
		?>

		<main class="clearfix<?= $main_class; ?>">

			<?php if($has_fullwidth_header) { echo '<div class="fullsize_head_container">'; } ?>

			<section class="entete_page">
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<?php if(bbp_is_single_user()) { echo '<h1 class="entete_surtitre">Mes discussions</h1>'; } ; ?>
							<h1 class="entry-title"><?php the_title() ?><?= $protected_icon; ?></h1>
						</header>
					</article>
				<!-- #post -->
				<?php endwhile; // end of the loop. ?>
			</section>

			<?php if($has_fullwidth_header) { ?>
				<section class="entete_content">
					<div class="icon"><?= get_field('fullsize_header_icon', $fullwidth_header_page) ?></div>
					<div class="separateur_vert"></div>
					<div class="texte">
						<?= get_field('fullsize_header_text', $fullwidth_header_page) ?>
					</div>
				</section>
			<?php } ?>

			<?php if($has_fullwidth_header) { echo '</div>'; } ?>

			<section class="forums_section">

				<?php if(!bbp_is_single_user()) { ?>
					<div class="bbpress_disclaimer">
						<?php echo get_field('disclaimer_bienseance', 'option'); ?>
					</div>
				<?php } ?>
				<div class="bbpress_content_container">
					<?php the_content(); ?>
				</div>

			</section>

		</main>

		<?php } else { ?>
			<main class="forbidden_access">
				<section>
					<i class="fas fa-lock"></i>
					<h1>Accès à ce contenu privé interdit</h1>
					<p>Vous essayez d'accéder à un contenu départemental protégé,<br/>
					veuillez valider le mot de passe départemental avant d'accéder<br/>
					à ce contenu depuis la page <a href="<?php echo get_permalink($password_protected_child_ID); ?>">Espace protégé</a>.</p>
				</section>
			</main>
		<?php } ?>

	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>
