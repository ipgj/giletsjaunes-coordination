<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Gilet_Jaune_France
 */

get_header();

// Déterminer le nom du post-type des pages du même département
$post_type_news = $post->post_type;
$post_type_page = 'page'.strstr($post_type_news, '-');

// Récupérer la page protégée du département
$args = array(
    'post_type' => $post_type_page,
);
$pages_dep = get_posts( $args );
foreach ($pages_dep as $page_dep) {
	if($page_dep->post_parent > 0) { $departement_mainpage_ID = $page_dep->post_parent; }
	if(!empty($page_dep->post_password)) {
		$password_protected_page_ID = $page_dep->ID;
	}
}

$is_protected = get_field('contenu_protege');
if(($is_protected AND $is_protected[0] == 'content_is_protected') AND post_password_required($password_protected_page_ID)) {
	// Ce post est protégé et le mot de passe n'est pas bon ?>

	<main class="forbidden_access">
		<section>
			<i class="fas fa-lock"></i>
			<h1>Accès à ce contenu privé interdit</h1>
			<p>Vous essayez d'accéder à un contenu départemental protégé,<br/>
			veuillez valider le mot de passe départemental avant d'accéder<br/>
			à ce contenu depuis la page <a href="<?php echo get_permalink($password_protected_child_ID); ?>">Espace protégé</a>.</p>
		</section>
	</main>
		
<?php } else { ?>

	<?php
	// Si une page d'accueil départementale a été trouvée 
	if(isset($departement_mainpage_ID)) {

		// Afficher la nav départementale
		include('templatechunk-ada-nav.php'); 
	} ?> 

	<div id="primary" class="content-area single-news">

		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'single-post' );

			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php } ?>

<?php
//get_sidebar();
get_footer();
