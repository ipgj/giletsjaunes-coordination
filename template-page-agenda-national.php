<?php
/*
Template Name: Page - Agenda national
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container">
		<main class="clearfix container">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>

			<section class="content_page">

				<?php
				$item_per_page = 10;

				// Récupération des Customs Post Types de tous les deps
				$dep_forum_links_array = get_dep_forum_links_array();
				foreach($dep_forum_links_array as $dep) {
					$deps['post_type'][] = 'actu-'.$dep['slug'];
					$deps['full_name'][] = $dep['num'].' - '.$dep['nom'];
				}
				// Ajout des actus ADA
				$deps['post_type'][] = 'actu-ada';
				$deps['full_name'][] = 'ADA';
				// Ajout des actus FAGJ
				$deps['post_type'][] = 'actu-fagj';
				$deps['full_name'][] = 'FAGJ';
				// Ajout des actus nationales
				$deps['post_type'][] = 'actu-nationale';
				$deps['full_name'][] = 'Actualité nationale';

				$args = array(
				    'post_type'      => $deps['post_type'],
				    'posts_per_page' => $item_per_page,
				    'order'          => 'DESC',
				    'orderby'        => 'date',
				    'paged' => $paged,
					'meta_query'	=> array(
						'relation' => 'OR',
						array(
							'key'	  	=> 'contenu_protege',
							'value'	  	=> '',
							'compare' 	=> '=',
						),
						array(
							'key'	  	=> 'contenu_protege',
							'value'	  	=> '',
							'compare' 	=> 'NOT EXISTS',
						),
					),
				 );

				$all_news = new WP_Query( $args );

				if ( $all_news->have_posts() ) :
				    while ( $all_news->have_posts() ) : $all_news->the_post(); 

				    	$post_type = get_post_type();
				    	$key = array_search($post_type, $deps['post_type']);
				    	$nom_departement = $deps['full_name'][$key];

				    	$img = get_the_post_thumbnail();
				    	if(!$img OR $img == '') { $img = '<img src="'.get_template_directory_uri().'/images/no_image.jpg"/>'; }
				    	$date = get_the_date(get_option('date_format'));
				    	
						$text_unformed = get_the_content();
						$text = wpautop($text_unformed);
						$type_evenement = get_field('type_devenement');
						$emplacement = get_field('emplacement_geographique');

						$comment_number = get_comments_number();
						if($comment_number == 0) { $comment_text = ''; }
						if($comment_number == 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaire'; }
						if($comment_number > 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaires'; }
				    	?>

				    	<div class="news_container row no-gutters shadowed_box">
				    		<div class="onglet_dep">
				    			<p><?= $nom_departement; ?></p>
				    			<div class="triangle"></div>
				    		</div>
				    		<div class="gauche col-lg-3">
				    			<?php echo $img; ?>
				    		</div>
				    		<div class="droite col-lg-9">
				    			<div class="date">> Publié le <?php echo $date; ?></div>
				    			<?php if($comment_number > 0) { ?>
				    				<div class="nb_comment"><?= $comment_text; ?></div>
				    			<?php } ?>
				    			<a class="titre <?php echo $current; ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
				    			<div class="entete">
				    				<?php if($type_evenement != '') { echo '<span class="type">'.$type_evenement.'</span>'; } ?>
				    				<?php if($type_evenement != '' AND $emplacement != '') { echo '<span class="tiret"> - </span>'; } ?>
				    				<?php if($emplacement != '') { echo '<span class="emplacement">'.$emplacement.'</span>'; } ?>
				    			</div>
				    			<div class="texte">
				    				<?php echo $text; ?>
				    			</div>
				    		</div>
				    	</div>

				    <?php endwhile;
				else :
					echo '<p class="bloc no_result">Aucune actualité n\'a été ajoutée dans ce département pour le moment...</p>';
				endif;

				wp_reset_postdata(); ?>

			</section>

			<?php $big = 999999999;
		    echo '<section class="wp_pagination">';
				echo paginate_links( array( // Plus d'info sur les arguments possibles : https://codex.wordpress.org/Function_Reference/paginate_links
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $all_news->max_num_pages
				) );
			echo '</section>'; ?>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




