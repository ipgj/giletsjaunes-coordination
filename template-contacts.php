<?php
/*
Template Name: Departement - Contacts
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container section_departement">

		<?php include('templatechunk-departement-nav.php'); ?>

		<main class="clearfix">

			<?php $parentId = $post->post_parent;
			$have_moderateur = get_field('have_moderateur', $parentId);
			if(!$have_moderateur) { ?>
				<div class="no_moderator">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/warning_icon_white.png"/>
					</div>
					<div class="texte">
						<?php echo get_field('message_aucun_moderateur', 'option'); ?>
					</div>
				</div>
			<?php } ?>

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title">Contacter les groupes Gilet Jaune : <?= explode(')', get_the_title($target_id))[1] ?></h1>
					</header>
				</article>
				<p><?= get_field('texte_entete_contacts_liens', 'option'); ?></p>
			</section>

			<div class="boxed_content shadowed_box">

				<?php
				$pinfo_image = get_field('pinned_info_image');
				$pinfo_texte = get_field('pinned_info_texte'); 
				
				if($pinfo_texte != '') { ?>
					<div class="pinned_info">
						<div class="pin_container">
							<img class="pin" src="<?php echo get_template_directory_uri(); ?>/images/pin_icon.png"/>
						</div>
						<div class="content">
							<?php if($pinfo_image != '') { ?>
								<div class="img_container">
									<img src="<?php echo $pinfo_image['sizes']['medium']; ?>"/>
								</div>
							<?php } ?>
							<div class="texte_container">
								<?php echo $pinfo_texte; ?>
							</div>
						</div>
					</div>
				<?php } ?>

				<section class="contacts_lieux">
					<?php
					if(have_rows('lieux_physiques')) { ?>					
						<?php while(have_rows('lieux_physiques')) : the_row(); 
							$lieu = get_sub_field('lieu');
							$description = get_sub_field('description');
							?>

							<div class="lieu_container">
								<div class="icon">
									<i class="fas fa-home"></i>
								</div>
								<div class="contenu">
									<h4 class="lieu"><?php echo $lieu ?></h4>
									<div class="description">
										<?php echo $description ?>
									</div>
								</div>
							</div>

						<?php endwhile; 
					} ?>	

				</section>

				<section class="liste_liens contacts_groupesfb">

					<h3 class="titre_section">Groupes Facebook</h3>
					<?php
					if(have_rows('groupes_facebook')) { ?>					
						<?php while(have_rows('groupes_facebook')) : the_row(); 
							$nom_groupe = get_sub_field('nom_du_groupe');
							$lien = get_sub_field('lien');
							?>

							<a href="<?php echo $lien; ?>" target="_blank" class="ligne flex">
								<div class="icon">
									<i class="fab fa-facebook-square"></i>
								</div>
								<div class="texte">
									<p class="titre"><?php echo $nom_groupe; ?></p>
									<p class="lien_texte"><?php echo $lien; ?></p>
								</div>
							</a>

						<?php endwhile; 
					} else { echo '<p class="bloc no_result">Aucun groupe facebook n\'a été ajouté dans cette section pour le moment...<br/>Si vous connaissez un, faites le savoir à vos modérateurs.</p>'; } ?>	

				</section>

				<section class="liste_liens contacts_sites_locaux">

					<h3 class="titre_section">Sites web locaux</h3>
					<?php
					if(have_rows('site_web_locaux')) { ?>					
						<?php while(have_rows('site_web_locaux')) : the_row(); 
							$nom_site = get_sub_field('nom_du_site');
							$lien = get_sub_field('lien');
							?>

							<a href="<?php echo $lien; ?>" target="_blank" class="ligne flex">
								<div class="icon">
									<i class="fas fa-globe"></i>
								</div>
								<div class="texte">
									<p class="titre"><?php echo $nom_site; ?></p>
									<p class="lien_texte"><?php echo $lien; ?></p>
								</div>
							</a>

						<?php endwhile; 
					} else { echo '<p class="bloc no_result">Aucun site web local n\'a été ajouté dans cette section pour le moment...<br/>Si vous connaissez un, faites le savoir à vos modérateurs.</p>'; } ?>	

				</section>

				<section class="contacts_utiles">

					<h3 class="titre_section">Contacts utiles</h3>
					<?php
					if(have_rows('contacts_utiles')) { ?>					
						<?php while(have_rows('contacts_utiles')) : the_row(); 
							$nom = get_sub_field('nom');
							$description = get_sub_field('description');
							$telephone = get_sub_field('telephone');
							$email = get_sub_field('e-mail');
							?>

							<div class="contact_utile_container">
								<div class="contenu">
									<h4 class="nom"><?php echo $nom ?></h4>
									<div class="description">
										<?php echo $description ?>
									</div>
									<div class="moyens_contact">
										<?php if($telephone != '') { echo '<a href="tel:'.$telephone.'" class="telephone"><i class="fas fa-phone"></i> '.$telephone.'</span>'; } ?>
					    				<?php if($telephone != '' AND $email != '') { echo '<span class="tiret"> - </span>'; } ?>
					    				<?php if($email != '') { echo '<a href="mailto:'.$email.'" class="email"><i class="fas fa-envelope"></i> '.$email.'</span>'; } ?>
									</div>
								</div>
							</div>

						<?php endwhile; 
					} else { echo '<p class="bloc no_result">Aucun contact n\'a été ajouté dans cette section pour le moment...</p>'; } ?>	

				</section>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




