
	<?php $user_main_role = get_current_user_main_role();

	$unread_count = do_shortcode('[bbpm-unread-count]');
	if($unread_count == 0) {
		$unread_display = '';
	} else {
		$unread_display = '<div class="unread_display"><span>'.$unread_count.'</span></div>';
	}
	if($unread_count > 99) { $unread_display = '<div class="unread_display"><span class="toomuch">99+</span></div>'; }
	?>
	<div class="account_button">
		<?php echo get_avatar( get_current_user_id(), 96 ); ?>
		<?= $unread_display; ?>
		<div class="user_menu">
			<div class="text">
				<span class="pretext">Connecté en tant que :</span>
				<span><?php global $current_user; wp_get_current_user(); echo $current_user->user_login; ?></span>
			</div>
			<div class="separator"></div>
			<?php $departement_user = get_current_user_departement();
			if($departement_user != 'Aucun' AND $departement_user != '') { ?>
				<a href="<?php echo get_site_url().'/'.get_current_user_departement_slug(); ?>"><i class="fas fa-map-marker-alt"></i> <?php echo $departement_user; ?></a>
			<?php } ?>
			<a href="<?php echo get_site_url().'/mon-compte'; ?>"><i class="fas fa-user"></i> Mon compte</a>
			<?php $bbp_user_id = bbp_get_current_user_id();
			if(isset($bbp_user_id)) { ?>
				<a href="<?php echo bbp_user_profile_url($bbp_user_id); ?>"><i class="fas fa-comments"></i> Mes discussions <?= $unread_display; ?></a>
			<?php } ?>
			<?php if(in_array($user_main_role, array('administrator', 'administrateur', 'admin', 'moderateur'))) {
			/*if($user_main_role == 'administrator' OR $user_main_role == 'administrateur' OR $user_main_role == 'moderateur' OR $user_main_role == 'moderateur_forums') {*/ ?>
				<a href="<?php echo get_admin_url(); ?>" target="_blank"><i class="fas fa-tools"></i> Administration</a>
				<a href="<?php echo get_permalink(613); ?>"><i class="fas fa-user-tie"></i> Forum Staff</a>
			<?php } ?>
			<a href="<?php echo wp_logout_url( $redirect ); ?>"><i class="fas fa-sign-out-alt"></i> Déconnexion</a>
		</div>
	</div>