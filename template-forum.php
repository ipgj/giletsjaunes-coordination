<?php
/*
Template Name: Departement - Forum
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container section_departement">

		<?php include('templatechunk-departement-nav.php'); ?>
		
		<main class="clearfix">

			<?php $parentId = $post->post_parent;
			$have_moderateur = get_field('have_moderateur', $parentId);
			if(!$have_moderateur) { ?>
				<div class="no_moderator">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/warning_icon_white.png"/>
					</div>
					<div class="texte">
						<?php echo get_field('message_aucun_moderateur', 'option'); ?>
					</div>
				</div>
			<?php } ?>

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title">Les forums Gilet Jaune : <?= explode(')', get_the_title($target_id))[1] ?></h1>
					</header>
				</article>
				<p><?= get_field('texte_entete_forums', 'option'); ?></p>
			</section>

			<div class="boxed_content shadowed_box">

				<?php
				$pinfo_image = get_field('pinned_info_image');
				$pinfo_texte = get_field('pinned_info_texte'); 
				
				if($pinfo_texte != '') { ?>
					<div class="pinned_info">
						<div class="pin_container">
							<img class="pin" src="<?php echo get_template_directory_uri(); ?>/images/pin_icon.png"/>
						</div>
						<div class="content">
							<?php if($pinfo_image != '') { ?>
								<div class="img_container">
									<img src="<?php echo $pinfo_image['sizes']['medium']; ?>"/>
								</div>
							<?php } ?>
							<div class="texte_container">
								<?php echo $pinfo_texte; ?>
							</div>
						</div>
					</div>
				<?php } ?>

				<section class="forum_container">
					<?php 
					$parent_page = get_post($post->post_parent);
					$parent_page_ID = $parent_page->ID;
					$corresponding_forum_id = get_corresponding_forum_dep_root($parent_page_ID);
					$bbpress_shortcode = '[bbp-single-forum id='.$corresponding_forum_id.']';

					if($corresponding_forum_id != '' AND $corresponding_forum_id != 0) {
						echo do_shortcode($bbpress_shortcode); 
					} else {
						echo '<p class="bloc no_result">Aucune forum n\'est connecté à ce département pour le moment...</p>';
					}
					?>
				</section>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




