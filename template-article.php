<?php
/*
Template Name: Base Article
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container">
		<main class="clearfix container articled">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>

			<section class="content_page">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content-page-notitle', 'content-page-notitle' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

				<?php
				if(have_rows('blocs')) { ?>	

					<div class="indexblocs_container row">

						<?php while(have_rows('blocs')) : the_row(); 
							$titre = get_sub_field('titre');
							$texte = get_sub_field('texte');
							$lien = get_sub_field('lien');
							?>

							<div class="col-lg-6">
								<div class="bloc_container shadowed_box">
									<a href="<?= $lien; ?>"><h4 class="titre"><?= $titre ?></h4></a>
									<div class="description">
										<?= $texte ?>
									</div>
									<div class="text-left">
										<a href="<?= $lien; ?>" class="">Lire la suite</a>
									</div>
								</div>
							</div>

						<?php endwhile; ?>

					</div>

				<?php } ?>

			</section>

		</main>

		<div class="back_to_parent container p-0 mt-4">
			<a href="<?= get_permalink(wp_get_post_parent_id(get_the_ID())); ?>" class="bouton_type_1">Retour</a>
		</div>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




