<?php
/*
Template Name: Mon Compte
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

check_page_security();
require_once('includes/update-profile.php');

if(!empty($_POST['delete_account_submit'])) {

	if(!empty($_POST['delete_account_check'])) {
		$delete_check = $_POST['delete_account_check'];

		if($delete_check == 'SUPPRIMER') {

			if (is_user_logged_in()) {

				// Verify that the user intended to take this action.
				/*if ( ! wp_verify_nonce( 'delete_account' ) ) {
					$delete_account_error = 'L\'utilisateur n\'est pas à l\'origine de cette action.';
					return;
				}*/

				require_once(ABSPATH.'wp-admin/includes/user.php' );
				$current_user = wp_get_current_user();
				wp_delete_user($current_user->ID, 24);

				wp_redirect( home_url().'?delete_account=true' );
				exit;
			}

		} else {
			$delete_account_error = 'Vous n\'avez pas entré le bon mot de vérification, veuillez réessayer.';
		}
	} else {
		$delete_account_error = 'Le champ de vérification est vide.';
	}
}
?>

<?php get_header(); ?>

	<div class="main_content_container">

		<?php 
		$departement_user = get_current_user_departement();
		if($departement_user != 'Aucun' AND $departement_user != '') {
			$numdep_user = strtok($departement_user,  ' ');
			$departement_mainpage_ID = get_main_dep_page_from_num($numdep_user);
			include('templatechunk-departement-nav.php');
		} ?>

		<main class="clearfix">

			<section class="entete_page">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/pinned_info_hideable', 'pinned_info_hideable' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

			</section>

			<section class="messages">
				<?php if( !empty( $_GET['updated'] ) ): ?>
					<div class="success"><?php _e('Votre profil a été mis à jour avec succès.', 'textdomain'); ?></div>
				<?php endif; ?>

				<?php if( !empty( $_GET['validation'] ) ): ?>
					
					<?php if( $_GET['validation'] == 'emailnotvalid' ): ?>
						<div class="error"><?php _e('L\'adresse e-mail n\'est pas valide.', 'textdomain'); ?></div>
					<?php elseif( $_GET['validation'] == 'emailexists' ): ?>
						<div class="error"><?php _e('Cette adresse e-mail est déjà utilisée.', 'textdomain'); ?></div>
					<?php elseif( $_GET['validation'] == 'passwordmismatch' ): ?>
						<div class="error"><?php _e('Les 2 mots de passe ne correspondent pas.', 'textdomain'); ?></div>
					<?php elseif( $_GET['validation'] == 'unknown' ): ?>
						<div class="error"><?php _e('Une erreur est survenue, contactez le webmaster si elle persiste.', 'textdomain'); ?></div>
					<?php endif; ?>

				<?php endif; ?>
			</section>

			<?php if(get_current_user_main_role() == 'administrateur' OR get_current_user_main_role() == 'administrator') { echo '<style>.wpua-edit p.submit { display: block !important; margin: 0 !important; }</style>'; } ?>
			<section class="choix_avatar_container">
				<h3 class="titre_section"><?php _e('Choisir votre avatar', 'textdomain'); ?></h3>
				<div class="inputs_bloc">
					<?php echo do_shortcode('[avatar_upload]'); ?>
				</div>
			</section>

			<?php get_template_part('parts/dashboard/user'); ?>

			<?php if ( !have_posts() ) get_template_part( 'parts/notice/no-posts' ); ?>

			<?php while (have_posts()) : the_post(); ?>

			<section id="profile_editor_container">
			
				<div class="wrap">

					<?php get_template_part( 'parts/dashboard/edit-profile/intro' ); ?>

					<?php 
				    $departements = array( 'Aucun','01 - Ain','02 - Aisne','03 - Allier','04 - Alpes-de-Haute-Provence','05 - Hautes-alpes','06 - Alpes-maritimes','07 - Ardèche','08 - Ardennes','09 - Ariège','10 - Aube','11 - Aude','12 - Aveyron','13 - Bouches-du-Rhône','14 - Calvados','15 - Cantal','16 - Charente','17 - Charente-maritime','18 - Cher','19 - Corrèze','2a - Corse-du-sud','2b - Haute-Corse','21 - Côte-d\'Or','22 - Côtes-d\'Armor','23 - Creuse','24 - Dordogne','25 - Doubs','26 - Drôme','27 - Eure','28 - Eure-et-loir','29 - Finistère','30 - Gard','31 - Haute-garonne','32 - Gers','33 - Gironde','34 - Hérault','35 - Ille-et-vilaine','36 - Indre','37 - Indre-et-loire','38 - Isère','39 - Jura','40 - Landes','41 - Loir-et-cher','42 - Loire','43 - Haute-loire','44 - Loire-atlantique','45 - Loiret','46 - Lot','47 - Lot-et-garonne','48 - Lozère','49 - Maine-et-loire','50 - Manche','51 - Marne','52 - Haute-marne','53 - Mayenne','54 - Meurthe-et-moselle','55 - Meuse','56 - Morbihan','57 - Moselle','58 - Nièvre','59 - Nord','60 - Oise','61 - Orne','62 - Pas-de-calais','63 - Puy-de-dôme','64 - Pyrénées-atlantiques','65 - Hautes-Pyrénées','66 - Pyrénées-orientales','67 - Bas-rhin','68 - Haut-rhin','69 - Rhône','70 - Haute-saône','71 - Saône-et-loire','72 - Sarthe','73 - Savoie','74 - Haute-savoie','75 - Paris','76 - Seine-maritime','77 - Seine-et-marne','78 - Yvelines','79 - Deux-sèvres','80 - Somme','81 - Tarn','82 - Tarn-et-garonne','83 - Var','84 - Vaucluse','85 - Vendée','86 - Vienne','87 - Haute-vienne','88 - Vosges','89 - Yonne','90 - Territoire de belfort','91 - Essonne','92 - Hauts-de-seine','93 - Seine-Saint-Denis','94 - Val-de-marne','95 - Val-d\'oise','971 - Guadeloupe','972 - Martinique','973 - Guyane','974 - La réunion','976 - Mayotte',  );
				    $default	= array( 'departement' => 'Aucun');
				    $departement_actuel = get_user_meta(
					    get_current_user_id(),
					    $key = 'departement',
					    $single = false
					); ?>

					<?php $current_user = wp_get_current_user(); ?>

					<form method="post" id="adduser" action="<?php the_permalink(); ?>">

					    <h3 class="titre_section"><?php _e('Informations personnelles', 'textdomain'); ?></h3>

					    <div class="inputs_bloc">

					    	<p class="cant_change_ident"><?php _e('Il n\'est pas possible de changer d\'identifiant.', 'textdomain'); ?></p>
						    
					        <div class="labelinput">
						        <label for="first-name"><?php _e('Identifiant', 'textdomain'); ?></label>
						        <input class="text-input" name="user_login" type="text" id="user_login" value="<?php the_author_meta( 'user_login', $current_user->ID ); ?>" disabled/>
						    </div>
		
					    	<div class="labelinput">
						        <label for="email"><?php _e('E-mail', 'textdomain'); ?></label>
						        <input class="text-input" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
						    </div>
					    

					    	<div class="labelinput">
						        <label for="last-name"><?php _e('Pseudonyme', 'textdomain'); ?></label>
						        <input class="text-input" name="nickname" type="text" id="last-name" value="<?php the_author_meta( 'nickname', $current_user->ID ); ?>" />
						    </div>

					    	<div class="labelinput">
					    	<label for="birth-date-day">Votre département</label></th>
					   			<select id="select-departement" name="departement">
					   			 	<?php foreach ( $departements as $departement ) {
										printf( '<option value="%1$s" %2$s>%1$s</option>', $departement, selected( $departement_actuel[0], $departement, false ) );
									} ?>
						   		</select>
						   	</div>

							<h4 class="publicinfo_titre">Informations visibles publiquement</h4>
							
							<div class="publicinfo">
								<p><strong><?php _e('Nom affiché :', 'textdomain'); ?></strong> <?php echo $current_user->display_name; ?></p>
								<?php if($departement_actuel[0] != '' AND $departement_actuel[0] != 'Aucun') { ?>
									<p><strong><?php _e('Département :', 'textdomain'); ?></strong> <?php echo $departement_actuel[0]; ?></p>
								<?php } ?>
							</div>

						</div>



						<h3 class="titre_section"><?php _e('Changer de mot de passe', 'textdomain'); ?></h3>

						<div class="inputs_bloc">

						    <p><?php _e('Si vous laissez ces 2 champs vides, votre mot de passe ne sera pas modifié.', 'textdomain'); ?></p>

						    <input type="password" name="pass_fake" id="pass_fake" class="hidden" autocomplete="off" style="display: none;" />
						    <div class="labelinput">
						        <label for="pass1"><?php _e('Mot de passe', 'profile'); ?> </label>
						        <input class="text-input" name="pass1" type="password" id="pass1" autocomplete="off" value="" />
						    </div>
						    <div class="labelinput">
						        <label for="pass2"><?php _e('Confirmation', 'profile'); ?></label>
						        <input class="text-input" name="pass2" type="password" id="pass2" autocomplete="off" value="" />
						    </div>

						</div>

						<p class="form-submit">
					        <input name="updateuser" type="submit" id="updateuser" class="submit bouton_type_1" value="<?php _e('Mettre à jour le profil', 'textdomain'); ?>" />
					        <?php wp_nonce_field( 'update-user' ) ?>
					        <input name="honey-name" value="" type="text" style="display:none;"></input>
					        <input name="action" type="hidden" id="action" value="update-user" />
					    </p><!-- .form-submit -->

					</form><!-- #adduser -->

					<form method="post" id="delete_self" action="<?php the_permalink(); ?>#delete_account">

						<h3 id="delete_account" class="titre_section"><?php _e('Supprimer mon compte', 'textdomain'); ?></h3>

					    <div class="inputs_bloc">
					    	<p><?php _e('Si vous souhaitez supprimer votre compte, veuillez taper le mot <strong>"SUPPRIMER"</strong> dans le champ ci-dessous et valider en cliquant sur le bouton rouge.', 'textdomain'); ?></p>
					    	<div class="error">
								<?php if($delete_account_error) { echo $delete_account_error; } ?>
							</div>
					    	<div class="delete_account_container">
					    		<input class="text-input" name="delete_account_check" type="text" id="delete_account_check" autocomplete="off" value="" placeholder="<?php _e('Mot de vérification...', 'textdomain'); ?>" />
					    		<input name="delete_account_submit" type="submit" id="updateuser" class="submit bouton_type_1 delete_account_button" value="<?php _e('SUPPRIMER MON COMPTE DÉFINITIVEMENT', 'textdomain'); ?>" />
					    	</div>
					    </div>

					</form>

				</div>

			</section>

			<?php endwhile; ?>

		</main>
	</div>

	<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>