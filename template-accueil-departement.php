<?php
/*
Template Name: Accueil département
Template Post Type: post, page, product
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); 

// Récupération des liens des pages enfants
$args = array(
    'post_parent' => $post->ID,
    'posts_per_page' => -1,
    'post_type' => 'any',
    );
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();
	$child_template = get_page_template_slug();
    if($child_template == 'template-actualites.php') { $childlink_actus = get_permalink(); $childid_actus = get_the_id(); }
    if($child_template == 'template-comptes-rendus.php') { $childlink_compterendus = get_permalink(); $childid_compterendus = get_the_id(); }
    if($child_template == 'template-documents.php') { $childlink_documents = get_permalink(); $childid_documents = get_the_id(); }
    if($child_template == 'template-presse.php') { $childlink_presse = get_permalink(); $childid_presse = get_the_id(); }
    if($child_template == 'template-contacts.php') { $childlink_contacts = get_permalink(); $childid_contacts = get_the_id(); }
    if($child_template == 'template-forum.php') { $childlink_forum = get_permalink(); $childid_forum = get_the_id(); }
endwhile;
endif;
wp_reset_postdata();
?>

	<div class="main_content_container section_departement">

		<?php include('templatechunk-departement-nav.php'); ?>

		<main class="clearfix">

			<?php $have_moderateur = get_field('have_moderateur');
			if(!$have_moderateur) { ?>
				<div class="no_moderator">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/warning_icon_white.png"/>
					</div>
					<div class="texte">
						<?php echo get_field('message_aucun_moderateur', 'option'); ?>
					</div>
				</div>
			<?php } ?>

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title">Espace départemental des Gilets Jaunes : <?= explode(')', get_the_title($target_id))[1] ?></h1>
					</header>
				</article>
				<p><?= get_field('texte_entete_accueil', 'option'); ?></p>
			</section>

			<div class="raccourcis_entete row">
				<div class="acces_rapide_container col-lg-9">
					<h2>Accès rapides</h2>
					<div class="accesrapide_container">
						<div class="row">
							<?php
							foreach($pages_nav_dep as $page_dep) {
								if($page_dep['nom'] == 'Agenda / actus') { $icon = get_template_directory_uri().'/images/outils_dep_icons/agenda_actions.png'; }
								elseif($page_dep['nom'] == 'Documents / CR') { $icon = get_template_directory_uri().'/images/outils_dep_icons/documents_cr.png'; }
								elseif($page_dep['nom'] == 'Affiches / tracts') { $icon = get_template_directory_uri().'/images/outils_dep_icons/affiches_tracts.png'; }
								elseif($page_dep['nom'] == 'Médias / presse') { $icon = get_template_directory_uri().'/images/outils_dep_icons/presse_medias.png'; }
								elseif($page_dep['nom'] == 'Contacts / liens') { $icon = get_template_directory_uri().'/images/outils_dep_icons/contacts_liens.png'; }
								elseif($page_dep['nom'] == 'Forums') { $icon = get_template_directory_uri().'/images/outils_dep_icons/forums.png'; }
								elseif($page_dep['nom'] == 'Espace protégé') { $icon = get_template_directory_uri().'/images/outils_dep_icons/espace_protege.png'; }
								else { $icon = get_template_directory_uri().'/images/outils_dep_icons/unknow.png'; }
							?>
								<div class="col-6 col-sm-4 col-md-3">
									<a class="accesrapide_link" href="<?= $page_dep['lien'] ?>">
										<img class="img-fluid" src="<?= $icon ?>" />
										<p><?= $page_dep['nom'] ?></p>
									</a>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="carte_container col-lg-3">
					<h2>Carte des groupes</h2>
					<p>Zoomer sur la carte et cliquer sur les puces pour trouver des groupes</p>
					<div class="iframe_container">
						<img class="img-fluid" src="<?= get_template_directory_uri(); ?>/images/apercu_carte_groupes_petite.jpg" />
					</div>
					<a href="<?= get_permalink(102199); ?>" class="bouton_type_1">Voir la carte</a>
				</div>
			</div>

			<div class="corps_accueildep">

				<?php $pinfo_image = get_field('pinned_info_image');
				$pinfo_texte = get_field('pinned_info_texte'); 
				$sidebar_zone_class = '';
				if($pinfo_texte != '') { $sidebar_zone_class = ' has_pinned_info'; } ?>

				<div class="sidebar_zone row no-gutters<?= $sidebar_zone_class ?>">

					<div class="main_content col-lg-9">

						
						<?php if($pinfo_texte != '') { ?>
							<div class="pinned_info">
								<div class="pin_container">
									<img class="pin" src="<?php echo get_template_directory_uri(); ?>/images/pin_icon.png"/>
								</div>
								<div class="content">
									<?php if($pinfo_image != '') { ?>
										<div class="img_container">
											<img src="<?php echo $pinfo_image['sizes']['medium']; ?>"/>
										</div>
									<?php } ?>
									<div class="texte_container">
										<?php echo $pinfo_texte; ?>
									</div>
								</div>
							</div>
						<?php } ?>

						<section class="liste_news last_news">

							<h3 class="titre_section">Dernières actualités</h3>

							<?php
							$post_data = get_post($post);
							$post_slug = $post_data->post_name;
							$post_type_name = 'actu'.strstr($post_slug, '-');
							$nb_to_display = get_field('nombre_actus');

							$args = array(
							    'post_type'      => $post_type_name,
							    'posts_per_page' => $nb_to_display,
							    'order'          => 'DESC',
							    'orderby'        => 'date',
							    'meta_query'	=> array(
									'relation' => 'OR',
									array(
										'key'	  	=> 'contenu_protege',
										'value'	  	=> '',
										'compare' 	=> '=',
									),
									array(
										'key'	  	=> 'contenu_protege',
										'value'	  	=> '',
										'compare' 	=> 'NOT EXISTS',
									),
								),
							 );

							$news = new WP_Query( $args );

							if ( $news->have_posts() ) {
							    while ( $news->have_posts() ) : $news->the_post();

							    	$img = get_the_post_thumbnail();
							    	if(!$img OR $img == '') { $img = '<img src="'.get_template_directory_uri().'/images/no_image.jpg"/>'; }
							    	$date = get_the_date(get_option('date_format'));
									$text_unformed = get_the_content();
									$text = wpautop($text_unformed);
									$type_evenement = get_field('type_devenement');
									$emplacement = get_field('emplacement_geographique');

									$comment_number = get_comments_number();
									if($comment_number == 0) { $comment_text = ''; }
									if($comment_number == 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaire'; }
									if($comment_number > 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaires'; }

							    	$page_link_ID = get_the_ID(); 
							    	$current = '';
							    	if($page_link_ID == $current_page_ID) { $current = 'active'; } ?>
							    	<div class="news_container row no-gutters">
							    		<div class="gauche col-lg-3">
							    			<?php echo $img; ?>
							    		</div>
							    		<div class="droite col-lg-9">
							    			<div class="date">> Publié le <?php echo $date; ?></div>
							    			<?php if($comment_number > 0) { ?>
							    				<div class="nb_comment"><?= $comment_text; ?></div>
							    			<?php } ?>
							    			<a class="titre <?php echo $current; ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
							    			<div class="entete">
							    				<?php if($type_evenement != '') { echo '<span class="type">'.$type_evenement.'</span>'; } ?>
							    				<?php if($type_evenement != '' AND $emplacement != '') { echo '<span class="tiret"> - </span>'; } ?>
							    				<?php if($emplacement != '') { echo '<span class="emplacement">'.$emplacement.'</span>'; } ?>
							    			</div>
							    			<div class="texte">
							    				<?php echo $text; ?>
							    			</div>
							    		</div>
							    	</div>

							    	<div class="news_separateur"></div>
							        
							    <?php endwhile;
							} else {
								echo '<p class="no_result">Aucune actualité n\'a été ajoutée dans ce département</p>';
							}

							wp_reset_postdata();
							?>

							<div class="text-center"><a href="<?php echo $childlink_actus; ?>" class="bouton_type_1">TOUTES LES ACTUALITÉS</a></div>

						</section>

					</div>

					<div class="sidebar_container col-lg-3">

						<div class="widget last_compterendu liste_fichiers">

							<h4 class="sidebar_title">ADMINS DU DÉPARTEMENT</h4>

							<?php 
							$numero_departement = get_corresponding_num(get_the_ID());
							$args = array(
							    'role'    => 'admin_dpt_'.$numero_departement,
							    'orderby' => 'user_nicename',
							    'order'   => 'ASC'
							);
							$users = get_users( $args );
							foreach ( $users as $user ) { ?>
								<a class="nom_admin lien_texte" href="<?= esc_html(bbp_user_profile_url($user->ID)); ?>"><div class="ligne"><?= esc_html($user->display_name); ?></div></a>
							<?php } ?>

							<div class="separateur"></div>

						</div>

						<?php $nb_to_display = get_field('nombre_de_comptes_rendus'); $i = 0;
						if($nb_to_display > 0) { ?>

							<div class="widget last_compterendu liste_fichiers">

								<h4 class="sidebar_title">DERNIERS C.R.</h4>

								<?php 
								if(have_rows('comptes_rendus_liste', $childid_compterendus)) : while(have_rows('comptes_rendus_liste', $childid_compterendus) AND ($i < $nb_to_display)) : the_row(); 
									$surtitre = get_sub_field('surtitre');
									$titre = get_sub_field('titre_lien');
									$fichier = get_sub_field('fichier');
									if($fichier['subtype'] == 'pdf') { $icon = 'icon_pdf.png'; }
									elseif($fichier['subtype'] == 'jpg') { $icon = 'icon_jpg.png'; }
									elseif($fichier['subtype'] == 'png') { $icon = 'icon_png.png'; }
									elseif($fichier['subtype'] == 'vnd.openxmlformats-officedocument.wordprocessingml.document') { $icon = 'icon_word.png'; }
									else { $icon = 'icon_file.png'; }
								?>

								<a href="<?php echo $fichier['url']; ?>" target="_blank" class="ligne">
									<div class="file_icon"><img src="<?php echo get_template_directory_uri().'/images/'.$icon; ?>"/></div>
									<div class="texte">
										<p class="surtitre"><?php echo $surtitre; ?></p>
										<p class="titre"><?php echo $titre; ?></p>
									</div>
								</a>

								<?php $i++; endwhile; else :
									echo '<p class="no_result">Aucun compte-rendu n\'a été ajouté dans ce département</p>';
								endif; ?>

								<div class="widget_footer">
									<a href="<?php echo $childlink_compterendus; ?>" class="bouton_sidebar">TOUS LES COMPTES-RENDUS</a>
								</div>

								<div class="separateur"></div>

							</div>

						<?php } ?>

						<?php $nb_to_display = get_field('nombre_de_documents'); $i = 0;
						if($nb_to_display > 0) { ?>

							<div class="widget last_compterendu liste_fichiers">

								<h4 class="sidebar_title">DERNIERS DOCUMENTS</h4>

								<?php 
								if(have_rows('documents_liste', $childid_documents)) : while(have_rows('documents_liste', $childid_documents) AND ($i < $nb_to_display)) : the_row(); 
									$surtitre = get_sub_field('surtitre');
									$titre = get_sub_field('titre_lien');
									$fichier = get_sub_field('fichier');
									if($fichier['subtype'] == 'pdf') { $icon = 'icon_pdf.png'; }
									elseif($fichier['subtype'] == 'jpg') { $icon = 'icon_jpg.png'; }
									elseif($fichier['subtype'] == 'png') { $icon = 'icon_png.png'; }
									elseif($fichier['subtype'] == 'vnd.openxmlformats-officedocument.wordprocessingml.document') { $icon = 'icon_word.png'; }
									else { $icon = 'icon_file.png'; }
								?>

								<a href="<?php echo $fichier['url']; ?>" target="_blank" class="ligne">
									<div class="file_icon"><img src="<?php echo get_template_directory_uri().'/images/'.$icon; ?>"/></div>
									<div class="texte">
										<p class="surtitre"><?php echo $surtitre; ?></p>
										<p class="titre"><?php echo $titre; ?></p>
									</div>
								</a>

								<?php $i++; endwhile; else :
									echo '<p class="no_result">Aucun document n\'a été ajouté dans ce département</p>';
								endif; ?>

								<div class="widget_footer">
									<a href="<?php echo $childlink_documents; ?>" class="bouton_sidebar">TOUS LES DOCUMENTS</a>
								</div>

								<div class="separateur"></div>

							</div>

						<?php } ?>

						<?php $nb_to_display = get_field('nombre_de_liens_presse'); $i = 0;
						if($nb_to_display > 0) { ?>

							<div class="widget last_presse liste_liens">

								<h4 class="sidebar_title">DANS LA PRESSE</h4>

								<?php
								if(have_rows('liens', $childid_presse)) : while(have_rows('liens', $childid_presse) AND ($i < $nb_to_display)) : the_row(); 
									$date = get_sub_field('date_de_publication');
									$nom_media = get_sub_field('nom_du_media');
									$titre_lien = get_sub_field('titre_du_lien');
									$lien = get_sub_field('lien');
									/*$lien_char_max = 50;
									$lien = strlen($lien) > $lien_char_max ? substr($lien,0,$lien_char_max)."..." : $lien;*/
									?>

									<a href="<?php echo $lien; ?>" target="_blank" class="ligne">
										<div class="texte">
											<p class="surtitre"><span class="date"><?php echo $date; ?></span> - <span class="type"><?php echo $nom_media; ?></span></p>
											<p class="titre"><?php echo $titre_lien; ?></p>
											<p class="lien_texte"><?php echo $lien; ?></p>
										</div>
									</a>

								<?php $i++; endwhile; else :
									echo '<p class="no_result">Aucun lien presse n\'a été ajouté dans ce département</p>';
								endif; ?>	

								<div class="widget_footer">
									<a href="<?php echo $childlink_presse; ?>" class="bouton_sidebar">TOUS LES LIENS PRESSE</a>
								</div>

								<div class="separateur"></div>

							</div>

						<?php } ?>

					</div>

				</div>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




