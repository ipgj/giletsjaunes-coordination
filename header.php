<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gilet_Jaune_France
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Les Gilets Jaunes">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,400i,700" rel="stylesheet"> 

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php $departement_user = get_current_user_departement();
if($departement_user != 'Aucun' AND $departement_user != '') {
	$has_dep_nav = ' has_dep_nav';
} else {
	$has_dep_nav = '';
} ?>
<div id="page" class="site<?= $has_dep_nav ?>">
	
	<div class="navbar_container">

		<div class="topbar">

		</div>

		<?php if(isset($_GET['loggedout']) AND $_GET['loggedout'] == true) { ?>
			<div class="logout_confirm">
				<p>Déconnexion confirmée... à bientôt !</p>
			</div>

		<?php } ?>

		<a class="navbar-brand floating" href="/">
			<img src="<?php echo get_template_directory_uri(); ?>/images/logo_portailgj_black2_NEW.png" alt="Logo black"/>
			<div class="texte">
				Portail<br/>
				Gilets Jaunes
			</div>
		</a>
		<nav class="navbar navbar-expand-lg">

			<div class="puller"></div>

			<div class="usermenu_resp">
				<?php if ( is_user_logged_in() ) { 
					include('templatechunk-usermenu.php'); 
				} else { 
					if($post->ID == '188') { $active = ' active'; } else { $active = ''; }
					$return_url = explode('?',$_SERVER['REQUEST_URI']);
					$return_url = $return_url[0];
					?>
				    <a class="lien_connexion icon<?php echo $active ?>" href="<?php echo get_page_link('188').'?return_url='.$return_url; ?>"><i class="fas fa-sign-in-alt"></i><span style="display: none;">Connexion</span></a>
			<?php } ?>

			</div>

			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon">
					<div class="toggler-bar top-bar"></div>
					<div class="toggler-bar middle-bar"></div>
					<div class="toggler-bar bottom-bar"></div>
				</span>
			</button>
			

			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<div class="container">
					<a class="navbar-brand" href="/">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo_portailgj_black2.png" alt="Logo black 2"/>
						<div class="texte">
							Portail<br/>
							Gilets Jaunes
						</div>
					</a>

					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
						'container'       => 'div', 
						'container_class' => '', 
						'container_id'    => 'nav-container',
						'menu_class'      => 'navbar-nav mr-auto', 
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'         => new Primary_Walker_Nav_Menu()
					) );
					?>

					<div class="nav_spacer"></div>

					<div class="right_container">
						<?php if ( is_user_logged_in() ) { 
							include('templatechunk-usermenu.php'); 
						} else { 
							if($post->ID == '188') { $active = ' active'; } else { $active = ''; }
							$return_url = explode('?',$_SERVER['REQUEST_URI']);
							$return_url = $return_url[0];
							?>
						    <a class="lien_connexion txt<?php echo $active ?>" href="<?php echo get_page_link('188').'?return_url='.$return_url; ?>">Connexion / Inscription</a>
						<?php } ?>
					</div>
					<div class="right_container">
						<div class="help_button">
							<i class="far fa-question-circle"></i>
							<div class="help_menu">
								<a href="<?= get_permalink(655); ?>"><i class="fas fa-chart-area"></i> Statistiques</a>
								<a href="<?= get_permalink(465); ?>"><i class="fas fa-question"></i> F.A.Q.</a>
								<a href="<?= get_permalink(103957); ?>"><i class="far fa-list-alt"></i> Notes de mise à jour</a>
								<a href="<?= get_permalink(467); ?>"><i class="fas fa-comments"></i> Forums d'aide</a>
							</div>
						</div>
					</div>
				</div>

			</div>

			
		</nav>
    
	</div>
	<!-- FIN NAV -->

	<div class="subnav">
		<div class="container">
			<p> <?= get_field('phrase_sousnav', 'options') ?></p>
			<div class="search_link closed">
				<div class="txt_search">Rechercher</div>
				<img src="<?= get_template_directory_uri() ?>/images/picto_loupe.png" />
				<div class="search_overlay">
					<div class="close_container text-right">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>

	<a href="#page" class="back_to_top detect-scroll-level js-scrollTo" detect-scroll-level="200"><i class="fas fa-angle-up"></i></a>



	<div id="content" class="site-content page_content">
