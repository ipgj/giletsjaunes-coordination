<?php
/*
Template Name: Page - Coordination (CNR)
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page">
		<main class="clearfix nopaddingtop">

			<div class="fullsize_head_container">
				<section class="entete_page">
					<?php
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content-page', 'content-page' );
					endwhile; // End of the loop.
					?>
				</section>

				<section class="entete_content">
					<div class="icon"><?= get_field('fullsize_header_icon') ?></div>
					<div class="separateur_vert"></div>
					<div class="texte">
						<?= get_field('fullsize_header_text') ?>
					</div>
				</section>
			</div>

			<div class="carte_groupes_container">
				<iframe src="https://www.giletsjaunes-coordination.fr/carte.html?embed"></iframe>
			</div>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




