<?php
/*
Template Name: Page - Liens utiles
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page">
		<main class="clearfix nopaddingtop liens liste">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>

			<section class="liens_container">
				<?php
				if(have_rows('categories')) :
					$i = 0;
					while(have_rows('categories')) : the_row();
						$i++;
						$titre = get_sub_field('titre');
						?>
				
						<a class="titre_cat" data-toggle="collapse" href="#reponse_<?=$i?>" role="button" aria-expanded="true" aria-controls="reponse_<?=$i?>">
							<h4><i class="fas fa-arrow-right"></i> <?= $titre ?></h4>
						</a>

						<div id="reponse_<?=$i?>" class="contenu_cat collapse show">
							<ul class="row">
								<?php
								if(have_rows('contenu')) :
									$j = 0;
									while(have_rows('contenu')) : the_row();
										$j++;
										$lien = get_sub_field('lien');
										$titre = get_sub_field('titre');
										$description = get_sub_field('description');
										?>
								
										<li class="col-lg-3 col-md-4 col-sm-6">
											<a target="_blank" href="<?= $lien ?>">
												<p class="libelle"><?= $titre ?></p>
												<div class="description"><?= $description ?></div>
											</a>
										</li>
								
									<?php endwhile;
									else :
										echo '<p>Pas d\'entrée trouvée...</p>';
								endif;
								?>
							</ul>
						</div>
				
					<?php endwhile;
					else :
						echo '<p>Pas d\'entrée trouvée...</p>';
				endif;
				?>

			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




