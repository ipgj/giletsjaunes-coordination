<?php
/**
 * Gilet Jaune France functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Gilet_Jaune_France
 */

if ( ! function_exists( 'gilet_jaune_france_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function gilet_jaune_france_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Gilet Jaune France, use a find and replace
		 * to change 'gilet-jaune-france' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'gilet-jaune-france', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'gilet-jaune-france' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'gilet_jaune_france_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'gilet_jaune_france_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gilet_jaune_france_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'gilet_jaune_france_content_width', 640 );
}
add_action( 'after_setup_theme', 'gilet_jaune_france_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gilet_jaune_france_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gilet-jaune-france' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gilet-jaune-france' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gilet_jaune_france_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gilet_jaune_france_scripts() {
	wp_enqueue_style( 'gilet-jaune-france-style', get_stylesheet_uri() );

	wp_enqueue_script( 'gilet-jaune-france-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'gilet-jaune-france-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array('jquery'), '3.5.1', true);
	wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap.bundle.js', array(), true );
	wp_enqueue_script( 'mobile_detection', get_stylesheet_directory_uri() . '/js/mobile_detection.js', array(), true );
	wp_enqueue_script( 'main_javascript', get_stylesheet_directory_uri() . '/js/main_javascript.js', array(), true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gilet_jaune_france_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


include('functions_shortcodes.php');



// CUSTOM REWRITE RULES
// $data_pages_IDs = [102199, 102201, 102203, 102207, 102290];
// foreach($data_pages_IDs as $data_page_ID) {
// 	$page_url = str_replace( home_url().'/', '', get_permalink($data_page_ID) );
// 	$regex = $page_url.'.+$';
// 	$redirect_value = 'index.php+?p='.$data_page_ID;
// 	add_rewrite_rule($regex, $redirect_value, 'top');
// }





// AJOUT CSS ADMIN
function admin_css() {
	$admin_handle = 'admin_css';
	$admin_stylesheet = get_template_directory_uri() . '/style_backoffice.css';
	wp_enqueue_style( $admin_handle, $admin_stylesheet );
}
add_action('admin_print_styles', 'admin_css', 11 );

function my_custom_admin_head() {
	echo '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">';
}
add_action( 'admin_head', 'my_custom_admin_head' );


// AJOUT DU PAGE SLUG AUX CLASS DU BODY
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		/*foreach((get_the_category($post->ID)) as $category) {
		  	// add category slug to the $classes array
		  	$classes[] = $category->category_nicename;
		}
		
		$classes[] = $post->post_type . '-' . $post->post_name;*/
		$classes[] = strstr(get_page_template_slug($post), '.', true);;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// Enlever "Archives" des titre de page dans les catégories
add_filter('avf_which_archive_output','avf_change_which_archive', 10, 3);
function avf_change_which_archive($output)
{
    if(is_category())
    {
        $output = single_cat_title('',false);
    }

    return $output;
}


// Désactiver Gutenberg
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

// FORCER LES COPIER COLLER EN PLAIN TEXT
// always paste as plain text
foreach ( array( 'tiny_mce_before_init', 'teeny_mce_before_init') as $filter ) {
	add_filter( $filter, function( $mceInit ) {
		$mceInit[ 'paste_text_sticky' ] = true;
		$mceInit[ 'paste_text_sticky_default' ] = true;
		return $mceInit;
	});
}
// load 'paste' plugin in minimal/pressthis editor
add_filter( 'teeny_mce_plugins', function( $plugins ) {
	$plugins[] = 'paste';
	return $plugins;
});
// remove "Paste as Plain Text" button from editor
add_filter( 'mce_buttons_2', function( $buttons ) {
	if( ( $key = array_search( 'pastetext', $buttons ) ) !== false ) {
		unset( $buttons[ $key ] );
	}
	return $buttons;
});

function slugify($string, $replace = array(), $delimiter = '-') {
	// https://github.com/phalcon/incubator/blob/master/Library/Phalcon/Utils/Slug.php
	if (!extension_loaded('iconv')) {
	  throw new Exception('iconv module not loaded');
	}
	// Save the old locale and set the new locale to UTF-8
	$oldLocale = setlocale(LC_ALL, '0');
	setlocale(LC_ALL, 'en_US.UTF-8');
	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
	if (!empty($replace)) {
	  $clean = str_replace((array) $replace, ' ', $clean);
	}
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower($clean);
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
	$clean = trim($clean, $delimiter);
	// Revert back to the old locale
	setlocale(LC_ALL, $oldLocale);
	return $clean;
}



// AJOUT PAGE OPTIONS ACF
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Textes généraux',
		'menu_title'	=> 'Textes généraux',
		'menu_slug' 	=> 'textes-generaux',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_page(array(
		'page_title' 	=> 'DEPARTEMENTS',
		'menu_title'	=> 'DEPARTEMENTS',
		'menu_slug' 	=> 'liens-dep-forum',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	/*acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));*/
	
}


// Executer les shortcode dans les champs texte ACF
add_filter('acf/format_value/type=text', 'do_shortcode');


// SÉCURITÉ

// Cacher la version de wordpress
function wp_version_remove_version() {
return '';
}
add_filter('the_generator', 'wp_version_remove_version');



//
/*add_filter( 'post_link', 'remove_parent_cats_from_link', 10, 3 );
function remove_parent_cats_from_link( $permalink, $post, $leavename )
{
$cats = get_the_category( $post->ID );
if ( $cats ) {
// Make sure we use the same start cat as the permalink generator
usort( $cats, '_usort_terms_by_ID' ); // order by ID
$category = $cats[0]->slug;
if ( $parent = $cats[0]->parent ) {
// If there are parent categories, collect them and replace them in the link
$parentcats = get_category_parents( $parent, false, '/', true );
// str_replace() is not the best solution if you can have duplicates:
// myexamplesite.com/luxemburg/luxemburg/ will be stripped down to myexamplesite.com/
// But if you don't expect that, it should work
$permalink = str_replace( $parentcats, '', $permalink );
}
}
return $permalink;
}
*/



// FONCTIONS CONCERNANT LES LIENS PAGE DEP/FORUM DEP
function get_dep_forum_links_array() {
	//$dep_forum_links = get_field('liens_departements_forums', 'options');
	//global $dep_forum_links_array;
	$dep_forum_links_array = array();
	$i = 0;
	if(have_rows('liens_departements_forums', 'options')) : while(have_rows('liens_departements_forums', 'options')) : the_row(); 
		$i++;
		$num = get_sub_field('num');
		$main_dep_page = get_sub_field('page_principale_departementale');
		$forum_dep_root = get_sub_field('forum_departemental_racine');
		$nom = get_sub_field('nom');
		$slug = get_sub_field('slug');
		$dep_forum_links_array[] = [
			'num' => $num,
			'nom' => $nom,
			'slug' => $slug,
			'main_dep_page' => $main_dep_page,
			'forum_dep_root' => $forum_dep_root,
		];
	endwhile; endif;

	return $dep_forum_links_array;
}

function get_corresponding_forum_dep_root($main_dep_page_id) {
	$dep_forum_links_array = get_dep_forum_links_array();
	$corresponding_index = array_search($main_dep_page_id, array_column($dep_forum_links_array, 'main_dep_page'));
	$corresponding_forum_dep_root = $dep_forum_links_array[$corresponding_index]['forum_dep_root'];

	return $corresponding_forum_dep_root;
}

function get_corresponding_num($main_dep_page_id) {
	$dep_forum_links_array = get_dep_forum_links_array();
	$corresponding_index = array_search($main_dep_page_id, array_column($dep_forum_links_array, 'main_dep_page'));
	$corresponding_num = $dep_forum_links_array[$corresponding_index]['num'];

	return $corresponding_num;
}

function get_corresponding_main_dep_page($forum_dep_root_id) {
	$dep_forum_links_array = get_dep_forum_links_array();
	$corresponding_index = array_search($forum_dep_root_id, array_column($dep_forum_links_array, 'forum_dep_root'));
	$corresponding_main_dep_page = $dep_forum_links_array[$corresponding_index]['main_dep_page'];

	return $corresponding_main_dep_page;
}

function get_slug_from_root_forum_id($forum_dep_root_id) {
	$dep_forum_links_array = get_dep_forum_links_array();
	$corresponding_index = array_search($forum_dep_root_id, array_column($dep_forum_links_array, 'forum_dep_root'));
	$corresponding_slug = $dep_forum_links_array[$corresponding_index]['slug'];

	return $corresponding_slug;
}

function get_dep_num_from_forum_root_id($forum_dep_root_id) {
	$dep_forum_links_array = get_dep_forum_links_array();
	$corresponding_index = array_search($forum_dep_root_id, array_column($dep_forum_links_array, 'forum_dep_root'));
	$corresponding_num = $dep_forum_links_array[$corresponding_index]['num'];

	return $corresponding_num;
}

function get_main_dep_page_from_num($num) {
	$dep_forum_links_array = get_dep_forum_links_array();
	$corresponding_index = array_search($num, array_column($dep_forum_links_array, 'num'));
	$corresponding_main_dep_page = $dep_forum_links_array[$corresponding_index]['main_dep_page'];

	return $corresponding_main_dep_page;
}

function get_dep_name_from_num($num) {
	$dep_forum_links_array = get_dep_forum_links_array();
	$corresponding_index = array_search($num, array_column($dep_forum_links_array, 'num'));
	$dep_name = $dep_forum_links_array[$corresponding_index]['nom'];

	return $dep_name;
}











//===========================================================================
// Function to check page security
//===========================================================================
function check_page_security() {
	if( !is_user_logged_in() ) {
		wp_redirect( home_url() );
		exit();
	}
}




// Génération de la navigation boostrap style
class Primary_Walker_Nav_Menu extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {

        if (array_search('menu-item-has-children', $item->classes)) {
            $output .= sprintf("\n<li class='nav-item dropdown %s'><a href='%s' class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" >%s</a>\n", ( array_search('current-menu-item', $item->classes) || array_search('current-page-parent', $item->classes) ) ? 'active' : '', $item->url, $item->title
            );
        } else {
        	if(in_array('fake_link', $item->classes)) {
        		$output .= sprintf("\n<li class=\"nav-item%s\"><div class=\"nav-link\" onclick=\"location.href='%s'\">%s</div>\n", ( array_search('current-menu-item', $item->classes) ) ? ' active' : '', $item->url, $item->title);
        	} else {
            	$output .= sprintf("\n<li class=\"nav-item%s\"><a class=\"nav-link\" href='%s'>%s</a>\n", ( array_search('current-menu-item', $item->classes) ) ? ' active' : '', $item->url, $item->title);
        	}
        } 
    }

    function start_lvl(&$output, $depth=0, $args=array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"dropdown-menu\" role=\"menu\">\n";
    }
}


/**
 * Get current page depth
 *
 * @return integer
 */
function get_current_page_depth(){
    global $wp_query;
     
    $object = $wp_query->get_queried_object();
    $parent_id  = $object->post_parent;
    $depth = 0;
    while($parent_id > 0){
        $page = get_page($parent_id);
        $parent_id = $page->post_parent;
        $depth++;
    }
  
    return $depth;
}


/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
	$content = force_balance_tags( $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
	return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);


// AJOUTER DES FORMATS D'IMAGE AUTO
if ( function_exists( 'add_image_size' ) ) { 
	remove_image_size( 'medium_large');
	remove_image_size( 'scaled');
	remove_image_size( '1536x1536');
	remove_image_size( '2048x2048');
	add_image_size( 'fullhd', 1920, 1080, true ); //(cropped)
	add_image_size( 'fullhd-nocrop', 1920, 0, false ); //(cropped)
}



// GESTION DES PAGES, POSTS, FORUMS PROTÉGÉS

// Temps au bout duquel il faut redonner le mot de passe
function mind_set_cookie_expire( $time ) {
  return 0; 
  // 86400 = 1 day 
  // 60 = 1 minute 
  // return 0 to set the cookie to expire at the end of the session. 
}
add_filter( 'post_password_expires', 'mind_set_cookie_expire' );

// Enlever le therme "protégé" devant les pages protégées par mot de passe
add_filter( 'private_title_format', 'myprefix_private_title_format' );
add_filter( 'protected_title_format', 'myprefix_private_title_format' );

function myprefix_private_title_format( $format ) {
    return '%s';
}

// Modifier le formulaire de mot de passe de page protégée
function my_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form class="password_protected_form" action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    ' . __( "<p class=\"texte\">Ce contenu est protégé par un <strong>mot de passe départemental</strong><br/>(fixé par les modérateurs de ce département).</p><p class=\"texte\">Veuillez entrer en contact avec eux si vous souhaitez y accéder.</p>" ) . '
    <input type="password" name="pass_fake" id="pass_fake" class="hidden" autocomplete="off" style="display: none;" />
    <input name="post_password" id="' . $label . '" class="input_pass" type="password" size="20" maxlength="20" placeholder="Mot de passe départemental..."/><input class="bouton_type_2 submit" type="submit" name="Submit" value="' . esc_attr__( "Valider" ) . '" />
    </form>
    ';
    return $o;
}
add_filter( 'the_password_form', 'my_password_form' );


// Modifier l'excerpt des posts protégés
function my_excerpt_protected( $excerpt ) {
    if ( post_password_required() )
        $excerpt = '<em class="password_protected_excerpt">[Protégé par mot de passe]</em>';
    return $excerpt;
}
add_filter( 'the_excerpt', 'my_excerpt_protected' );




// Ajouter le champ ACF "contenu_protege" aux tableaux de l'admin des customs fields (filters dans la déclaration des customs fields)
// ADD NEW COLUMN
function add_custom_columns_to_post_admin($columns) {
    $columns['is_protected'] = '<i class="fas fa-unlock-alt"></i>';
    return $columns;
}

// Déplacer la colonne "protected" en début de tableau
function custom_admin_columns_reorder($columns) {
  $custom_admin_columns = array();
  $is_protected = 'is_protected'; 
  $title = 'title'; 
  foreach($columns as $key => $value) {
    if ($key==$title){
      $custom_admin_columns[$is_protected] = $is_protected;
    }
      $custom_admin_columns[$key] = $value;
  }
  return $custom_admin_columns;
}
 
// Mettre un cadenas ouvert ou fermé en fonctionde si la news est protégée
function custom_admin_column_content($column_name, $post_ID) {
    if ($column_name == 'is_protected') {
    	$is_protected = get_field('contenu_protege', $post_ID);
        if($is_protected) {
            echo '<i class="fas fa-lock"></i>';
        } else {
        	echo '<i class="fas fa-lock-open"></i>';
        }
    } 
}
//add_filter('manage_posts_columns', 'add_custom_columns_to_post_admin');
//add_action('manage_posts_custom_column', 'custom_admin_column_content', 10, 2);









// AJOUTER UNE META "DEPARTEMENT" AUX UTILISATEURS

/**
 * Add new fields above 'Update' button.
 *
 * @param WP_User $user User object.
 */
function tm_additional_profile_fields( $user ) {

    $departements = array( 
    	'Aucun',
    	'01 - Ain',
		'02 - Aisne',
		'03 - Allier',
		'04 - Alpes-de-Haute-Provence',
		'05 - Hautes-alpes',
		'06 - Alpes-maritimes',
		'07 - Ardèche',
		'08 - Ardennes',
		'09 - Ariège',
		'10 - Aube',
		'11 - Aude',
		'12 - Aveyron',
		'13 - Bouches-du-Rhône',
		'14 - Calvados',
		'15 - Cantal',
		'16 - Charente',
		'17 - Charente-maritime',
		'18 - Cher',
		'19 - Corrèze',
		'2a - Corse-du-sud',
		'2b - Haute-Corse',
		'21 - Côte-d\'Or',
		'22 - Côtes-d\'Armor',
		'23 - Creuse',
		'24 - Dordogne',
		'25 - Doubs',
		'26 - Drôme',
		'27 - Eure',
		'28 - Eure-et-loir',
		'29 - Finistère',
		'30 - Gard',
		'31 - Haute-garonne',
		'32 - Gers',
		'33 - Gironde',
		'34 - Hérault',
		'35 - Ille-et-vilaine',
		'36 - Indre',
		'37 - Indre-et-loire',
		'38 - Isère',
		'39 - Jura',
		'40 - Landes',
		'41 - Loir-et-cher',
		'42 - Loire',
		'43 - Haute-loire',
		'44 - Loire-atlantique',
		'45 - Loiret',
		'46 - Lot',
		'47 - Lot-et-garonne',
		'48 - Lozère',
		'49 - Maine-et-loire',
		'50 - Manche',
		'51 - Marne',
		'52 - Haute-marne',
		'53 - Mayenne',
		'54 - Meurthe-et-moselle',
		'55 - Meuse',
		'56 - Morbihan',
		'57 - Moselle',
		'58 - Nièvre',
		'59 - Nord',
		'60 - Oise',
		'61 - Orne',
		'62 - Pas-de-calais',
		'63 - Puy-de-dôme',
		'64 - Pyrénées-atlantiques',
		'65 - Hautes-Pyrénées',
		'66 - Pyrénées-orientales',
		'67 - Bas-rhin',
		'68 - Haut-rhin',
		'69 - Rhône',
		'70 - Haute-saône',
		'71 - Saône-et-loire',
		'72 - Sarthe',
		'73 - Savoie',
		'74 - Haute-savoie',
		'75 - Paris',
		'76 - Seine-maritime',
		'77 - Seine-et-marne',
		'78 - Yvelines',
		'79 - Deux-sèvres',
		'80 - Somme',
		'81 - Tarn',
		'82 - Tarn-et-garonne',
		'83 - Var',
		'84 - Vaucluse',
		'85 - Vendée',
		'86 - Vienne',
		'87 - Haute-vienne',
		'88 - Vosges',
		'89 - Yonne',
		'90 - Territoire de belfort',
		'91 - Essonne',
		'92 - Hauts-de-seine',
		'93 - Seine-Saint-Denis',
		'94 - Val-de-marne',
		'95 - Val-d\'oise',
		'971 - Guadeloupe',
		'972 - Martinique',
		'973 - Guyane',
		'974 - La réunion',
		'976 - Mayotte',
    );
    $default	= array( 'departement' => 'Aucun');
    $departement_actuel = get_user_meta(
	    $user->ID,
	    $key = 'departement',
	    $single = false
	);

    ?>
    <h3>Informations géographiques</h3>

    <table class="form-table">
   	 <tr>
   		 <th><label for="birth-date-day">Votre département</label></th>
   		 <td>
   			 <select id="select-departement" name="departement">
   			 	<?php
					foreach ( $departements as $departement ) {
						printf( '<option value="%1$s" %2$s>%1$s</option>', $departement, selected( $departement_actuel[0], $departement, false ) );
					}
	   			?>
   			 </select>
   		 </td>
   	 </tr>
    </table>
    <?php
}

add_action( 'show_user_profile', 'tm_additional_profile_fields' );
add_action( 'edit_user_profile', 'tm_additional_profile_fields' );


/**
 * Save additional profile fields.
 *
 * @param  int $user_id Current user ID.
 */
function tm_save_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	 return false;
    }

    if ( empty( $_POST['departement'] ) ) {
   	 return false;
    }

    update_usermeta( $user_id, 'departement', $_POST['departement'] );
}

add_action( 'personal_options_update', 'tm_save_profile_fields' );
add_action( 'edit_user_profile_update', 'tm_save_profile_fields' );

// Enregistrement du champ département à l'inscription
add_action( 'user_register', 'save_departement_on_registration', 10, 1 );
function save_departement_on_registration( $user_id ) {
    if (isset($_POST['departement'])) {
        update_usermeta($user_id, 'departement', $_POST['departement']);
    }

}



// AJOUT DE L'ENREGISTREMENT DU PASSWORD UTILISATEUR CHOISI SUR LA PAGE D'INSCRIPTION
add_action( 'user_register', 'registration_password_change_pass', 10, 99 );

function registration_password_change_pass( $user_id ) {

    if ( isset( $_POST['user_password'] ) )
        wp_set_password( $_POST['user_password'], $user_id );
}


// LOGIN AUTO ET REDIRECTION APRES INSCRIPTION
function auto_login_new_user( $user_id ) {
    wp_set_current_user($user_id);
    //wp_set_auth_cookie($user_id);
    $secure_cookie = is_ssl() ? true : false;
	wp_set_auth_cookie( $user_id, true, $secure_cookie );
    //$user = get_user_by( 'id', $user_id );
    //do_action( 'wp_login', $user->user_login );// [Codex Ref.][1]
    wp_redirect( get_permalink('265') ); // You can change home_url() to the specific URL,such as "wp_redirect( 'http://www.wpcoke.com' )";
    exit;
}
add_action( 'user_register', 'auto_login_new_user' );

// GESTION ERREUR INSCRIPTION
add_action('register_post', 'binda_register_fail_redirect', 99, 3);

function binda_register_fail_redirect( $sanitized_user_login, $user_email, $errors ){
	$secret = "6LcZZZAUAAAAALvh4xDwYN5jFPY7Fr9U70wQfoh0";
	$response = $_POST['g-recaptcha-response'];
	$remoteip = $_SERVER['REMOTE_ADDR'];
	 
	$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$response."&remoteip=".$remoteip;
	 
	$api_call_result = json_decode(file_get_contents($api_url), true);

	if ($_POST['user_password'] != $_POST['user_password2']) {
		$redirect_url = get_permalink('188').'#inscription';
		$redirect_url = add_query_arg( 'passwordcheck', 'error', $redirect_url );
		wp_redirect( $redirect_url );
        exit; 
	}
	 
	if ($api_call_result['success'] != true && false /* HARD SHUNT */) {
		$redirect_url = get_permalink('188').'#inscription';
		$redirect_url = add_query_arg( 'grecaptcha_error', 'true', $redirect_url );
		wp_redirect( $redirect_url );
        exit; 
	}

	$accept_cgu = $_POST['accept_cgu'];
	if(!isset($accept_cgu)) {
		$redirect_url = get_permalink('188').'#inscription';
		$redirect_url = add_query_arg( 'cgu_error', 'true', $redirect_url );
		wp_redirect( $redirect_url );
        exit; 
	}


    //this line is copied from register_new_user function of wp-login.php
    $errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email );
    //this if check is copied from register_new_user function of wp-login.php
    if ( $errors->get_error_code() ){
        //setup your custom URL for redirection
        $redirect_url = get_permalink('188').'#inscription';
        //add error codes to custom redirection URL one by one
        foreach ( $errors->errors as $e => $m ){
            $redirect_url = add_query_arg( $e, '1', $redirect_url );    
        }
        //add finally, redirect to your custom page with all errors in attributes
        wp_redirect( $redirect_url );
        exit;   
    }
}

/*function update_caps() {
$role = get_role( 'subscriber' );

$caps_to_add = array(
'edit_others_posts',
);

foreach( $caps_to_add as $cap )
$role->add_cap( $cap );
}
add_action('init','update_caps');*/



// ------------- FONCTIONS RELATIVES AUX UTILISATEURS -----------------
// --------------------------------------------------------------------
function get_current_user_main_role() {
	$user = wp_get_current_user();
	$user_data = get_userdata($user->ID);
	$user_roles = $user_data->roles;
	$user_mainrole = $user_roles[0];
	$user_mainrole = explode('_',$user_mainrole);
	$user_mainrole = $user_mainrole[0];
	return $user_mainrole;
}
function get_user_main_role($user) {
	$user_data = get_userdata($user);
	$user_roles = $user_data->roles;
	$user_mainrole = $user_roles[0];
	$user_mainrole = explode('_',$user_mainrole);
	$user_mainrole = $user_mainrole[0];
	return $user_mainrole;
}
function get_current_user_role() {
	$user = wp_get_current_user();
	$user_data = get_userdata($user->ID);
	$user_roles = $user_data->roles;
	$user_mainrole = $user_roles[0];
	return $user_mainrole;
}
function get_user_role($user_id) {
	$user_data = get_userdata($user_id);
	$user_roles = $user_data->roles;
	$user_mainrole = $user_roles[0];
	return $user_mainrole;
}
function get_user_role_name($user_id) {
	$user_data = get_userdata($user_id);
	$user_roles = $user_data->roles;
	$user_mainrole = $user_roles[0];
	$user_mainrole = str_replace('_', ' ', $user_mainrole);
	$user_mainrole = ucwords($user_mainrole);
	if($user_mainrole == 'Administrator') { $user_mainrole = 'Webmaster'; }
	if($user_mainrole == 'Subscriber') { $user_mainrole = 'Participant'; }
	return $user_mainrole;
}
function get_current_user_departement() {
	$user = wp_get_current_user();
	$departement_user = get_user_meta(
	    $user->ID,
	    $key = 'departement',
	    $single = false
	);
	return $departement_user[0];
}
function get_user_departement($user_id) {
	$user = wp_get_current_user();
	$departement_user = get_user_meta(
	    $user_id,
	    $key = 'departement',
	    $single = false
	);
	return $departement_user[0];
}
function get_current_user_departement_slug() {
	$user = wp_get_current_user();
	$departement_user = get_user_meta(
	    $user->ID,
	    $key = 'departement',
	    $single = false
	);
	$departement_user = str_replace('\'', '-', $departement_user[0]);
	$departement_user = sanitize_title($departement_user);
	return $departement_user;
}


// RENOMMER CERTAINS ROLES
function change_role_name() {
    global $wp_roles;

    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    // Lister les roles
    /*$roles = $wp_roles->get_names();
    print_r($roles);*/

    //You can replace "administrator" with any other role "editor", "author", "contributor" or "subscriber"...
    $wp_roles->roles['administrator']['name'] = 'Webmaster';
    $wp_roles->role_names['administrator'] = 'Webmaster';           
}
add_action('init', 'change_role_name');


// GESTION DES PAGES ADMINS DISPONIBLES EN FONCTION DU ROLE
add_action( 'admin_init', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
	if (get_current_user_main_role() == 'admin') {
		remove_menu_page('admin.php?page=forums_toolkit');
		remove_menu_page('edit.php?post_type=topic');
		remove_menu_page('edit.php?post_type=reply');
		remove_menu_page('profile.php');
	}
	if (get_current_user_main_role() == 'moderateur') {
		remove_menu_page('profile.php');
		//remove_menu_page('upload.php');
		remove_menu_page('index.php');
	}
	if (get_current_user_main_role() == 'administrateur') {
		remove_menu_page('tools.php');

	}
}

// LIMITER LA MEDIA LIBRARY : Les utilisteurs non-admin ne peuvent voir que leurs propres fichiers
add_filter( 'ajax_query_attachments_args', 'wpb_show_current_user_attachments' );
 
function wpb_show_current_user_attachments( $query ) {
    $user_id = get_current_user_id();
    $user_main_role = get_current_user_main_role();
    if ($user_main_role != 'webmaster' AND $user_main_role != 'administrateur' AND $user_main_role != 'administrator') {
        $query['author'] = $user_id;
    }
    return $query;
} 


// CACHER CERTAINS BOUTONS DE L'EDITEUR TEXTE, en fonction du rôle
add_filter( 'mce_buttons', 'jivedig_remove_tiny_mce_buttons_from_editor');
function jivedig_remove_tiny_mce_buttons_from_editor( $buttons ) {
    $remove_buttons = array(
        //'bold',
        //'italic',
        //'strikethrough',
        //'bullist',
        //'numlist',
        'blockquote',
        //'hr', // horizontal line
        //'alignleft',
        //'aligncenter',
        //'alignright',
        //'link',
        //'unlink',
        //'wp_more', // read more link
        //'spellchecker',
        //'dfw', // distraction free writing mode
        //'wp_adv', // kitchen sink toggle (if removed, kitchen sink will always display)
    );
    foreach ( $buttons as $button_key => $button_value ) {
        if ( in_array( $button_value, $remove_buttons ) ) {
            unset( $buttons[ $button_key ] );
        }
    }
    return $buttons;
}

// CACHER YOAST AUX NON ADMINS
function remove_yoast_from_admin() {
    
    if (!current_user_can('administrator')) {
		echo "
		<style type='text/css'>
			#wpseo_meta { display: none !important; }
			#A2A_SHARE_SAVE_meta { display: none !important; }
		</style>
		";
	}
}
add_action( 'admin_head', 'remove_yoast_from_admin' );








// GESTION DE LA REDIRECTION A LA CONNEXION ET DE LA BAR ADMIN EN FONCTION DU ROLE UTILISATEUR
// Utilisateur de base : "subscriber" et "bbp_participant"
// Admin : "administrator" et "bbp_keymaster"

// Enlever l'admin bar pour les non-admins
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}


add_filter( 'query_vars', 'addnew_query_vars', 10, 1 );
function addnew_query_vars($vars)
{   
    $vars[] = 'return_url'; // var1 is the name of variable you want to add       
    return $vars;
}

// REDIRIGER LES TENTATIVES DE CONNEXION (ECHEC, OU EN FONCTION DU ROLE SI RÉUSSI)
function my_login_redirect( $url, $request, $user )	{
	// SI IL Y A UNE ERREUR DE LOGIN
	if (is_wp_error($user)) {
        $error_types = array_keys($user->errors);
        //Error type seems to be empty if none of the fields are filled out
        $error_type = 'both_empty';
        //Otherwise just get the first error (as far as I know there
        //will only ever be one)
        if (is_array($error_types) && !empty($error_types)) {
            $error_type = $error_types[0];
        }
        wp_redirect( get_permalink( 188 ).'?login=failed&reason='.$error_type.'&return_url='.$url ); 
        exit;
    } else {
        //Login OK - redirect to another page?
        // REDIRECTION EN FONCTION DU ROLE 
	    /*if ( isset( $user->roles ) && is_array( $user->roles ) ) {

	    	$main_role = get_user_main_role($user);

	        //if ( in_array( 'administrator', $user->roles ) OR $main_role == 'moderateur' ) {
	        if ( in_array( 'administrator', $user->roles ) ) {
	            $url = '/wp-admin/';
	        } else {
	        	if(isset($_GET['return_url'])) {
					$url = $_SERVER['HTTP_HOST'].$_GET['return_url'];
				} else {
					$url = '/';
				}
	        }
	    }*/
	    return $url;
    }
}
add_filter('login_redirect', 'my_login_redirect', 10, 3 );

// Rediriger après la déconnexion
/*add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}*/




// GESTION DE LA PAGE "MON COMPTE"
//======================================================================
// Add post state to the projects page
//======================================================================
add_filter( 'display_post_states', 'ecs_add_post_state', 10, 2 );
function ecs_add_post_state( $post_states, $post ) {
	if( $post->post_name == 'edit-profile' ) {
		$post_states[] = 'Profile edit page';
	}
	return $post_states;
}


//======================================================================
// Add notice to the profile edit page
//======================================================================
add_action( 'admin_notices', 'ecs_add_post_notice' );
function ecs_add_post_notice() {
	global $post;
	if( isset( $post->post_name ) && ( $post->post_name == 'edit-profile' ) ) {
	  /* Add a notice to the edit page */
		add_action( 'edit_form_after_title', 'ecs_add_page_notice', 1 );
		/* Remove the WYSIWYG editor */
		remove_post_type_support( 'page', 'editor' );
	}	
}
function ecs_add_page_notice() {
	echo '<div class="notice notice-warning inline"><p>' . __( 'You are currently editing the profile edit page. Do not edit the title or slug of this page!', 'textdomain' ) . '</p></div>';
}




// GESTION DES FORUMS
// ------------------


function get_root_forum_slug($current_forum_id) {
    $forum_ancestors = get_post_ancestors($current_forum_id);
	$forum_root_id = end($forum_ancestors);
	$forum_name = bbp_forum_title($forum_root_id);
    return $forum_name;
}
// Trouver le root forum ID à parti d'un forum id 
function get_root_forum_id($forum_id) {
	$forum_ancestors = get_post_ancestors($forum_id);
	$root_forum_id = end($forum_ancestors);
	return $root_forum_id;
}


// Enlever certains éléments de la Breadcrumb
function mycustom_breadcrumb_options() {
    // Home - default = true
    $args['include_home']    = false;
    // Forum root - default = true
    $args['include_root']    = false;
    // Current - default = true
    $args['include_current'] = true;
 
    return $args;
}
add_filter('bbp_before_get_breadcrumb_parse_args', 'mycustom_breadcrumb_options');


// Changer le nb de caractères max d'un titre de topic
add_filter ('bbp_get_title_max_length','rkk_change_title') ;
function rkk_change_title ($default) {
	$default=100 ;
	return $default ;
}

// Ajouter le visual editor lors de l'écriture de messages dans les forums
function bbp_enable_visual_editor( $args = array() ) {
    $args['tinymce'] = true;
    $args['quicktags'] = false;
    return $args;
}
add_filter( 'bbp_after_get_the_content_parse_args', 'bbp_enable_visual_editor' );

// Éviter les problèmes de copié coller 
function bbp_tinymce_paste_plain_text( $plugins = array() ) {
    $plugins[] = 'paste';
    return $plugins;
}
add_filter( 'bbp_get_tiny_mce_plugins', 'bbp_tinymce_paste_plain_text' );


// Raccourcir l'affichage de la "freshness" du dernier message
function bbp_shorten_freshness_display($return) {
	$return = preg_replace( '/ et .*/', ' ', $return );
	$return = ucfirst($return);
	return $return;
}
add_filter( 'bbp_get_time_since', 'bbp_shorten_freshness_display' );
add_filter( 'bp_core_time_since', 'bbp_shorten_freshness_display');



// Custom RSS feed pour l'agenda national
add_action( 'init', 'agenda_national_custom_feed' );
function agenda_national_custom_feed() {
 
    // Create your feed name like this : https://yoursite.com/wpdocs_custom?tag=test
    add_feed( 'agenda-national-rss', 'agenda_national_rss_change_main_query' );
    function agenda_national_rss_change_main_query() {
 
        // Set right headers for RSS Feed
		header( 'Content-Type: application/rss+xml' );
 
        // Get main WP Query
		global $wp_query;
		
		$dep_forum_links_array = get_dep_forum_links_array();
		foreach($dep_forum_links_array as $dep) {
			$deps['post_type'][] = 'actu-'.$dep['slug'];
		}
		$deps['post_type'][] = 'actu-ada';

		// foreach ($deps['post_type'] as $key => $value) {
		// 	echo $value;
		// }

        // Overwrite main WP Query with yours
        $wp_query = new WP_Query(
			array(
				'post_type'      => $deps['post_type'],
				'posts_per_page' => 3,
				'order'          => 'DESC',
				'orderby'        => 'date',
				'meta_query'	=> array(
					'relation' => 'OR',
					array(
						'key'	  	=> 'contenu_protege',
						'value'	  	=> '',
						'compare' 	=> '=',
					),
					array(
						'key'	  	=> 'contenu_protege',
						'value'	  	=> '',
						'compare' 	=> 'NOT EXISTS',
					),
				),
			)
		);

		echo '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
		xmlns:content="http://purl.org/rss/1.0/modules/content/"
		xmlns:wfw="http://wellformedweb.org/CommentAPI/"
		xmlns:dc="http://purl.org/dc/elements/1.1/"
		xmlns:atom="http://www.w3.org/2005/Atom"
		xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
		xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
		>
		
			<channel>
				<title>🟨 Portail Gilet Jaune</title>
				<atom:link href="https://giletsjaunes-coordination.fr/feed/" rel="self" type="application/rss+xml" />
				<link>https://giletsjaunes-coordination.fr</link>
				<description>Portail national d&#039;information, de discussion, de débat et de ressources à l&#039;usage des Gilets Jaunes</description>
				<lastBuildDate>'.date('r', time()).'</lastBuildDate>
				<language>fr-FR</language>
				<sy:updatePeriod>
				hourly	</sy:updatePeriod>
				<sy:updateFrequency>
				1	</sy:updateFrequency>

				<image>
					<url>https://giletsjaunes-coordination.fr/wp-content/uploads/2019/06/favicon-150x150.png</url>
					<title>🟨 Portail Gilet Jaune</title>
					<link>https://giletsjaunes-coordination.fr</link>
					<width>32</width>
					<height>32</height>
				</image>';

				if ( $wp_query->have_posts() ) :
					while ( $wp_query->have_posts() ) : $wp_query->the_post(); 

						echo '<item>
							<title>'.get_the_title().'</title>
							<link>'.get_the_permalink().'</link>
							<comments>'.get_the_permalink().'#comments</comments>
							
							<dc:creator><![CDATA[GiletsJaunes-Coordination.fr]]></dc:creator>
							<pubDate>'.date('r', get_post_timestamp()).'</pubDate>
							<category><![CDATA[Non classé]]></category>
							<guid isPermaLink="false">'.get_the_guid().'</guid>
							
							<description><![CDATA['.get_the_content().']]></description>
							<content:encoded><![CDATA['.apply_filters('the_content', get_the_content()).']]></content:encoded>
							
							<wfw:commentRss>'.esc_url( get_post_comments_feed_link(null, 'rss2') ).'</wfw:commentRss>
							<slash:comments>'.get_comments_number().'</slash:comments>

						</item>';

					endwhile;
				endif;
				
			echo '</channel>
		</rss>
		';
    }
}




// ADDING CUSTOM POST TYPE
// https://wpchannel.com/wordpress/tutoriels-wordpress/creer-custom-post-types-wordpress/


function all_custom_post_types() {

	$i = 0;
	if(have_rows('liens_departements_forums', 'options')) : while(have_rows('liens_departements_forums', 'options')) : the_row(); 
		$i++;

		$nom = get_sub_field('nom');
		$slug = get_sub_field('slug');

		for ($j = 1; $j <= 2; $j++) {
		    
			$position = 1000+$i;

			if($j == 1) {
				$type = 'page';
				$cpt_name = 'page-'.$slug;
				$rw_slug = 'pages-'.$slug;
				$single = 'Page '.$nom;
				$plural = 'Pages '.$nom;
				$icon = 'dashicons-admin-'.$type;
				$show_in_menu = true;
			} else {
				$type = 'post';
				$cpt_name = 'actu-'.$slug;
				$rw_slug = 'actualites-'.$slug;
				$single = 'Actu '.$nom;
				$plural = 'Actus '.$nom;
				$icon = 'dashicons-admin-'.$type;
				//$show_in_menu = 'edit.php?post_type='.'page-'.$slug;
				$show_in_menu = true;
			}

			$labels = array(
				'name'                  => _x( $plural, 'Post Type General Name', 'fr_FR' ),
				'singular_name'         => _x( $single, 'Post Type Singular Name', 'fr_FR' ),
				'menu_name'             => __( $plural, 'fr_FR' ),
				'name_admin_bar'        => __( $plural, 'fr_FR' ),
				'archives'              => __( $plural.' Archives', 'fr_FR' ),
				'attributes'            => __( $plural.' Attributes', 'fr_FR' ),
				'parent_item_colon'     => __( 'Parent Item:', 'fr_FR' ),
				'all_items'             => __( 'Tous', 'fr_FR' ),
				'add_new_item'          => __( 'Ajouter', 'fr_FR' ),
				'add_new'               => __( 'Nouveau', 'fr_FR' ),
				'new_item'              => __( 'Nouveau', 'fr_FR' ),
				'edit_item'             => __( 'Éditer', 'fr_FR' ),
				'update_item'           => __( 'Mettre à jour', 'fr_FR' ),
				'view_item'             => __( 'Voir', 'fr_FR' ),
				'view_items'            => __( 'Voir', 'fr_FR' ),
				'search_items'          => __( 'Rechercher', 'fr_FR' ),
				'not_found'             => __( 'Non trouvé', 'fr_FR' ),
				'not_found_in_trash'    => __( 'Non trouvé dans la corbeille', 'fr_FR' ),
				'featured_image'        => __( 'Image mise en avant', 'fr_FR' ),
				'set_featured_image'    => __( 'Définir l\'image mise en avant', 'fr_FR' ),
				'remove_featured_image' => __( 'Supprimer l\'image', 'fr_FR' ),
				'use_featured_image'    => __( 'Utilisé comme image', 'fr_FR' ),
				'insert_into_item'      => __( 'Insérer', 'fr_FR' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'fr_FR' ),
				'items_list'            => __( 'Items list', 'fr_FR' ),
				'items_list_navigation' => __( 'Items list navigation', 'fr_FR' ),
				'filter_items_list'     => __( 'Filter items list', 'fr_FR' ),
			);
			$args = array(
				'label'                 => __( $single, 'fr_FR' ),
				'description'           => __( $single.'.', 'fr_FR' ),
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes', 'comments' ),
				//'taxonomies'            => array( 'category', 'post_tag' ),
				'hierarchical'          => true,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => $show_in_menu,
				'menu_position'         => $position,
				'menu_icon'             => $icon,
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'capabilities' => array(
					'edit_post'              => 'edit_'.$slug,
					'read_post'              => 'read_'.$slug,
					'delete_post'            => 'delete_'.$slug,
					'create_posts'           => 'create_'.$slug,
					'edit_posts'             => 'edit_'.$slug,
					'edit_others_posts'      => 'edit_others_'.$slug,
					'publish_posts'          => 'publish_'.$slug,
					'read_private_posts'     => 'read_'.$slug,
					'read'                   => 'read',
					'delete_posts'           => 'delete_'.$slug,
					'delete_private_posts'   => 'delete_private_'.$slug,
					'delete_published_posts' => 'delete_published_'.$slug,
					'delete_others_posts'    => 'delete_others_'.$slug,
					'edit_private_posts'     => 'edit_private_'.$slug,
					'edit_published_posts'   => 'edit_published_'.$slug
				),
				'rewrite' => array('slug' => $rw_slug,'with_front' => false),
			);

			register_post_type( $cpt_name, $args );

			// Ajouter la colonne "protégé"
			if($j == 2) {
				add_filter('manage_'.$cpt_name.'_posts_columns', 'add_custom_columns_to_post_admin');
				add_filter('manage_'.$cpt_name.'_posts_columns', 'custom_admin_columns_reorder');
				add_action('manage_'.$cpt_name.'_posts_custom_column', 'custom_admin_column_content', 10, 2);
			}

		}

	endwhile; endif;

}

add_action('init', 'all_custom_post_types', 10);

// --- CUSTOM POST TYPE ADA ---
function ada_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Pages AdA', 'Post Type General Name', 'fr_FR' ),
		'singular_name'         => _x( 'Page AdA', 'Post Type Singular Name', 'fr_FR' ),
		'menu_name'             => __( 'Espace AdA', 'fr_FR' ),
		'name_admin_bar'        => __( 'Pages AdA', 'fr_FR' ),
		'archives'              => __( 'Pages AdA'.' Archives', 'fr_FR' ),
		'attributes'            => __( 'Pages AdA'.' Attributes', 'fr_FR' ),
		'parent_item_colon'     => __( 'Parent Item:', 'fr_FR' ),
		'all_items'             => __( 'Tous', 'fr_FR' ),
		'add_new_item'          => __( 'Ajouter', 'fr_FR' ),
		'add_new'               => __( 'Nouveau', 'fr_FR' ),
		'new_item'              => __( 'Nouveau', 'fr_FR' ),
		'edit_item'             => __( 'Éditer', 'fr_FR' ),
		'update_item'           => __( 'Mettre à jour', 'fr_FR' ),
		'view_item'             => __( 'Voir', 'fr_FR' ),
		'view_items'            => __( 'Voir', 'fr_FR' ),
		'search_items'          => __( 'Rechercher', 'fr_FR' ),
		'not_found'             => __( 'Non trouvé', 'fr_FR' ),
		'not_found_in_trash'    => __( 'Non trouvé dans la corbeille', 'fr_FR' ),
		'featured_image'        => __( 'Image mise en avant', 'fr_FR' ),
		'set_featured_image'    => __( 'Définir l\'image mise en avant', 'fr_FR' ),
		'remove_featured_image' => __( 'Supprimer l\'image', 'fr_FR' ),
		'use_featured_image'    => __( 'Utilisé comme image', 'fr_FR' ),
		'insert_into_item'      => __( 'Insérer', 'fr_FR' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'fr_FR' ),
		'items_list'            => __( 'Items list', 'fr_FR' ),
		'items_list_navigation' => __( 'Items list navigation', 'fr_FR' ),
		'filter_items_list'     => __( 'Filter items list', 'fr_FR' ),
	);
	$args = array(
		'label'                 => __( 'Page AdA', 'fr_FR' ),
		'description'           => __( 'Page AdA'.'.', 'fr_FR' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes', 'comments' ),
		//'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capabilities' => array(
			'edit_post'              => 'edit_ada',
			'read_post'              => 'read_ada',
			'delete_post'            => 'delete_ada',
			'create_posts'           => 'create_ada',
			'edit_posts'             => 'edit_ada',
			'edit_others_posts'      => 'edit_others_ada',
			'publish_posts'          => 'publish_ada',
			'read_private_posts'     => 'read_ada',
			'read'                   => 'read',
			'delete_posts'           => 'delete_ada',
			'delete_private_posts'   => 'delete_private_ada',
			'delete_published_posts' => 'delete_published_ada',
			'delete_others_posts'    => 'delete_others_ada',
			'edit_private_posts'     => 'edit_private_ada',
			'edit_published_posts'   => 'edit_published_ada'
		),
		'rewrite' => array('slug' => 'ada','with_front' => false),
	);
	register_post_type( 'page-ada', $args );

	$labels = array(
		'name'                  => _x( 'Actus AdA', 'Post Type General Name', 'fr_FR' ),
		'singular_name'         => _x( 'Actu AdA', 'Post Type Singular Name', 'fr_FR' ),
		'menu_name'             => __( 'Actus AdA', 'fr_FR' ),
		'name_admin_bar'        => __( 'Actus AdA', 'fr_FR' ),
		'archives'              => __( 'Actus AdA'.' Archives', 'fr_FR' ),
		'attributes'            => __( 'Actus AdA'.' Attributes', 'fr_FR' ),
		'parent_item_colon'     => __( 'Parent Item:', 'fr_FR' ),
		'all_items'             => __( 'Tous', 'fr_FR' ),
		'add_new_item'          => __( 'Ajouter', 'fr_FR' ),
		'add_new'               => __( 'Nouveau', 'fr_FR' ),
		'new_item'              => __( 'Nouveau', 'fr_FR' ),
		'edit_item'             => __( 'Éditer', 'fr_FR' ),
		'update_item'           => __( 'Mettre à jour', 'fr_FR' ),
		'view_item'             => __( 'Voir', 'fr_FR' ),
		'view_items'            => __( 'Voir', 'fr_FR' ),
		'search_items'          => __( 'Rechercher', 'fr_FR' ),
		'not_found'             => __( 'Non trouvé', 'fr_FR' ),
		'not_found_in_trash'    => __( 'Non trouvé dans la corbeille', 'fr_FR' ),
		'featured_image'        => __( 'Image mise en avant', 'fr_FR' ),
		'set_featured_image'    => __( 'Définir l\'image mise en avant', 'fr_FR' ),
		'remove_featured_image' => __( 'Supprimer l\'image', 'fr_FR' ),
		'use_featured_image'    => __( 'Utilisé comme image', 'fr_FR' ),
		'insert_into_item'      => __( 'Insérer', 'fr_FR' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'fr_FR' ),
		'items_list'            => __( 'Items list', 'fr_FR' ),
		'items_list_navigation' => __( 'Items list navigation', 'fr_FR' ),
		'filter_items_list'     => __( 'Filter items list', 'fr_FR' ),
	);
	$args = array(
		'label'                 => __( 'Actu AdA', 'fr_FR' ),
		'description'           => __( 'Actu AdA'.'.', 'fr_FR' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'comments' ),
		//'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-testimonial',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capabilities' => array(
			'edit_post'              => 'edit_ada',
			'read_post'              => 'read_ada',
			'delete_post'            => 'delete_ada',
			'create_posts'           => 'create_ada',
			'edit_posts'             => 'edit_ada',
			'edit_others_posts'      => 'edit_others_ada',
			'publish_posts'          => 'publish_ada',
			'read_private_posts'     => 'read_ada',
			'read'                   => 'read',
			'delete_posts'           => 'delete_ada',
			'delete_private_posts'   => 'delete_private_ada',
			'delete_published_posts' => 'delete_published_ada',
			'delete_others_posts'    => 'delete_others_ada',
			'edit_private_posts'     => 'edit_private_ada',
			'edit_published_posts'   => 'edit_published_ada'
		),
		'rewrite' => array('slug' => 'actu-ada','with_front' => false),
	);
	register_post_type( 'actu-ada', $args );

}

add_action('init', 'ada_custom_post_type', 10);



// --- CUSTOM POST TYPE FAGJ ---
function fagj_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Pages FAGJ', 'Post Type General Name', 'fr_FR' ),
		'singular_name'         => _x( 'Page FAGJ', 'Post Type Singular Name', 'fr_FR' ),
		'menu_name'             => __( 'Espace FAGJ', 'fr_FR' ),
		'name_admin_bar'        => __( 'Pages FAGJ', 'fr_FR' ),
		'archives'              => __( 'Pages FAGJ'.' Archives', 'fr_FR' ),
		'attributes'            => __( 'Pages FAGJ'.' Attributes', 'fr_FR' ),
		'parent_item_colon'     => __( 'Parent Item:', 'fr_FR' ),
		'all_items'             => __( 'Tous', 'fr_FR' ),
		'add_new_item'          => __( 'Ajouter', 'fr_FR' ),
		'add_new'               => __( 'Nouveau', 'fr_FR' ),
		'new_item'              => __( 'Nouveau', 'fr_FR' ),
		'edit_item'             => __( 'Éditer', 'fr_FR' ),
		'update_item'           => __( 'Mettre à jour', 'fr_FR' ),
		'view_item'             => __( 'Voir', 'fr_FR' ),
		'view_items'            => __( 'Voir', 'fr_FR' ),
		'search_items'          => __( 'Rechercher', 'fr_FR' ),
		'not_found'             => __( 'Non trouvé', 'fr_FR' ),
		'not_found_in_trash'    => __( 'Non trouvé dans la corbeille', 'fr_FR' ),
		'featured_image'        => __( 'Image mise en avant', 'fr_FR' ),
		'set_featured_image'    => __( 'Définir l\'image mise en avant', 'fr_FR' ),
		'remove_featured_image' => __( 'Supprimer l\'image', 'fr_FR' ),
		'use_featured_image'    => __( 'Utilisé comme image', 'fr_FR' ),
		'insert_into_item'      => __( 'Insérer', 'fr_FR' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'fr_FR' ),
		'items_list'            => __( 'Items list', 'fr_FR' ),
		'items_list_navigation' => __( 'Items list navigation', 'fr_FR' ),
		'filter_items_list'     => __( 'Filter items list', 'fr_FR' ),
	);
	$args = array(
		'label'                 => __( 'Page FAGJ', 'fr_FR' ),
		'description'           => __( 'Page FAGJ'.'.', 'fr_FR' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes', 'comments' ),
		//'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capabilities' => array(
			'edit_post'              => 'edit_fagj',
			'read_post'              => 'read_fagj',
			'delete_post'            => 'delete_fagj',
			'create_posts'           => 'create_fagj',
			'edit_posts'             => 'edit_fagj',
			'edit_others_posts'      => 'edit_others_fagj',
			'publish_posts'          => 'publish_fagj',
			'read_private_posts'     => 'read_fagj',
			'read'                   => 'read',
			'delete_posts'           => 'delete_fagj',
			'delete_private_posts'   => 'delete_private_fagj',
			'delete_published_posts' => 'delete_published_fagj',
			'delete_others_posts'    => 'delete_others_fagj',
			'edit_private_posts'     => 'edit_private_fagj',
			'edit_published_posts'   => 'edit_published_fagj'
		),
		'rewrite' => array('slug' => 'fagj','with_front' => false),
	);
	register_post_type( 'page-fagj', $args );

	$labels = array(
		'name'                  => _x( 'Actus FAGJ', 'Post Type General Name', 'fr_FR' ),
		'singular_name'         => _x( 'Actu FAGJ', 'Post Type Singular Name', 'fr_FR' ),
		'menu_name'             => __( 'Actus FAGJ', 'fr_FR' ),
		'name_admin_bar'        => __( 'Actus FAGJ', 'fr_FR' ),
		'archives'              => __( 'Actus FAGJ'.' Archives', 'fr_FR' ),
		'attributes'            => __( 'Actus FAGJ'.' Attributes', 'fr_FR' ),
		'parent_item_colon'     => __( 'Parent Item:', 'fr_FR' ),
		'all_items'             => __( 'Tous', 'fr_FR' ),
		'add_new_item'          => __( 'Ajouter', 'fr_FR' ),
		'add_new'               => __( 'Nouveau', 'fr_FR' ),
		'new_item'              => __( 'Nouveau', 'fr_FR' ),
		'edit_item'             => __( 'Éditer', 'fr_FR' ),
		'update_item'           => __( 'Mettre à jour', 'fr_FR' ),
		'view_item'             => __( 'Voir', 'fr_FR' ),
		'view_items'            => __( 'Voir', 'fr_FR' ),
		'search_items'          => __( 'Rechercher', 'fr_FR' ),
		'not_found'             => __( 'Non trouvé', 'fr_FR' ),
		'not_found_in_trash'    => __( 'Non trouvé dans la corbeille', 'fr_FR' ),
		'featured_image'        => __( 'Image mise en avant', 'fr_FR' ),
		'set_featured_image'    => __( 'Définir l\'image mise en avant', 'fr_FR' ),
		'remove_featured_image' => __( 'Supprimer l\'image', 'fr_FR' ),
		'use_featured_image'    => __( 'Utilisé comme image', 'fr_FR' ),
		'insert_into_item'      => __( 'Insérer', 'fr_FR' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'fr_FR' ),
		'items_list'            => __( 'Items list', 'fr_FR' ),
		'items_list_navigation' => __( 'Items list navigation', 'fr_FR' ),
		'filter_items_list'     => __( 'Filter items list', 'fr_FR' ),
	);
	$args = array(
		'label'                 => __( 'Actu FAGJ', 'fr_FR' ),
		'description'           => __( 'Actu FAGJ'.'.', 'fr_FR' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'comments' ),
		//'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-testimonial',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capabilities' => array(
			'edit_post'              => 'edit_fagj',
			'read_post'              => 'read_fagj',
			'delete_post'            => 'delete_fagj',
			'create_posts'           => 'create_fagj',
			'edit_posts'             => 'edit_fagj',
			'edit_others_posts'      => 'edit_others_fagj',
			'publish_posts'          => 'publish_fagj',
			'read_private_posts'     => 'read_fagj',
			'read'                   => 'read',
			'delete_posts'           => 'delete_fagj',
			'delete_private_posts'   => 'delete_private_fagj',
			'delete_published_posts' => 'delete_published_fagj',
			'delete_others_posts'    => 'delete_others_fagj',
			'edit_private_posts'     => 'edit_private_fagj',
			'edit_published_posts'   => 'edit_published_fagj'
		),
		'rewrite' => array('slug' => 'actu-fagj','with_front' => false),
	);
	register_post_type( 'actu-fagj', $args );

}

add_action('init', 'fagj_custom_post_type', 10);


// --- CUSTOM POST TYPE ACTUS NATIONALES ---
function national_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Actus nationales', 'Post Type General Name', 'fr_FR' ),
		'singular_name'         => _x( 'Actu nationale', 'Post Type Singular Name', 'fr_FR' ),
		'menu_name'             => __( 'Actus nationales', 'fr_FR' ),
		'name_admin_bar'        => __( 'Actus nationales', 'fr_FR' ),
		'archives'              => __( 'Actus nationales'.' Archives', 'fr_FR' ),
		'attributes'            => __( 'Actus nationales'.' Attributes', 'fr_FR' ),
		'parent_item_colon'     => __( 'Parent Item:', 'fr_FR' ),
		'all_items'             => __( 'Tous', 'fr_FR' ),
		'add_new_item'          => __( 'Ajouter', 'fr_FR' ),
		'add_new'               => __( 'Nouveau', 'fr_FR' ),
		'new_item'              => __( 'Nouveau', 'fr_FR' ),
		'edit_item'             => __( 'Éditer', 'fr_FR' ),
		'update_item'           => __( 'Mettre à jour', 'fr_FR' ),
		'view_item'             => __( 'Voir', 'fr_FR' ),
		'view_items'            => __( 'Voir', 'fr_FR' ),
		'search_items'          => __( 'Rechercher', 'fr_FR' ),
		'not_found'             => __( 'Non trouvé', 'fr_FR' ),
		'not_found_in_trash'    => __( 'Non trouvé dans la corbeille', 'fr_FR' ),
		'featured_image'        => __( 'Image mise en avant', 'fr_FR' ),
		'set_featured_image'    => __( 'Définir l\'image mise en avant', 'fr_FR' ),
		'remove_featured_image' => __( 'Supprimer l\'image', 'fr_FR' ),
		'use_featured_image'    => __( 'Utilisé comme image', 'fr_FR' ),
		'insert_into_item'      => __( 'Insérer', 'fr_FR' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'fr_FR' ),
		'items_list'            => __( 'Items list', 'fr_FR' ),
		'items_list_navigation' => __( 'Items list navigation', 'fr_FR' ),
		'filter_items_list'     => __( 'Filter items list', 'fr_FR' ),
	);
	$args = array(
		'label'                 => __( 'Actu nationale', 'fr_FR' ),
		'description'           => __( 'Actu nationale'.'.', 'fr_FR' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'comments' ),
		//'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-testimonial',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capabilities' => array(
			'edit_post'              => 'edit_ada',
			'read_post'              => 'read_ada',
			'delete_post'            => 'delete_ada',
			'create_posts'           => 'create_ada',
			'edit_posts'             => 'edit_ada',
			'edit_others_posts'      => 'edit_others_ada',
			'publish_posts'          => 'publish_ada',
			'read_private_posts'     => 'read_ada',
			'read'                   => 'read',
			'delete_posts'           => 'delete_ada',
			'delete_private_posts'   => 'delete_private_ada',
			'delete_published_posts' => 'delete_published_ada',
			'delete_others_posts'    => 'delete_others_ada',
			'edit_private_posts'     => 'edit_private_ada',
			'edit_published_posts'   => 'edit_published_ada'
		),
		'rewrite' => array('slug' => 'actu-nationale','with_front' => false),
	);
	register_post_type( 'actu-nationale', $args );

}

add_action('init', 'national_custom_post_type', 10);




// DEBUT de la partie intégration de data
// function is_current_request_for_data() {
//     global $wp;
//     if (strpos($wp->request, 'outils/') === 0)  {
// 	$outilsHorsData = array(
// 	    'outils/recueil-des-actions-pour-inspiration',
// 	    'outils/annuaire-des-serveurs-discord-2',
// 	    'outils/espace-ada'
// 	);
// 	$horsData = false;
// 	foreach ($outilsHorsData as $outil) if (strpos($wp->request, $outil) === 0) $horsData = true;
//     	if ($horsData) {
//     		return false;
//     	} else {
//     		return true;
//     	}
//     }
//     return false;
// }


// add_filter('template_include', function($template) {
//     if (is_current_request_for_data()) {
//         return locate_template('reverseproxy.php');
//     } else {
//     	return $template;
//     }
// });

// function filter_pre_handle_404( $false, $wp_query ) {
//     return is_current_request_for_data();
// };
// add_filter( 'pre_handle_404', 'filter_pre_handle_404', 10, 2 );



// function changeHeaders($headers) {
//     global $wp;
//     if (strpos($wp->request, 'outils/css/') === 0 || substr($wp->request, -4) == '.css') $headers['Content-Type'] = 'text/css';
//     if (strpos($wp->request, 'outils/js/') === 0 || substr($wp->request, -3) == '.js') $headers['Content-Type'] = 'application/javascript';

//     return $headers;
// }
// add_filter('wp_headers', 'changeHeaders');

// add_filter( 'redirect_canonical', function( $redirect_url, $requested_url ){
//     if (is_current_request_for_data()) {
//         $redirect_url = $requested_url;
//     }
//     return $redirect_url;
// }, 10, 2 );

// // FIN de la partie intégration de data


