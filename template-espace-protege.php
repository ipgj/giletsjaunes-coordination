<?php
/*
Template Name: Departement - Espace protégé
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container section_departement espace_protege">

		<?php include('templatechunk-departement-nav.php'); ?>

		<main class="clearfix">

			<?php $parentId = $post->post_parent;
			$have_moderateur = get_field('have_moderateur', $parentId);
			if(!$have_moderateur) { ?>
				<div class="no_moderator">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/warning_icon_white.png"/>
					</div>
					<div class="texte">
						<?php echo get_field('message_aucun_moderateur', 'option'); ?>
					</div>
				</div>
			<?php } ?>			

			<section class="entete_page">

				<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/espace_protege', 'espace_protege' );
				endwhile; // End of the loop.
				?>

			</section>

			<?php if ( !post_password_required() ) { ?>

				<div class="boxed_content shadowed_box">

					<section class="entete">

						<?php $pinfo_image = get_field('pinned_info_image');
						$pinfo_texte = get_field('pinned_info_texte');

						if($pinfo_texte != '') { ?>
							<div class="pinned_info">
								<div class="pin_container">
									<img class="pin" src="<?php echo get_template_directory_uri(); ?>/images/pin_icon.png"/>
								</div>
								<div class="content">
									<?php if($pinfo_image != '') { ?>
										<div class="img_container">
											<img src="<?php echo $pinfo_image['sizes']['medium']; ?>"/>
										</div>
									<?php } ?>
									<div class="texte_container">
										<?php echo $pinfo_texte; ?>
									</div>
								</div>
							</div>
						<?php } ?>

						<div class="protected_forum_link_container">
							<a href="#forums-proteges" class="bouton_type_2 b js-scrollTo">Forums protégés <i class="fas fa-chevron-down"></i></a>
						</div>

					</section>

					<section class="liste_protected_news">

						<h3 class="titre_section">Actualités privées</h3>

						<?php
						$post_data = get_post($post->post_parent);
						$parent_slug = $post_data->post_name;
						$post_type_name = 'actu'.strstr($parent_slug, '-');

						$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

						$item_per_page = get_field('nombre_dactualites_par_page');
						$args = array(
						    'post_type'      => $post_type_name,
						    'posts_per_page' => $item_per_page,
						    'order'          => 'DESC',
						    'orderby'        => 'date',
						    'paged' => $paged,
							'meta_query'	=> array(
								array(
									'key'	  	=> 'contenu_protege',
									'value'	  	=> 'content_is_protected',
									'compare' 	=> 'LIKE',
								),
							),
						 );

						$news = new WP_Query( $args );

						if ( $news->have_posts() ) {

						    while ( $news->have_posts() ) : $news->the_post();

						    	// N'AFFICHER QUE LES ACTUS PROTÉGÉES
						    	$is_protected = get_field('contenu_protege');
						    	if($is_protected AND $is_protected[0] == 'content_is_protected') {

							    	$img = get_the_post_thumbnail();
							    	if(!$img OR $img == '') { $img = '<img src="'.get_template_directory_uri().'/images/no_image.jpg"/>'; }
							    	$date = get_the_date(get_option('date_format'));
									$text_unformed = get_the_content();
									$text = wpautop($text_unformed);
									$type_evenement = get_field('type_devenement');
									$emplacement = get_field('emplacement_geographique');

							    	$page_link_ID = get_the_ID(); 
							    	$current = '';
							    	if($page_link_ID == $current_page_ID) { $current = 'active'; } ?>
							    	<div class="news_container row no-gutters">
							    		<div class="gauche col-lg-3">
							    			<?php echo $img; ?>
							    		</div>
							    		<div class="droite col-lg-9">
							    			<div class="date">> Publié le <?php echo $date; ?></div>
							    			<a class="titre <?php echo $current; ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
							    			<div class="entete">
							    				<?php if($type_evenement != '') { echo '<span class="type">'.$type_evenement.'</span>'; } ?>
							    				<?php if($type_evenement != '' AND $emplacement != '') { echo '<span class="tiret"> - </span>'; } ?>
							    				<?php if($emplacement != '') { echo '<span class="emplacement">'.$emplacement.'</span>'; } ?>
							    			</div>
							    			<div class="texte">
							    				<?php echo $text; ?>
							    			</div>
							    		</div>
							    	</div>

							    	<div class="news_separateur"></div>

							    <?php } ?>
						        
						    <?php endwhile;
						} else {
							echo '<p class="bloc no_result">Aucune actualité privée n\'a été ajouté dans ce département pour le moment...</p>';
						}

						wp_reset_postdata();
						?>

					</section>

					<?php $big = 999999999;
				    echo '<section class="wp_pagination">';
						echo paginate_links( array( // Plus d'info sur les arguments possibles : https://codex.wordpress.org/Function_Reference/paginate_links
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total' => $news->max_num_pages
						) );
					echo '</section>'; ?>


					<section id="forums-proteges" class="protected_forum mt-5">

						<h3 class="titre_section">Forums privés</h3>

						<?php 
						$parent_page = get_post($post->post_parent);
						$parent_page_ID = $parent_page->ID;
						$corresponding_forum_id = get_corresponding_forum_dep_root($parent_page_ID);

						$args = array(
						    'post_type'      => 'forum',
						    'posts_per_page' => -1,
						    'post_parent'    => $corresponding_forum_id,
						    'order'          => 'ASC',
						    'orderby'        => 'menu_order'
						 );
						$sub_forums = new WP_Query( $args );
						if ($sub_forums->have_posts()) :
						    while ( $sub_forums->have_posts() ) : $sub_forums->the_post();
						    	$is_protected = get_field('contenu_protege');
								if($is_protected AND $is_protected[0] == 'content_is_protected') {
									$protected_forum_id = get_the_ID(); 
								}
						    endwhile;
						endif;

						if($corresponding_forum_id != '' AND $corresponding_forum_id != 0) {
							$bbpress_shortcode = '[bbp-single-forum id='.$protected_forum_id.']';
							echo do_shortcode($bbpress_shortcode); 
						} else {
							echo '<p class="bloc no_result">Aucune forum privé n\'est connecté à ce département pour le moment...</p>';
						}
						?>

					</section>

				</div>

			<?php } ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




