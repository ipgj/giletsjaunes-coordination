		<?php $page_depth = get_current_page_depth(); ?>
		
		<nav class="nav_departement_container">

			<div class="container">
				<div class="flex_container">

					<div class="nom_departement">
						<div class="left_filler"></div>
						
						<?php 
						if(isset($departement_mainpage_ID)) {
							$linkToParent = get_permalink($departement_mainpage_ID); 
							$title = get_the_title($departement_mainpage_ID); ?>
							<a href="<?php echo $linkToParent; ?>"><?php echo $title; ?></a>
						<?php } else {
							//echo 'departement_mainpage_ID NOT SET';
							if($page_depth > 0) {
								$parentId = $post->post_parent;
								$linkToParent = get_permalink($parentId); 
								$title = get_the_title($parentId); ?>
								<a href="<?php echo $linkToParent; ?>"><?php echo $title; ?></a>
							<?php } else { ?>
								<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
							<?php } 
						} ?>
					</div>

					<div class="separateur_departement"></div>

					<div class="bouton_menu_responsive">
						<div>Menu</div> <i class="fas fa-caret-down"></i>
					</div>

					<div class="nav_departement">

						<?php 
						if(isset($departement_mainpage_ID)) {
							$target_id = $departement_mainpage_ID;
						} else {
							if($page_depth > 0) {
								$target_id = $parentId;
							} else {
								$target_id = $post->ID;
							}

							//echo 'target ID = '.$target_id;
						}
						
						$args = array(
						    'post_type'      => 'any',
						    'posts_per_page' => -1,
						    'post_parent'    => $target_id,
						    'order'          => 'ASC',
						    'orderby'        => 'menu_order'
						 );

						$souspages = new WP_Query( $args );
						$current_page_ID = get_the_ID();

						if ( $souspages->have_posts() ) :
						    while ( $souspages->have_posts() ) : $souspages->the_post();
						    	
						    	$page_link_ID = get_the_ID(); 
						    	if(!empty($post->post_password)){
									$password_protected_child_ID = $page_link_ID;
								}

								$pages_nav_dep[] = [
									'nom' => get_the_title(), 
									'lien' => get_the_permalink()
								];

						    	$current = '';
						    	if($page_link_ID == $current_page_ID) { $current = 'active'; } ?>
						        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="<?php echo $current; ?>"><?php the_title(); ?></a>
						    <?php endwhile;
						endif; wp_reset_postdata(); ?>

					</div>

				</div>
			</div>

		</nav>