<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gilet_Jaune_France
 */

?>

	</div><!-- #content -->

	<!-- <script>
	var volume = document.getElementById("radioplayer");

	function setVolume() {
      document.getElementById("radioplayer").volume
	    = document.getElementById('volume').value;
	}
	</script>
	<audio id="radioplayer" src="http://91.121.153.12:9107/stream" autoplay="autoplay"></audio>
	<a href="#" onclick="document.getElementById('radioplayer').play()">Hi!</a>
	<a href="#" onclick="document.getElementById('radioplayer').pause()">Pause!</a>
	<input id="volume" name="volume" min="0" max="1" step="0.1" type="range" onchange="setVolume()" /> -->

	<section class="prefooter">
		<div class="container">
			<div class="row">
				<div class="col_acces col-lg-7">
					<h4><?= get_field('carte_titre_acces_rapide', 20); ?></h4>
					<div class="outils_nat_container row">
						<?php if(have_rows('outils_nat_liste_outils', 20)) : while(have_rows('outils_nat_liste_outils', 20)) : the_row(); 
							$image = get_sub_field('image', 20); 
							$texte = get_sub_field('texte', 20);
							$lien = get_sub_field('lien', 20); ?>
							<a class="lien col-6 col-sm-4 col-md-3" href="<?= $lien ?>">
								<img class="icon" src="<?= $image['sizes']['medium']; ?>" alt="<?= $image['alt']; ?>" />
								<div class="titre"><?= $texte; ?></div>
							</a>
						<?php endwhile; endif; ?>
					</div>
				</div>
				<div class="col_carte col-lg-5">
					<h4><?= get_field('carte_titre', 20); ?></h4>
					<div class="texte"><?= get_field('carte_texte', 20); ?></div>
					<?php $image = get_field('carte_image', 20); ?>
					<a href="<?= get_field('carte_lien_butoon', 20); ?>">
						<img src="<?php echo $image['sizes']['large']; ?>" alt="Carte">
					</a>
					<a href="<?= get_field('carte_lien_butoon', 20); ?>" class="bouton_type_1"><?= get_field('carte_texte_bouton', 20); ?></a>
				</div>
			</div>
		</div>
	</section>

	<!-- FOOTER -->
	<footer class="row no-gutters">

		<div class="col_droite col-lg-3">
			<h4><?php echo get_field('titre_footer_col_3', 'option'); ?></h4>
			<p class="txt_newsletter">
				<?php echo get_field('texte_newsletter', 'option'); ?>
			</p>
			<!-- Begin Mailchimp Signup Form -->
			<div id="mc_embed_signup2">
				<form action="https://giletjaune-france.us7.list-manage.com/subscribe/post?u=d74273426ae369ab9798e66f1&amp;id=bb63a24032" method="post" id="mc-embedded-subscribe-form2" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				    <div id="mc_embed_signup_scroll2">
						<div class="mc-field-group">
							<label for="mce-EMAIL2" style="display:none">Email</label>
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL2" placeholder="Votre e-mail...">
						</div>
						<div class="disclaimer">
							<input class="conditions_checkbox" type="checkbox" name="checked_conditions" value="1"><span for="checked_conditions" class="ml-2">J'accepte la <a href="<?=get_permalink(3)?>">politique de confidentialité</a>.</span>
						</div>
						<div id="mce-responses2" class="clear">
							<div class="response" id="mce-error-response2" style="display:none"></div>
							<div class="response" id="mce-success-response2" style="display:none"></div>
						</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d74273426ae369ab9798e66f1_bb63a24032" tabindex="-1" value=""></div>
					    <div class="clear"><input type="submit" value="Inscription" name="subscribe" id="mc-embedded-subscribe2" class="button bouton_type_1 disabled" disabled></div>
				    </div>
				</form>
			</div>
			<!--End mc_embed_signup-->
		</div>

		<div class="col_milieu col-lg-5">
			<h4><?php echo get_field('titre_footer_col_2', 'option'); ?></h4>
			<?php
			if(have_rows('footer_contacts_utiles', 'option')) :
			    while(have_rows('footer_contacts_utiles', 'option')) : the_row();
			    	$icon = get_sub_field('footer_cu_icone');
			    	$text = get_sub_field('footer_cu_texte');
			    	$email = get_sub_field('footer_cu_adresse');

			    	echo '<div class="ligne">';
			    		echo $icon.' <span>'.$text.' : <a href="mailto:'.$email.'">'.$email.'</a></span>';
			    	echo '</div>';
				    endwhile;
			endif;
			?>
		</div>

		<div class="col_gauche col-lg-4">
			<div class="gauche">
				<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_portailgj.png" alt="Logo portail"/></a>
			</div>
			<div class="droite">
				<h4><?php echo get_field('titre_footer_col_1', 'option'); ?></h4>
				<?php wp_nav_menu( array( 'menu' => 'menu-footer' ) ); ?>
			</div>
		</div>

		<div class="bas_footer col-lg-12">
			<a href="<?= get_permalink(628); ?>">Conditions générales d'utilisation</a> - <!-- <a href="/soutien-financier/">Soutenir financièrement</a> --><a href="<?= get_permalink(3); ?>">Politique de confidentialité</a> - <a href="<?= get_permalink(103957); ?>">Notes de mise à jour</a>
		</div>

	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>