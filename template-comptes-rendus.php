<?php
/*
Template Name: Departement - Comptes-rendus
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container section_departement">

		<?php include('templatechunk-departement-nav.php'); ?>
		
		<main class="clearfix">

			<?php $parentId = $post->post_parent;
			$have_moderateur = get_field('have_moderateur', $parentId);
			if(!$have_moderateur) { ?>
				<div class="no_moderator">
					<div class="icon">
						<img src="<?= get_template_directory_uri(); ?>/images/warning_icon_white.png"/>
					</div>
					<div class="texte">
						<?= get_field('message_aucun_moderateur', 'option'); ?>
					</div>
				</div>
			<?php } ?>

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title">Les documents et compte-rendus Gilet Jaune : <?= explode(')', get_the_title($target_id))[1] ?></h1>
					</header>
				</article>
				<p><?= get_field('texte_entete_documents_cr', 'option'); ?></p>
			</section>

			<div class="boxed_content shadowed_box">

				<?php
				$pinfo_image = get_field('pinned_info_image');
				$pinfo_texte = get_field('pinned_info_texte'); 
				
				if($pinfo_texte != '') { ?>
					<div class="pinned_info">
						<div class="pin_container">
							<img class="pin" src="<?= get_template_directory_uri(); ?>/images/pin_icon.png"/>
						</div>
						<div class="content">
							<?php if($pinfo_image != '') { ?>
								<div class="img_container">
									<img src="<?php echo $pinfo_image['sizes']['medium']; ?>"/>
								</div>
							<?php } ?>
							<div class="texte_container">
								<?= $pinfo_texte; ?>
							</div>
						</div>
					</div>
				<?php } ?>

				<section class="liste_fichiers">

					<?php
					if( get_query_var('page') ) {
						$page = get_query_var( 'page' );
					} else {
						$page = 1;
					}

					// Options et calculs pagination ACF
					$row              = 0;
					$element_per_page = get_field('nombre_compte_rendu_par_page');
					$repeater         = get_field('comptes_rendus_liste');
					$total            = count( $repeater );
					$pages            = ceil( $total / $element_per_page );
					$min              = ( ( $page * $element_per_page ) - $element_per_page ) + 1;
					$max              = ( $min + $element_per_page ) - 1;

					// Loop ACF
					if(have_rows('comptes_rendus_liste')) : 
						while(have_rows('comptes_rendus_liste')) : the_row(); 

							$row++;
						    if($row < $min) { continue; }
						    if($row > $max) { break; }

							$surtitre = get_sub_field('surtitre');
							$titre = get_sub_field('titre_lien');
							$fichier = get_sub_field('fichier');
							if($fichier['subtype'] == 'pdf') { $icon = 'icon_pdf.png'; }
							elseif($fichier['subtype'] == 'jpg') { $icon = 'icon_jpg.png'; }
							elseif($fichier['subtype'] == 'png') { $icon = 'icon_png.png'; }
							elseif($fichier['subtype'] == 'vnd.openxmlformats-officedocument.wordprocessingml.document') { $icon = 'icon_word.png'; }
							else { $icon = 'icon_file.png'; }
							?>

							<a href="<?= $fichier['url']; ?>" target="_blank" class="ligne">
								<div class="file_icon"><img src="<?= get_template_directory_uri().'/images/'.$icon; ?>"/></div>
								<div class="texte">
									<p class="surtitre"><?= $surtitre; ?></p>
									<p class="titre"><?= $titre; ?></p>
								</div>
							</a>

						<?php endwhile; 

						// Pagination ACF
						echo '<section class="wp_pagination">';
							echo paginate_links( array(
							    'base' => get_permalink() . '%#%' . '/',
							    'format' => '?page=%#%',
							    'current' => $page,
							    'total' => $pages
							) );
						echo '</section>';

					else :
						echo '<p class="bloc no_result">Aucun document n\'a été ajouté dans ce département pour le moment...</p>';
					endif; ?>	

				</section>

				<section class="liste_liens">

					<?php
					if( get_query_var('page') ) {
						$page = get_query_var( 'page' );
					} else {
						$page = 1;
					}

					// Loop ACF
					if(have_rows('liste_de_liens')) : 
						while(have_rows('liste_de_liens')) : the_row(); 
							$surtitre = get_sub_field('surtitre');
							$titre = get_sub_field('titre_lien');
							$lien = get_sub_field('lien');
							$icon = 'icon_link.png';
							?>

							<a href="<?= $lien; ?>" target="_blank" class="ligne">
								<div class="file_icon"><img src="<?= get_template_directory_uri().'/images/'.$icon; ?>"/></div>
								<div class="texte">
									<p class="surtitre"><?= $surtitre; ?></p>
									<p class="titre"><?= $titre; ?></p>
								</div>
							</a>

						<?php endwhile; 

						// Pagination ACF
						echo '<section class="wp_pagination">';
							echo paginate_links( array(
							    'base' => get_permalink() . '%#%' . '/',
							    'format' => '?page=%#%',
							    'current' => $page,
							    'total' => $pages
							) );
						echo '</section>';

					else :
						echo '<p class="bloc no_result">Aucun lien vers un document n\'a été ajouté dans ce département pour le moment...</p>';
					endif; ?>	

				</section>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




