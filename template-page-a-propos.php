<?php
/*
Template Name: Page - À propos
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page">
		<main class="clearfix">

			<section class="entete_page">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content-page', 'content-page' );

				endwhile; // End of the loop.
				?>

			</section>

			<div class="sidebar_zone row no-gutters<?= $sidebar_zone_class ?>">

				<div class="main_content col-lg-9">

					<?php $texte = get_field('texte_principal');
					echo $texte; ?>

				</div>

				<div class="sidebar_container col-lg-3">

					<div class="widget equipe">
						<h4 class="sidebar_title">L'ÉQUIPE</h4>

						<h4 class="sidebar_subtitle">Administration</h4>
						<div class="userlist_container">
							<?php
							$args = array(
							    'role__in' => ['administrateur','administrator'],
							    'orderby' => 'user_nicename',
							    'order'   => 'ASC'
							);
							$users = get_users($args);
							foreach($users as $user) {
								$profile_link = bbp_get_user_profile_edit_url($user->ID);
								//print_r($users);
							    echo '<a href="'.$profile_link.'">'.esc_html($user->display_name).'</a>';
							}
							?>
						</div>
						<h4 class="sidebar_subtitle">Modération</h4>
						<div class="userlist_container">
							<?php
							$args = array(
							    'role'    => 'moderateur',
							    'orderby' => 'user_nicename',
							    'order'   => 'ASC'
							);
							$users = get_users($args);
							foreach($users as $user) {
								$profile_link = bbp_get_user_profile_edit_url($user->ID);
								//print_r($users);
							    echo '<a href="'.$profile_link.'">'.esc_html($user->display_name).'</a>';
							}
							?>
						</div>

						<h4 class="sidebar_subtitle">Administration départementale</h4>
						<p class="infosmall">Voir page "Contact" de chaque département.</p> 

						<div class="separateur"></div>
					</div>

					<div class="widget statistiques">

						<h4 class="sidebar_title">STATISTIQUES</h4>

						<div class="dep_stats_container">
							<a class="open_dep_detail collapsed" data-toggle="collapse" href="#liste_dep_pris_en_charge" role="button" aria-expanded="false" aria-controls="reponse_1">
								<i class="fas fa-arrow-right"></i> Détail des départements administrés
							</a>
							<div id="liste_dep_pris_en_charge" class="reponse collapse">
								<?php $number_of_dep = 0; $nb_moderated_dep = 0; 
								if(have_rows('liens_departements_forums', 'options')) : while(have_rows('liens_departements_forums', 'options')) : the_row(); 

									$number_of_dep++;

									$num = get_sub_field('num');
									$nom = get_sub_field('nom');
									$slug = get_sub_field('slug');
									$main_dep_page = get_sub_field('page_principale_departementale');
									$root_forum = get_sub_field('forum_departemental_racine');

									// STAT - Nombre de départements pris en charge
									$have_moderateur = get_field('have_moderateur', $main_dep_page);
									if($have_moderateur[0] == 'have_moderateur') { 
										$nb_moderated_dep++; 
										$dep_class = 'have_moderateur';
									} else { $dep_class = ''; }
									?>
									<p class="<?= $dep_class ?>"><?= $num.' - '.$nom ?></p>

								<?php
								endwhile; endif; ?>
							</div>

							<?php // STAT - Total d'utilisateurs
							$usercount = count_users();
							$total_users = $usercount['total_users'];

							// STATS FORUMS
							$stats_forums = bbp_get_statistics('count_users');
							?>
						</div>

						<table class="table_stat">
							<tr>
								<td class="label">Utilisateurs</td>
								<td class="valeur"><?= number_format($total_users, 0, ',', ' '); ?></td>
							</tr>
							<tr>
								<td class="label">Total de forums</td>
								<!-- <td class="valeur"><?php number_format($stats_forums['forum_count'], 0, ',', ' '); ?></td> -->
								<td class="valeur">1 216</td>
							</tr>
							<tr>
								<td class="label">Total de sujets</td>
								<td class="valeur"><?= number_format($stats_forums['topic_count']+$stats_forums['topic_count_hidden'], 0, ',', ' ') ?></td>
							</tr>
							<tr>
								<td class="label">Total de réponses</td>
								<td class="valeur"><?= number_format($stats_forums['reply_count']+$stats_forums['reply_count_hidden'], 0, ',', ' ') ?></td>
							</tr>
							<tr>
								<td class="label">Départements administrés</td>
								<td class="valeur"><?= $nb_moderated_dep; ?> / <?= $number_of_dep; ?></td>
							</tr>
						</table>


						<div class="widget_footer">
							<a href="<?= get_permalink(655) ?>" class="bouton_sidebar">STATISTIQUES DÉTAILLÉES</a>
						</div>
						<div class="separateur"></div>

					</div>

				</div>

			</div>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




