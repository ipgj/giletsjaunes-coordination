<?php
/*
Template Name: Page d'inscription - Confirmation
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container">
		<main class="clearfix">

			<div class="info_hautdroite">
				<p>Une fois connecté, un menu utilisateur 
				est disponible en haut à droite du site</p>
				<img src="<?php echo get_template_directory_uri(); ?>/images/fleche_haut_droite.png">
			</div>

			<section class="confirmation_inscription">

				<h1>BIENVENUE ! Votre inscription est validée</h1>

				<h3>La vérification d'adresse e-mail n'étant pas nécessaire, <br/>vous avez été automatiquement connecté(e).</h3>

				<p>Il vous est désormais possible d'<strong>échanger sur les différents forums du site</strong> !<br/>
				Les forums départementaux sont totalement ouverts, il vous est possible d'intervenir quel que soit le département.</p>

				<p>Comme indiqué en haut de page, un <strong>menu utilisateur</strong> est accessible en haut à droite du site pour accéder à vos options et espace personnel.</p>

				<div class="tips_container row">
					<div class="tip_container col-lg-4">
						<div class="bg_gris">
							<p>Vous pouvez choisir votre <strong>avatar</strong> dans vos options de profil, ainsi qu'un <strong>pseudonyme</strong> différent de votre nom d'utilisateur si vous le désirez.</p>
							<a href="<?= get_permalink(270) ?>" class="bouton_type_1">Mon compte</a>
						</div>
					</div>
					<div class="tip_container col-lg-4">
						<div class="bg_gris">
							<p>Vous souhaitez <strong>en savoir plus</strong> sur ce projet ? La page "À propos" saura peut-être vous renseigner, sinon n'hésitez pas à nous contacter directement.</p>
							<a href="<?= get_permalink(444) ?>" class="bouton_type_1">À propos du site</a>
						</div>
					</div>
					<div class="tip_container col-lg-4">
						<div class="bg_gris">
							<p>N’hésitez pas à nous faire part de <strong>toutes remarques ou suggestions pertinentes</strong> pour l’évolution du site dans le forum d'aide "Suggestions" !</p>
							<a href="<?= get_permalink(651) ?>" class="bouton_type_1">Forum "Suggestions"</a>
						</div>
					</div>
				</div>

				<h6>Nous vous souhaitons une bonne navigation sur le site :)</h6>

				<a href="<?= get_home_url() ?>" class="bouton_type_1 retour_accueil">Retour à l'accueil</a>

			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




