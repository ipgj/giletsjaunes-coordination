<?php
/*
Template Name: Page d'Accueil
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

<div class="modal bare fade show" id="info_fusion" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Informations sur la fusion</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row no-gutters">
					<div class="col_image col-lg-6">
						<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/illustration_fusion.jpg" alt="Fusion entre GJ-F et GJ-C"/>
					</div>
					<div class="col_texte col-lg-6">
						<h4>Giletjaune-france.fr et<br/>Giletsjaunes-coordination.fr fusionnent&nbsp;!</h4>
						<p>Retrouvez tous les outils de ces 2 projets combinés (et bien d'autres...) en une seule plateforme unifiée.</p>
						<p>Il s'agit d'une première version, de nombreuses corrections et améliorations sont à venir.</p>
						<ul>
							<li>Informations sur le mouvement GJ</li>
							<li>Outils de coordination et d'échange</li>
							<li>Espace dédié pour chaque département de France</li>
							<li>Outils de diffusion "Assemblée des assemblées"</li>
							<li>Forums de discussion</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="main_content_container">

		<section class="entete">
			<div class="info_message">
				<p class="texte"><i class="fas fa-info-circle"></i> Cette nouvelle plateforme est le résultat de la fusion de <span class="site">giletjaune-france.fr</span> et <span class="site">giletsjaunes-coordination.fr</span> !</p>
				<div class="text-right">
					<a href="#info_fusion" class="bouton_type_1" data-toggle="modal" data-target="#info_fusion">Plus d'infos</a>
				</div>
			</div>
			<div class="top">
				<div class="container">
					<img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo_portailgj.png" alt="Logo site"/>
					<h1><?= get_field('entete_titre'); ?></h1>
					<h2><?= get_field('entete_soustitre'); ?></h2>
					<p><?= get_field('entete_phrase'); ?></p>
					<div class="boutons_container">
						<?php if(have_rows('entete_boutons')) : while(have_rows('entete_boutons')) : the_row(); 
						$texte = get_sub_field('texte');
						$lien = get_sub_field('lien'); ?>
							<a href="<?= $lien; ?>" class="bouton_type_1"><?= $texte ?></a>
						<?php endwhile; endif; ?>
					</div>
				</div>
				<div class="overlay"></div>
				<?php $img = get_field('image_entete_accueil'); ?>
				<img class="fond" src="<?= $img['sizes']['fullhd'] ?>" title="<?= $img['title'] ?>" alt="<?= $img['alt'] ?>" />
			</div>
			<div class="bottom">
				<div class="container">
					<div class="row">
						<div class="col_texte col-lg-5">
							<?= get_field('entete_texte_bas'); ?>
						</div>
						<div class="col_acces col-lg-5">
							<div class="outils_nat_container row">
								<?php if(have_rows('outils_nat_liste_outils')) : while(have_rows('outils_nat_liste_outils')) : the_row(); 
									$image = get_sub_field('image'); 
									$texte = get_sub_field('texte');
									$lien = get_sub_field('lien'); ?>
									<a class="lien col-6 col-sm-4 col-md-3" href="<?= $lien ?>">
										<img class="icon" src="<?= $image['sizes']['medium']; ?>" alt="<?= $image['alt']; ?>" />
										<div class="titre"><?= $texte; ?></div>
									</a>
								<?php endwhile; endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="cols_explications">

			<div class="container">
				<div class="row no-gutters">
					<div class="col-lg-4">
						<p class="surtitre"><?= get_field('gauche_surtitre'); ?></p>
						<h2><?= get_field('gauche_titre'); ?></h2>
						<?= get_field('gauche_texte'); ?>
						<div class="mt-4">
							<a href="<?= get_field('gauche_lien_bouton'); ?>" class="bouton_type_1"><?= get_field('gauche_txtbouton'); ?></a>
						</div>
					</div>
					<div class="col-lg-4">
						<p class="surtitre"><?= get_field('milieu_surtitre'); ?></p>
						<h2><?= get_field('milieu_titre'); ?></h2>
						<?= get_field('milieu_texte'); ?>
						<div class="mt-4">
							<a href="<?= get_field('milieu_lien_bouton'); ?>" class="bouton_type_1"><?= get_field('milieu_txtbouton'); ?></a>
						</div>
					</div>
					<div class="col-lg-4">
						<p class="surtitre"><?= get_field('droite_surtitre'); ?></p>
						<h2><?= get_field('droite_titre'); ?></h2>
						<?= get_field('droite_texte'); ?>
						<div class="mt-4">
							<a href="<?= get_field('droite_lien_bouton'); ?>" class="bouton_type_1"><?= get_field('droite_txtbouton'); ?></a>
						</div>
					</div>
				</div>
			</div>

		</section>

		<section class="dernieres_actus">
			<div class="container">
				<h2>Actualités, événements, actions, rendez-vous des Gilets Jaunes</h2>
				<div class="actus_container row">
					<?php
					// Récupération des Customs Post Types de tous les deps
					$dep_forum_links_array = get_dep_forum_links_array();
					foreach($dep_forum_links_array as $dep) {
						$deps['post_type'][] = 'actu-'.$dep['slug'];
						$deps['full_name'][] = $dep['num'].' - '.$dep['nom'];
					}
					// Ajout des actus ADA
					$deps['post_type'][] = 'actu-ada';
					$deps['full_name'][] = 'ADA';
					// Ajout des actus FAGJ
					$deps['post_type'][] = 'actu-fagj';
					$deps['full_name'][] = 'FAGJ';
					// Ajout des actus nationales
					$deps['post_type'][] = 'actu-nationale';
					$deps['full_name'][] = 'Actualité nationale';

					$args = array(
						'post_type'      => $deps['post_type'],
						'posts_per_page' => 6,
						'order'          => 'DESC',
						'orderby'        => 'date',
						'meta_query'	=> array(
							'relation' => 'OR',
							array(
								'key'	  	=> 'contenu_protege',
								'value'	  	=> '',
								'compare' 	=> '=',
							),
							array(
								'key'	  	=> 'contenu_protege',
								'value'	  	=> '',
								'compare' 	=> 'NOT EXISTS',
							),
						),
					);

					$all_news = new WP_Query( $args );

					if ( $all_news->have_posts() ) :
						while ( $all_news->have_posts() ) : $all_news->the_post(); 

							$post_type = get_post_type();
							$key = array_search($post_type, $deps['post_type']);
							$nom_departement = $deps['full_name'][$key];

							$img = get_the_post_thumbnail();
							if(!$img OR $img == '') { $img = '<img src="'.get_template_directory_uri().'/images/no_image.jpg"/>'; }
							$date = get_the_date(get_option('date_format'));

							$type_evenement = get_field('type_devenement');
							$emplacement = get_field('emplacement_geographique');

							$comment_number = get_comments_number();
							if($comment_number == 0) { $comment_text = ''; }
							if($comment_number == 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaire'; }
							if($comment_number > 1) { $comment_text = '<i class="far fa-comment"></i>'.$comment_number.' commentaires'; }
							?>

							<div class="news_container col-lg-4">
								<div class="onglet_dep">
									<p><?= $nom_departement; ?></p>
									<div class="triangle"></div>
								</div>
								<div class="img_container gauche">
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo $img; ?></a>
								</div>
								<div class="bottom droite">
									<a class="titre <?php echo $current; ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h3><?php the_title(); ?></h3></a>
									<div class="entete">
										<?php if($type_evenement != '') { echo '<span class="type">'.$type_evenement.'</span>'; } ?>
										<?php if($type_evenement != '' AND $emplacement != '') { echo '<span class="tiret"> - </span>'; } ?>
										<?php if($emplacement != '') { echo '<span class="emplacement">'.$emplacement.'</span>'; } ?>
									</div>
									<!-- <div class="date">> Publié le <?php echo $date; ?></div>
									<?php if($comment_number > 0) { ?>
										<div class="nb_comment"><?= $comment_text; ?></div>
									<?php } ?> -->
								</div>
							</div>

						<?php endwhile;
					else :
						echo '<p class="bloc no_result">Aucune actualité...</p>';
					endif;

					wp_reset_postdata(); ?>
				</div>

				<div class="see_all_container">
					<a href="<?=get_permalink(102205)?>" class="bouton_type_1">Voir toutes les actus</a>
				</div>
			</div>
		</section>

		<section class="encart_cartes">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<h4><?= get_field('encart_gauche_titre'); ?></h4>
						<?php $image = get_field('encart_gauche_image'); ?>
						<img src="<?php echo $image['sizes']['large']; ?>" alt="Image encart gauche">
						<a href="<?= get_permalink(102199); ?>" class="bouton_type_1">Accéder à la carte des groupes locaux</a>
						<div class="texte"><?= get_field('encart_gauche_texte'); ?></div>
					</div>
					<div class="col-lg-6">
						<h4><?= get_field('encart_dep_titre'); ?></h4>
						<?php $image = get_field('encart_dep_image'); ?>
						<img src="<?php echo $image['sizes']['large']; ?>" alt="Image encart dep">
						<a href="<?= get_permalink(102270); ?>" class="bouton_type_1">Accéder aux espaces départementaux</a>
						<div class="texte"><?= get_field('encart_dep_texte'); ?></div>
					</div>
				</div>
			</div>
		</section>

		<section class="resume_contenu">
			<div class="container">
				<?php if(have_rows('blocs_contenu')) : while(have_rows('blocs_contenu')) : the_row(); 
					$icone = get_sub_field('icone'); 
					$titre = get_sub_field('titre');
					$texte = get_sub_field('texte');
					$texte_bouton = get_sub_field('texte_bouton');
					$lien_bouton = get_sub_field('lien_bouton'); ?>
					<div class="bloc_resume">
						<div class="col_icone">
							<img src="<?= $icone; ?>" alt="Icone"/>
						</div>
						<div class="col_texte">
							<h3><?= $titre; ?></h3>
							<div class="texte"><?= $texte; ?></div>
							<a href="<?= $lien_bouton; ?>" class="bouton_type_1"><?= $texte_bouton; ?></a>
						</div>
					</div>
				<?php endwhile; endif; ?>				
			</div>
		</section>

	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




