<?php
/*
Template Name: TEMPORAIRE - Liens
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container">
		<main class="clearfix container">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>

			<section class="content_page">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content-page-notitle', 'content-page-notitle' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

				<div class="grabbed_page">
					<section class="data_content">



<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Alegreya|Josefin+Sans:700" rel="stylesheet">

<link href="/outils/css/embed.css" rel="stylesheet">


<div class="liens liste">

    <section class="toolbox_container">
    <div class="toolbox shadowed_box">
        <p>Cette rubrique est collaborative, participez à son élaboration :</p>
        <a href="/outils/liste-de-liens-utiles/proposer-lien.html" class="bouton_type_1">Proposer du contenu</a>
        <p>Inclure cette rubrique sur votre site web&nbsp;:</p>
        <textarea>&lt;iframe class="iframe_to_gjc" src="https://giletsjaunes-coordination.fr/outils/liste-de-liens-utiles/liens.html?embed"&gt;&lt;/iframe&gt;</textarea>
    </div>
</section>


    <section class="display_switcher_container">
        <div class="display_switcher shadowed_box">
            <div class="btn bouton_liste active">
                <i class="fas fa-th-list"></i>
            </div>
            <div class="btn bouton_thumbnail">
                <i class="fas fa-th-large"></i>
            </div>
        </div>
    </section>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_1" role="button" aria-expanded="true" aria-controls="reponse_1">
            <h4><i class="fas fa-arrow-right"></i> Sites internet Gilets Jaunes</h4>
        </a>

        <div id="reponse_1" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.gj-magazine.com/gj/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Gilets Jaunes Magazine</p>
                            <p class="description">Une revue de presse très vaste, avec beaucoup de commentaires, de tout ce qui est GJ</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.pagesgiletsjaunes.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Pages Gilets Jaunes</p>
                            <p class="description">Annuaire de pages Facebook de groupes et personnalités Gilets Jaunes. Agréable.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://gilets-jaunes-occitanie.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Gilets Jaunes Occitanie</p>
                            <p class="description">Site de coordination régionale pour l'occitanie</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://assembleesdesgiletsjaunes.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Assemblée de Commercy</p>
                            <p class="description">Chat, forum, wiki, prise de décision, publication</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.youtube.com/channel/UC03qCsERk3cQPmhdI0SI6xQ">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">YouTube Commercy</p>
                            <p class="description">Chaine YouTube des Gilets Jaunes de Commercy et de leur projet Assemblée des assemblées</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://www.giletsjaunes.sitew.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Gilets Jaunes 1 rond point</p>
                            <p class="description">Un site très rigolo avec des liens utiles accrochés sur l'image d'un rond point</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.plateformejaune.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Plateforme jaune</p>
                            <p class="description">programme révolutionnaire pour les gilets jaunes</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://lemurjaune.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Le Mur Jaune</p>
                            <p class="description">Galerie photo des blessés Gilets Jaunes. Impressionnant.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://lesgiletsjaunesdeforcalquier.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Les Gilets Jaunes de Forcalquier</p>
                            <p class="description">Des actualités régulières</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://giletsjaunes06.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Gilets Jaunes 06</p>
                            <p class="description"></p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://mouvement17novembre.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Mouvement du 17 novembre</p>
                            <p class="description">Quelques actualités, articles et revendications</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://26mai.vote-jaune.eu/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Le Vote Jaune</p>
                            <p class="description">Site proposant l'idée de voter avec des "bulletins jaunes" qui seraient comptabilisés par des volontaires</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://cagnottesgiletsjaunes.github.io/actions.html">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Cagnottes GJ</p>
                            <p class="description">Annuaire de cagnottes pour GJ</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://printemps-jaune.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Printemps Jaune</p>
                            <p class="description">Analyses, articles, points de vue, créé dans le Rhône</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://umap.openstreetmap.fr/fr/map/action-citoyens-et-gilet-jaune_327684#6/46.256/8.108">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Carte des groupes et événements Gilets Jaunes</p>
                            <p class="description">Une carte qui synthétise différentes sources d'informations dont la cartre des groupes de notre site.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://amnistiegj.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Amnistie GJ</p>
                            <p class="description">Pétition pour l'amnistie des GJ condamnés ; 80 000 signatures en mai 2019</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://www.citejaune.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Cité Jaune</p>
                            <p class="description">Sites des GJ de la ville d'Angers, beaucoup de contenu notamment en lien avec l'AdA</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_2" role="button" aria-expanded="true" aria-controls="reponse_2">
            <h4><i class="fas fa-arrow-right"></i> Plateformes participatives Gilets Jaunes (national)</h4>
        </a>

        <div id="reponse_2" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://revendicationsgiletsjaunes.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">revendicationsgiletsjaunes.fr</p>
                            <p class="description">Site pour voter des revendications dans une grande liste. 11500 votants au 22/03/2019</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://giletsjaunes.decidemos.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Decidemos Gilets Jaunes</p>
                            <p class="description">Propose plateforme participative aux groupes gilets jaune.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://jepolitique.fr/topics/71-gilet-jaune-liste-des-revendications">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">JePolitique.fr</p>
                            <p class="description">Espace de revendications Gilets Jaunes sur le site jepolitique.fr ; système de revendictions, arguments votes.</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_3" role="button" aria-expanded="true" aria-controls="reponse_3">
            <h4><i class="fas fa-arrow-right"></i> Plateformes participatives Gilets Jaunes (local)</h4>
        </a>

        <div id="reponse_3" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://evote.gilets-jaunes.online/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">eVoteGJ</p>
                            <p class="description">Site de vote sur 64 revendications, au jugement majoritaire. Concerne la région du Grand Pau</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://midi-pyrenees.plateforme-gilets-jaunes.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Outil participatif des Gilets Jaunes de Toulouse</p>
                            <p class="description">Plateforme de "Cap Collectif" adaptée aux GJ des environs de Toulouse</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://les-gilets-jaunes.re/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Outil participatif des Gilets Jaunes de la Réunion</p>
                            <p class="description">Plateforme "Cap Collectif"</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://paca.plateforme-gilets-jaunes.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Outil participatif des Gilets Jaunes de PACA</p>
                            <p class="description">Plateforme "Cap Collectif" - co-construction de revendications</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://bretagne.plateforme-gilets-jaunes.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Outil participatif des Gilets Jaunes de Bretagne</p>
                            <p class="description">Plateforme "Cap Collectif"</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_4" role="button" aria-expanded="true" aria-controls="reponse_4">
            <h4><i class="fas fa-arrow-right"></i> Applis</h4>
        </a>

        <div id="reponse_4" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=fr.devappfrance.gj_france">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Appli mobile GJ France</p>
                            <p class="description">Permet de recenser et compter les GJ. <br>
Fonctionnalité à venir : covoiturer avec d’autres GJ, héberger un gilet jaune ou demander à se faire héberger, solutions en cas d’urgence sur un événement, présenter un Cahier de Doléance GLOBAL avec système de vote</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_5" role="button" aria-expanded="true" aria-controls="reponse_5">
            <h4><i class="fas fa-arrow-right"></i> Media</h4>
        </a>

        <div id="reponse_5" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.giletsjaunes-coordination.fr/documents.html?categorie=19">
                            <img class="thumbnail" src="/outils/liste-de-liens-utiles/img-lien-vignette-1577562499-63.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Thinkerview</p>
                            <p class="description">Une chaîne youtube qui met à l’épreuve des points de vue peu médiatisés, afin d’élargir les prismes de lecture.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://aurismedia.fr/gazette.html">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Gazette le Mouton Libéré</p>
                            <p class="description">Gazette produite par Auris Media</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://laffreuxjojo.home.blog/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">L'affreux Jojo</p>
                            <p class="description">Journal d'expression de Gilets Jaunes du vignoble nantais</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://jaune.noblogs.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Jaune Le Journal</p>
                            <p class="description">Un seul numéro sorti pour le moment (20/01/19)</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://tvgj.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">TVGJ Télé Gilets Jaunes</p>
                            <p class="description">Une TV qui diffuse 24/24 des vidéos pertinentes, accompagnées d'un chat (le truc pour parler, pas l'animal)</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://blog.tempscritiques.net/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Blog de la revue Temps Critiques</p>
                            <p class="description">Regard intéressant sur les GJ</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://www.revolutionpermanente.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Révolution Permanente</p>
                            <p class="description">Parle souvent des Gilets Jaunes favorablement</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_6" role="button" aria-expanded="true" aria-controls="reponse_6">
            <h4><i class="fas fa-arrow-right"></i> RIC</h4>
        </a>

        <div id="reponse_6" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://operationarticle3.home.blog/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Opération Article 3</p>
                            <p class="description">Projet de porter le RIC devant chaque député de France</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://culture-ric.fr">
                            <img class="thumbnail" src="/outils/liste-de-liens-utiles/img-lien-vignette-1577479071-132.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Culture RIC</p>
                            <p class="description">Passerelle d’informations plurielles et sourcées autour du RIC. Rejoignez des ateliers, conférences, réunions et contribuez à la réalisation de la démocratie de demain.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://clic-ric.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Clic RIC</p>
                            <p class="description">Site riche d'informations sur le RIC, actualités, actions, vidéos, intégration aux municipales 2020</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.les-crises.fr/debat-proposition-d-un-referendum-d-initiative-citoyenne-par-olivier-berruyer/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">RIC sur les-crises.fr</p>
                            <p class="description">Proposition de RIC très construite après débat sur les-crises.fr</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.france-referendum.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">France Referendum</p>
                            <p class="description">Site d'organisation de référendums (de votes) en ligne ; semble jeune et prometteur (début avril)</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_7" role="button" aria-expanded="true" aria-controls="reponse_7">
            <h4><i class="fas fa-arrow-right"></i> Plateformes et outils</h4>
        </a>

        <div id="reponse_7" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.mediamanif.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Media Manif</p>
                            <p class="description">Carte collaborative et temps-réel des manifestations,</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://pollers.co/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Pollers</p>
                            <p class="description">Jeune plateforme indépendante de sondages ; ambition de faire un change.org mais avec des sondages plutôt que des pétitions</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://jugementmajoritaire.net">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Jugement Majoritaire</p>
                            <p class="description">Outil limpide de création de votation au jugement majoritaire ; gratuit et sans inscription.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.faisonsentendrenosvoix.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Faisons Entendre Nos Voix</p>
                            <p class="description">Plateforme de proposition de réformes, avec vote et discussion, à visée nationale et permanente</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://je-vote-mes-lois.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Je Vote Mes Lois</p>
                            <p class="description">Site pour proposer des réformes ; sélection aléatoire hebdomadaire de 6 propositions ; vote de ces propositions ; synthèse hebdo des résultats</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.demodyne.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Démodyne</p>
                            <p class="description">Outil de gouvernance citoyenne</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://laverna.cc/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Laverna</p>
                            <p class="description">Bloc note en ligne, open source et chiffré</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.humhub.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Humhub</p>
                            <p class="description">Plateforme collaborative (fil d'actu, partage de fichier, wiki, .etherpad, ...) pour partager des information et pour travailler en groupe.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://bescherelle.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Conjugueur</p>
                            <p class="description">Bescherelle en ligne</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://doodle.com/fr/planning-en-ligne">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Doodle planning</p>
                            <p class="description">Votre agenda en ligne</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://online-voice-recorder.com/fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Online voice recorder</p>
                            <p class="description">Enregistrer des messages vocaux</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://appear.in/user/login">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Appear</p>
                            <p class="description">Video conférence</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framasoft.org/fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Framasoft</p>
                            <p class="description">Collectif qui publie, classe, trie des logiciels libres. Grosse base de donnée. A consulter : https://degooglisons-internet.org/fr/alternatives</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framalistes.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Framalistes</p>
                            <p class="description">Outil libre de création de mailing lists</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framatalk.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Framatalk</p>
                            <p class="description">Permet la conversation vocale/cam par navigateur à plusieurs. Instance libre de Jitsi Meet, logiciel libre</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framavox.org">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Framavox</p>
                            <p class="description">Outil libre de discussion / proposition / décision en groupe</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framapad.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Framapad</p>
                            <p class="description">Outil libre de bloc-notes collaboratif en temps réel</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://etherpad.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Etherpad</p>
                            <p class="description">Outil libre de bloc-notes collaboratif en temps réel.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://cryptpad.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Cryptpad</p>
                            <p class="description">Editeur de texte, éditeur de code et éditeur de présentation. Permet également le travail collaboratif. Chiffré et en ligne.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.solucracy.com/homepage.php">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Solucracy</p>
                            <p class="description">Plateforme de recherche collaborative de solutions</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.loomio.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Loomio</p>
                            <p class="description">Outil d'aide à la prise de décision pour les groupes</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.tracim.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Tracim</p>
                            <p class="description">Plateforme libre d'échange et travail en équipe</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.discourse.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Discourse</p>
                            <p class="description">Logiciel de forum open source très évolué</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://protonmail.com/fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Protonmail</p>
                            <p class="description">Messagerie sécurisée</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://outilscollaboratifs.com/2018/12/agorakit-outil-collaboratif-pour-collectifs-citoyens/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Outils collaboratifs</p>
                            <p class="description">Site qui présente divers outils de travail collaboratif. Exemple : https://www.agorakit.org/fr/</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.openstreetmap.org">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Openstreetmap</p>
                            <p class="description">Alternative libre à googlemap</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://tails.boum.org/index.fr.html">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Tails</p>
                            <p class="description">Système d'exploitation live portatif, à but de préservation de la confidentialité</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://liberapay.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Liberapay</p>
                            <p class="description">Alternative à Tipeee. Financement participatif. Code source public</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://zeit.co/docs">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Zeit</p>
                            <p class="description">Solution en ligne d'hébergement instantané de site et service web, sans serveur</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://addons.mozilla.org/fr/firefox/addon/ublock-origin/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">uBlock Origin</p>
                            <p class="description">Extension de navigateur pour bloquer les publicités et mouchards</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://democracy.foundation/similar-projects/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Democracy Foundations : projets Civic Tech</p>
                            <p class="description">Une liste très complète et bien maintenue de projets civic tech internationaux</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_8" role="button" aria-expanded="true" aria-controls="reponse_8">
            <h4><i class="fas fa-arrow-right"></i> Creation graphique</h4>
        </a>

        <div id="reponse_8" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.canva.com/fr_fr">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Canva</p>
                            <p class="description">Création de visuel, retouche photo, via navigateur.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.photopea.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Photopea</p>
                            <p class="description">Création d'image, retouche photo avancé, via navigateur.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.kizoa.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Kizoa</p>
                            <p class="description">Création vidéo en ligne. Nécessite Adobe Flash Player.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.autodraw.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Autodraw</p>
                            <p class="description">Dessiner schémas et croquis. Outil simple et basic, en ligne</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://www.enfants.rmngp.fr/bd/hopper/ecran-bd">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Enfants rmngp</p>
                            <p class="description">Créer une bd en ligne</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://nuagedemots.co/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Nuage de mots</p>
                            <p class="description">Créer votre nuage de mot, en ligne</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://emojipedia.org/twitter/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Emoji</p>
                            <p class="description">Liste énorme de "emoji"</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://www.unicode.org/emoji/charts/full-emoji-list.html">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Unicode emoij</p>
                            <p class="description">Grande liste d'emoji</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://fr.pngtree.com/free-png">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Pngtree</p>
                            <p class="description">Banque d'image libre</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framindmap.org">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Framindmap</p>
                            <p class="description">Outil libre de création de carte heuristique</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.mindmup.com/#storage">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Mindmup</p>
                            <p class="description">Création de carte heuristique</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://pablo.buffer.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Pablo buffer</p>
                            <p class="description">Création de visuel pour réseaux sociaux. (type avatar et autres)</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_9" role="button" aria-expanded="true" aria-controls="reponse_9">
            <h4><i class="fas fa-arrow-right"></i> Réseaux sociaux</h4>
        </a>

        <div id="reponse_9" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://nexxociety.co/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Nexxo</p>
                            <p class="description">Réseau social indépendant, open source, sans pub, avec fonctionnalités de contacts locaux</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://le-desobeissant.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Le Désobéissant</p>
                            <p class="description">Réseau social open source basé sur OSSN</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://riot.im/app/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Matrix/Rio</p>
                            <p class="description">Chat en ligne pour remplacer Facebook et Discord. Communication décentralisée et chiffrée, protocole libre</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://joinmastodon.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Mastodon</p>
                            <p class="description">Pour remplacer twitter. Hébergé par Framasoft :  https://framapiaf.org/  ; hébergé par des GJ : https://reseaujaune.com</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://dino.im/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Dino</p>
                            <p class="description">Petit Chat open source chiffré et décentralisé</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://signal.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Signal</p>
                            <p class="description">Appel téléphonique chiffré, application libre, téléphone ou ordinateur</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.facebook.com/Gilets-Jaunes-Coordination-Nationale-des-R%C3%A9gions-2292047541075201/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Page Facebook Gilets Jaunes Portail Collaboratif</p>
                            <p class="description">(ex  Coordination Nationale des Régions) publications / infos...</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://twitter.com/GiletsN">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Twitter Gilets Jaunes Rond-point Numérique</p>
                            <p class="description">La boite à outil au Service des Gilets Jaunes</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.youtube.com/channel/UC70Lug8ipeFfgTFcfwrhKAw">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">YouTube Gilets Jaunes Portail Collaboratif</p>
                            <p class="description"></p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.facebook.com/LaGazetteGJ/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Page Facebook Gazette des GJ</p>
                            <p class="description"></p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framateam.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Framateam</p>
                            <p class="description">Instance de MatterMost, open source, alternative Discord</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framasphere.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Framasphère</p>
                            <p class="description">Réseau social Diaspora, open source et ethique, alternative Facebook</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://framalibre.org/tags/chat">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Chat framalibre</p>
                            <p class="description">Sélection de quelques un des meilleurs outils de Chat libre</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="/discord.html">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Discord Gilets Jaunes Portail Collaboratif</p>
                            <p class="description">Un des plus gros serveurs Discord Gilets Jaunes</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_10" role="button" aria-expanded="true" aria-controls="reponse_10">
            <h4><i class="fas fa-arrow-right"></i> Partage vidéo ou fichiers lourds</h4>
        </a>

        <div id="reponse_10" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.bitchute.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Bitchute</p>
                            <p class="description">Plateforme de vidéos décentralisées. Très efficace, bonne alternative Youtube</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://joinpeertube.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">PeerTube</p>
                            <p class="description">Plateforme de vidéos décentralisées sur logiciel libre. 1GB disponible sinon nécessite auto-hébergement</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://www.trilulilu.ro/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Trilulilu</p>
                            <p class="description">Equivalent Youtube roumain</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.aparat.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Aparat</p>
                            <p class="description">Equivalent Youtube iranien</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://disk.yandex.ru/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Yandesk disk</p>
                            <p class="description">Alternative à Google drive, solution russe. 50G gratuit + microsoft office</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_11" role="button" aria-expanded="true" aria-controls="reponse_11">
            <h4><i class="fas fa-arrow-right"></i> Débat</h4>
        </a>

        <div id="reponse_11" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://le-vrai-debat.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Le Vrai Débat</p>
                            <p class="description">Site alternatif au grand débat national, plateforme Cap Collectif soutenue par une équipe de Gilets Jaunes historiques, très prometteur</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.le-grand-debat.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Le (vrai) Grand Débat</p>
                            <p class="description">Site de vote sur une liste de propositions ; vote par note de 0 à 10</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://www.politizr.com/groupes/grand-debat-la-suite">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Politizr</p>
                            <p class="description">Plateforme indépendante de débat en ligne, résultats open data de son propre débat</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://gda-citoyens.fr/index.php">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Débat Autonome Citoyens</p>
                            <p class="description">Plateforme naissante visant à organiser un ou des débats autonomes, citoyens, permanents, en local ou en national</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://blog.parlement-ouvert.fr/cartographie-initiatives-grand-debat/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Initiatives Parallèles au Grand Débat</p>
                            <p class="description">Une liste étoffée de débats ouverts</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://grandeannotation.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">La Grande Annotation</p>
                            <p class="description">Outil proposant une annotation collaborative des réponses au Grand Débat gouvernemental. Intelligent et open source.</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://granddebat.codefor.fr/QCM/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Analyse des QCM du Grand Débat</p>
                            <p class="description">Bien fait, analyse départementale des résultats</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_12" role="button" aria-expanded="true" aria-controls="reponse_12">
            <h4><i class="fas fa-arrow-right"></i> Démocratie</h4>
        </a>

        <div id="reponse_12" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://democratieouverte.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Démocratie Ouverte</p>
                            <p class="description">Collectif d'innovation démocratique, beaucoup de projets</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://citoyens.info/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Citoyens.info</p>
                            <p class="description">Annuaire de mouvements citoyens et démocratiques ; modeste et propre</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://www.openmairie.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Open Mairie</p>
                            <p class="description">Catalogue d'application open source pour les mairies</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://publik.entrouvert.com/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Publik</p>
                            <p class="description">Plate-forme libre destinée aux interactions citoyens / administrations</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://36000communes.org/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">36 000 Communes</p>
                            <p class="description">Plein de ressources sur la démocratie communale</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://labelledemocratie.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">La Belle Démocratie</p>
                            <p class="description">"Label" de listes participatives aux élections municipales</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://listesparticipatives2020.frama.wiki/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Wiki des listes participatives aux municipales 2020</p>
                            <p class="description"></p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://integritywatch.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Integrity Watch</p>
                            <p class="description">Site bien fait permettant de consulter tous les intérêts et activités déclarés de tous les parlementaires</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
        <a class="titre_cat collapsed" data-toggle="collapse" href="#reponse_13" role="button" aria-expanded="true" aria-controls="reponse_13">
            <h4><i class="fas fa-arrow-right"></i> Monnaie</h4>
        </a>

        <div id="reponse_13" class="contenu_cat collapse">
                        <ul class="row">
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://monnaie-libre.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Monnaie Libre (Ğ1)</p>
                            <p class="description">Explications sur Ğ1, la monnaie libre</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="https://jura.monnaie-libre.fr/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Monnaie Libre (Ğ1) : Jura</p>
                            <p class="description">Promotion de la monnaie libre dans le Jura</p>
                        </a>
                    </li>
                                    <li class="col-lg-3 col-md-4 col-sm-6">
                        <a target="_blank" href="http://www.creationmonetaire.info/">
                            <img class="thumbnail" src="/outils/img/front/default/defaut.jpg?w=250&amp;h=250&amp;isMaxSize">
                            <p class="libelle">Création Monétaire (Ğ1 - Stéphane Laborde)</p>
                            <p class="description">Site de la Théorie Relative de la Monnaie, une étude passionnante et pertinente de ce que serait ou est une monnaie équitable</p>
                        </a>
                    </li>
                            </ul>
        </div>

    
</div>
<script type="text/javascript">
$(function() {
    $('div.display_switcher div.btn').on('click', function() {
        $('div.display_switcher div.btn').removeClass('active');
        $(this).addClass('active');
        if ($(this).hasClass('bouton_liste')) {
            $('div.liens').addClass('liste');
        } else {
            $('div.liens').removeClass('liste');
        }
    });
});
</script>



<script type="text/javascript">
$(function () {
    $("input[type=button].core_upload_file_trigger, button.core_upload_file_trigger").click(function () {
        var trigger = $(this);
        var param = new Object();
        param.ajax = "true";
        param.fileType = trigger.attr("fileType");
        param.callBackFieldName = trigger.attr("callBackFieldName");
        if (trigger.attr("autorizedMimeType") != undefined)
            param.autorizedMimeType = trigger.attr("autorizedMimeType").split(";");

        switch (trigger.attr("fileType")) {
            case "image":
                if (trigger.attr("fileExtension") != undefined)
                    param.fileExtension = trigger.attr("fileExtension");
                if (trigger.attr("exceptedwidth") != undefined)
                    param.width = trigger.attr("exceptedwidth");
                if (trigger.attr("exceptedheight") != undefined)
                    param.height = trigger.attr("exceptedheight");
                if (trigger.attr("isMaxSize") != undefined)
                    param.isMaxSize = true;
                if (trigger.attr("bandeColor") != undefined)
                    param.bandeColor = trigger.attr("bandeColor");
                break;
        }

        $("body").append('<div class="core_upload_file_popup" style="display:none;"></div>');
        var CoreUploadFilePopup = "div.core_upload_file_popup";
        param.core_UploadFile_Popup = CoreUploadFilePopup;

        $.get("/core_upload.html", param, function (data) {
            $(CoreUploadFilePopup).html(data);
            $(CoreUploadFilePopup).dialog({ autoOpen: false, closeOnEscape: true, height: 600, width: 950, modal: true, resizable: false, draggable: false, title: "Fenêtre d'envoi de fichier", close: function () {
                $(CoreUploadFilePopup).remove();
            } });
            $(CoreUploadFilePopup).dialog("open");
            $(CoreUploadFilePopup).find("iframe").each(function () {
                $(this).attr('src', $(this).attr('src2'));
            });
        });
    });

    // Bouton étape 2
    $("body").off('click', "input[type=button].core_upload_file_trigger_etape2, button.core_upload_file_trigger_etape2").on('click', "input[type=button].core_upload_file_trigger_etape2, button.core_upload_file_trigger_etape2", function () {
        var trigger = $(this);
        var param = new Object();
        param.ajax = "true";
        param.fileType = trigger.attr("fileType");
        //param.callBackFieldName  = trigger.attr("callBackFieldName");
        if (trigger.attr("autorizedMimeType") != undefined)
            param.autorizedMimeType = trigger.attr("autorizedMimeType").split(";");

        switch (trigger.attr("fileType")) {
            case "image":
                if (trigger.attr("fileExtension") != undefined)
                    param.fileExtension = trigger.attr("fileExtension");
                if (trigger.attr("exceptedwidth") != undefined)
                    param.width = trigger.attr("exceptedwidth");
                if (trigger.attr("exceptedheight") != undefined)
                    param.height = trigger.attr("exceptedheight");
                if (trigger.attr("isMaxSize") != undefined)
                    param.isMaxSize = true;
                if (trigger.attr("bandeColor") != undefined)
                    param.bandeColor = trigger.attr("bandeColor");
                break;
        }

        $("body").append('<div class="core_upload_file_popup" style="display:none;"></div>');

        var $champChargementFichier = $(".champChargementFichier[champ=" + trigger.attr("champ") + "]");
        var $fichierPourCoreUpload = $("input.fichierPourCoreUpload[champ=" + trigger.attr("champ") + "]");

        if ($(".fichierAEnvoyer[champ=" + trigger.attr("champ") + "]").length == 0) {
            var codeHTMLChampFichier = '<input type="hidden" champ="' + trigger.attr("champ") + '" class="fichierAEnvoyer" name="champFichierJSONs[' + $champChargementFichier.attr("idDisplay") + '][' + $champChargementFichier.attr("nom") + ']" value=""/>';
            $champChargementFichier.after(codeHTMLChampFichier);
        }

        var CoreUploadFilePopup = "div.core_upload_file_popup";
        param.core_UploadFile_Popup = CoreUploadFilePopup;

        param["raw"] = "";
        param[$fichierPourCoreUpload.attr("name")] = $fichierPourCoreUpload.attr("value");
        param["act"] = "add";
        param["etape2Directe"] = "";
        if ($fichierPourCoreUpload.attr("callBackFieldName") != undefined)
            param["callBackFieldName"] = $fichierPourCoreUpload.attr("callBackFieldName");
        else
            param["callBackFieldName"] = $(".fichierAEnvoyer").attr("name");
        param["mod"] = "mono";
        param["fileType"] = trigger.attr("fileType");
        param["fileExtension"] = trigger.attr("fileExtension");
        param["bandecolor"] = 0;
        param["width"] = trigger.attr("exceptedwidth");
        param["height"] = trigger.attr("exceptedheight");
        param["noDelete"] = false;
        param["nomChamp"] = trigger.attr("champ");
        param['raw'] = true;

        if ($fichierPourCoreUpload.attr("value") != "" && $fichierPourCoreUpload.attr("value") != undefined) {
            //+"champFichierJSONs["+champ.attr("idDisplay")+"]["+champ.attr("nom")+"]="+response.fichierJSON64
            $(this).ajaxWebApp({type: "POST", url: "/core_upload.html?ajax", async: true, data: param, success: function (received) {
                $(CoreUploadFilePopup).html(received);
                $(CoreUploadFilePopup).dialog({ autoOpen: false, closeOnEscape: true, height: 600, width: 950, modal: true, resizable: false, draggable: false, title: "Fenêtre d'envoi de fichier", close: function () {
                    $(CoreUploadFilePopup).remove();
                } });
                $(CoreUploadFilePopup).dialog("open");
            }});
        }
    });

    $(document).on('afterLoadingComplete', function() {
        $('div.champChargementFichier').each(function () {
            gererChampChargementFichier($(this));
        });
    });
});
</script>


                        </section>
				</div>

			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




