<?php
/*
Template Name: FAGJ - Sous-page FAGJ
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page_fagj souspage_fagj">

		<?php include('templatechunk-fagj-nav.php'); ?>

		<main class="clearfix">

			<section class="entete_page">
					<h1 class="entry-title"><?= get_field('titre_ada'); ?></h1>
			</section>

			<div class="sidebar_zone row has_pinned_info">

				<div class="main_content col-lg-8">

					<!-- Module Description FAGJ -->
					<?php if(get_field('section_ouverte_description')[0] == 'oui') { $show = 'show'; $collapsed = ''; } else { $show = ''; $collapsed = 'collapsed'; } ?>
					<div class="module_bloc shadowed_box" style="order: <?= get_field('ordre_module_description') ?>">
						<a data-toggle="collapse" href="#description" class="module_titre <?=$collapsed?>"><div>Le FAGJ dans le détail</div><i class="fas fa-chevron-up"></i></a>
						<div id="description" class="module_content_container collapse <?=$show?>">
							<div class="module_content">
								<?= get_field('texte_principal') ?>
							</div>
						</div>
					</div>

					<!-- Module Galeries -->
					<?php if(have_rows('galeries')) : ?>
						<?php if(get_field('section_ouverte_galerie')[0] == 'oui') { $show = 'show'; $collapsed = ''; } else { $show = ''; $collapsed = 'collapsed'; } ?>
						<div class="module_bloc shadowed_box" style="order: <?= get_field('ordre_module_galerie') ?>">
							<a data-toggle="collapse" href="#galerie" class="module_titre <?=$collapsed?>"><div>Images et fichiers</div><i class="fas fa-chevron-up"></i></a>
							<div id="galerie" class="module_content_container collapse <?=$show?>">
								<div class="module_content">

									<?php $nb_gal = 1;
									while(have_rows('galeries')) : the_row(); ?>

										<div class="galerie_bloc">
											<h3 class="titre_galerie"><?= get_sub_field('nom_galerie') ?></h3>
											<div class="row">
												<?php $images = get_sub_field('galerie');
												if(isset($images)) {
													$i = 0;
													foreach($images as $img) {  
														if(in_array($img['mime_type'], ['image/jpeg', 'image/gif', 'image/png', 'image/svg+xml', 'image/webp'])) {
														?>
															<a href="#" class="goto-carousel-slide img_container col-lg-3" data-toggle="modal" data-target="#modal_galerie_<?=$nb_gal?>" data-slide-to="<?=$i ?>" goto-carousel-slide="<?=$i ?>">
																<img src="<?= $img['sizes']['medium'] ?>" title="<?= $img['title'] ?>" alt="<?= $img['alt'] ?>">
															</a>
														<?php } else { ?>
															<a href="<?= $img['url'] ?>" target="_blank" class="file_container col-lg-3">
																<div class="icon_container">
																	<i class="far fa-file-alt"></i>
																</div>
																<div class="nom text-center"><?= $img['title'] ?></div>
															</a>
														<?php } ?>
													<?php $i++; }
												} ?>
											</div>
										</div>

										<!-- MODALE GALERIE -->
										<div class="modal bare carousel_modal fade modale_galerie" id="modal_galerie_<?=$nb_gal?>" tabindex="-1" role="dialog" aria-hidden="true">
											<div class="modal-dialog centered" role="document">
												<div class="modal-content">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													<div class="modal-body">

														<?php // ------------- CAROUSEL SETTINGS -------------
														$carousel_id = 'carousel_gallerie_modal'.$nb_gal;
														$carousel_autoplay = 'false'; // true = autoplay après premiere action, carousel = autoplay au chargement
														$carousel_interval = 5; // (secondes)
														$carousel_pause = 'false'; // false = jamais de pause, hover = pause au hover sur le carousel
														$carousel_left_icon = '<i class="fas fa-angle-left"></i>';
														$carousel_right_icon = '<i class="fas fa-angle-right"></i>';
														// ------------------- CAROUSEL SETTINGS ------------- ?>

														<div id="<?= $carousel_id; ?>" class="carousel carousel_galerie in_modal slide" data-ride="<?= $carousel_autoplay; ?>" data-interval="<?= $carousel_interval * 1000; ?>" data-pause="<?= $carousel_pause; ?>">

															<div class="carousel-inner" role="listbox">
																<?php
																$i = 0;
																$images = get_sub_field('galerie');
																if(isset($images)) {
																	foreach($images as $img) {
																		if(in_array($img['mime_type'], ['image/jpeg', 'image/gif', 'image/png', 'image/svg+xml', 'image/webp'])) { ?>
																			<div class="carousel-item<?php if($i == 0) { echo ' active'; } ?>">
																				<img src="<?= $img['sizes']['fullhd-nocrop'] ?>" title="<?= $img['title'] ?>" alt="<?= $img['alt'] ?>">
																			</div>
																		<?php } ?>
																	<?php $i++; 
																	}
																} $number_of_slides = $i; ?>
															</div>
															<!-- Contrôles gauche/droite -->
															<a class="carousel-control carousel-control-prev" href="#<?= $carousel_id; ?>" role="button" data-slide="prev">
																<?= $carousel_left_icon; ?>
																<span class="sr-only">Précédent</span>
															</a>
															<a class="carousel-control carousel-control-next" href="#<?= $carousel_id; ?>" role="button" data-slide="next">
																<?= $carousel_right_icon; ?>
																<span class="sr-only">Suivant</span>
															</a>
															<!-- Contrôles puces -->
															<ol class="carousel-indicators">
																<?php for ($i = 0; $i <= $number_of_slides-1; $i++) { 
																	if ($i == 0) { $active = ' class="active"'; } else { $active = ''; }
																	echo '<li data-target="#'.$carousel_id.'" data-slide-to="'.$i.'"'.$active.'><div class="carousel-indicator-inside"></div></li>';
																} ?>
															</ol>
														</div>

													</div>
												</div>
											</div>
										</div>
										<!-- FIN MODALE SHOWCASE -->

									<?php $nb_gal++; endwhile; ?>

								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>

				<div class="sidebar_container col-lg-4">
					<div class="sidebar">
						<h3 class="titre">
							<div class="triangle"></div>
							En bref
						</h3>
						<div class="sidebar_content">
							<!-- Widget contact -->
							<div class="widget contacts_medias liste_fichiers">
								<h4 class="sidebar_title">CONTACTS / MÉDIAS</h4>
								<?php $txt_intro = get_field('texte_intro_contacts');
								if($txt_intro != '') { ?>
									<div class="no_entry mb-3">
										<?= $txt_intro ?>
									</div>
								<?php } ?>

								<?php if(have_rows('liste_contacts')) : 
									while(have_rows('liste_contacts')) : the_row(); 
										$type = get_sub_field('type');
										$titre = get_sub_field('titre');
										$lien = get_sub_field('lien');

										switch ($type) {
											case 'facebook': $icon = '<i class="fab fa-facebook-square"></i>'; break;
											case 'youtube': $icon = '<i class="fab fa-youtube-square"></i>'; break;
											case 'vimeo': $icon = '<i class="fab fa-vimeo-square"></i>'; break;
											case 'twitch': $icon = '<i class="fab fa-twitch"></i>'; break;
											case 'email': $icon = '<i class="fas fa-envelope-square"></i>'; break;
											case 'site_web': $icon = '<i class="fas fa-window-maximize"></i>'; break;
											default: $icon = '<i class="fas fa-arrow-alt-circle-right"></i>'; break;
										}
										?>
										<a class="ligne" href="<?= $lien ?>" target="_blank">
											<div class="icon_container">
												<?= $icon ?>
											</div>
											<div class="label">
												<p class="titre"><?= $titre ?></p>
											</div>
										</a>
									<?php endwhile; ?>
								<?php else: ?>
									<div class="no_entry">
										<p>Aucuns contacts ou médias n'ont été renseignés pour cette FAGJ...</p>
										<p>Si vous avez des informations, n'hésitez pas à nous les transmettre !</p>
									</div>
								<?php endif; ?>
							</div>
							<!-- Widget synthèses -->
							<div class="widget syntheses liste_fichiers">
								<h4 class="sidebar_title">DOCUMENTS / SYNTHÈSES</h4>
								<?php $txt_intro = get_field('texte_intro_syntheses');
								if($txt_intro != '') { ?>
									<div class="no_entry mb-3">
										<?= $txt_intro ?>
									</div>
								<?php } ?>

								<?php if(have_rows('liste_syntheses')) : 
									while(have_rows('liste_syntheses')) : the_row(); 
										$titre = get_sub_field('titre');
										$soustitre = get_sub_field('soustitre');
										$fichier = get_sub_field('fichier');
										?>
										<a class="ligne" href="<?= $fichier ?>" target="_blank" rel="noopener nofollow noreferrer">
											<div class="icon_container">
												<i class="far fa-file-alt"></i>
											</div>
											<div class="label">
												<p class="titre"><?= $titre ?></p>
												<p class="soustitre"><?= $soustitre ?></p>
											</div>
										</a>
									<?php endwhile; ?>
								<?php else: ?>
									<div class="no_entry">
										<p>Aucuns documents ou synthèses n'ont été renseignés pour cette FAGJ...</p>
										<p>Si vous avez des informations, n'hésitez pas à nous les transmettre !</p>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>

			</div>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




