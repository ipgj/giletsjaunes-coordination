<?php
/*
Template Name: Base Template Sidebar
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container">
		<main class="clearfix">

			<section class="entete_page">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content-page', 'content-page' );

				endwhile; // End of the loop.
				?>

			</section>

			<div class="sidebar_zone row no-gutters<?= $sidebar_zone_class ?>">

				<div class="main_content col-lg-9">


				</div>

				<div class="sidebar_container col-lg-3">


				</div>

			</div>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




