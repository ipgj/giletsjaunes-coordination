<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Gilet_Jaune_France
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				
				<h1 class="message_404">404</h1>
				<h3>La démocratie que nous recherchons est intr...<br/>
				EUUUH... je veux dire...<br/>
				La page que vous recherchez est introuvable !</h3>

				<a href="<?= home_url(); ?>" class="bouton_type_1">Retour à l'accueil du site</a>

				<p>Vous pouvez également nous contacter via le forum d'aide si vous avez besoin d'éclaircissements.</p>
				<a href="<?= get_permalink(); ?>" class="bouton_type_1">Forum d'aide</a>

			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
