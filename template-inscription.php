<?php
/*
Template Name: Page d'inscription
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */


get_header(); ?>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<div class="main_content_container">
		<main class="clearfix">

			<section class="entete_page">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content-page', 'content-page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

			</section>


			<section class="form_container formulaire_connexion">
				<h2 class="titre_form">Connexion</h2>
				<div class="text-right mt-3 mb-1"><a href="#inscription" class="js-scrollTo bouton_type_1 btn_slide_inscription">Pas encore de compte ? Inscrivez-vous ! <i class="fas fa-caret-down"></i></a></div>
				<div class="error">
					<?php if($_GET['login'] == 'failed') {
						if($_GET['reason'] == 'both_empty') { echo 'Veuillez entrer un identifiant et un mot de passe.'; }
						elseif($_GET['reason'] == 'empty_username') { echo 'Veuillez entrer un identifiant.'; }
						elseif($_GET['reason'] == 'empty_password') { echo 'Veuillez entrer un mot de passe.'; }
						elseif($_GET['reason'] == 'invalid_username') { echo 'Identifiant ou mot de passe incorrect.'; }
						else { echo 'Erreur de connexion.'; }
					} ?>
				</div>
				<?php 
				$args = [
					'redirect' => $_GET['return_url']
				];
				wp_login_form($args); ?>

				<a class="lien_pass_oublie" data-toggle="collapse" href="#form_passwordreset_container" role="button" aria-expanded="false" aria-controls="form_passwordreset_container">
					Vous avez oublié votre mot de passe ?
				</a>
				<div class="collapse" id="form_passwordreset_container">
					<div class="passwordreset_separator"></div>
					<?php echo do_shortcode('[bbp-lost-pass]') ?>
				</div>
			</section>


			<section id="inscription" class="form_container formulaire_inscription">

				<h2 class="titre_form">Inscription</h2>

				<div class="error">
					<?php 
					if($_GET['passwordcheck'] == 'error') { echo '<p>Les 2 mots de passe que vous avez tapé ne correspondent pas !</p>'; }
					if($_GET['cgu_error'] == true) { echo '<p>Vous devez accepter les Conditions générales d\'utilisation pour créer un compte sur le site.</p>'; }
					if($_GET['empty_username'] == 1) { echo '<p>Vous devez choisir un nom d\'utilisateur.</p>'; }
					if($_GET['empty_email'] == 1) { echo '<p>Vous devez choisir une adresse e-mail.</p>'; }
					if($_GET['invalid_email'] == 1) { echo '<p>Adresse e-mail non valide !</p>'; }
					if($_GET['username_exists'] == 1) { echo '<p>Cet identifiant est déjà utilisé.</p>'; }
					if($_GET['email_exists'] == 1) { echo '<p>Cette adresse e-mail est déjà utilisé.</p>'; }
					if($_GET['grecaptcha_error'] == 'true') { echo '<p>Notre système vous a considéré comme un bot.</p>'; }
					?>
				</div>

				<?php $departements = array( 'Aucun','01 - Ain','02 - Aisne','03 - Allier','04 - Alpes-de-Haute-Provence','05 - Hautes-alpes','06 - Alpes-maritimes','07 - Ardèche','08 - Ardennes','09 - Ariège','10 - Aube','11 - Aude','12 - Aveyron','13 - Bouches-du-Rhône','14 - Calvados','15 - Cantal','16 - Charente','17 - Charente-maritime','18 - Cher','19 - Corrèze','2a - Corse-du-sud','2b - Haute-Corse','21 - Côte-d\'Or','22 - Côtes-d\'Armor','23 - Creuse','24 - Dordogne','25 - Doubs','26 - Drôme','27 - Eure','28 - Eure-et-loir','29 - Finistère','30 - Gard','31 - Haute-garonne','32 - Gers','33 - Gironde','34 - Hérault','35 - Ille-et-vilaine','36 - Indre','37 - Indre-et-loire','38 - Isère','39 - Jura','40 - Landes','41 - Loir-et-cher','42 - Loire','43 - Haute-loire','44 - Loire-atlantique','45 - Loiret','46 - Lot','47 - Lot-et-garonne','48 - Lozère','49 - Maine-et-loire','50 - Manche','51 - Marne','52 - Haute-marne','53 - Mayenne','54 - Meurthe-et-moselle','55 - Meuse','56 - Morbihan','57 - Moselle','58 - Nièvre','59 - Nord','60 - Oise','61 - Orne','62 - Pas-de-calais','63 - Puy-de-dôme','64 - Pyrénées-atlantiques','65 - Hautes-Pyrénées','66 - Pyrénées-orientales','67 - Bas-rhin','68 - Haut-rhin','69 - Rhône','70 - Haute-saône','71 - Saône-et-loire','72 - Sarthe','73 - Savoie','74 - Haute-savoie','75 - Paris','76 - Seine-maritime','77 - Seine-et-marne','78 - Yvelines','79 - Deux-sèvres','80 - Somme','81 - Tarn','82 - Tarn-et-garonne','83 - Var','84 - Vaucluse','85 - Vendée','86 - Vienne','87 - Haute-vienne','88 - Vosges','89 - Yonne','90 - Territoire de belfort','91 - Essonne','92 - Hauts-de-seine','93 - Seine-Saint-Denis','94 - Val-de-marne','95 - Val-d\'oise','971 - Guadeloupe','972 - Martinique','973 - Guyane','974 - La réunion','976 - Mayotte'); ?>

				<form id="registerform" name="registerform" action="<?php bloginfo('url'); ?>/wp-login.php?action=register" method="post">
				    <fieldset>
				        <label>Identifiant / Pseudonyme</label>
				        <input type="text" name="user_login" value="" required/>
				        <label>Adresse e-mail</label>
				        <input type="email" name="user_email" value="" required/>
				        <label>Mot de passe</label>
			   			<input type="password" name="user_password" value="" id="user_password" class="input" required/>
			   			<label>Confirmation du mot de passe</label>
			   			<input type="password" name="user_password2" value="" id="user_password2" class="input" required/>
			   			<label>Votre département</label>
			   			<select id="select-departement" name="departement" required><?php
							foreach ( $departements as $departement ) {
								printf( '<option value="%1$s">%1$s</option>', $departement );
							}
			   			?></select>
			   			<div class="cgu_check_container">
			   				<label><input name="accept_cgu" type="checkbox" id="accept_cgu" value="forever"> J'ai lu et j'accepte les <a class="cgulink" href="<?= get_permalink(628); ?>" target="_blank">Conditions générales d'utilisation</a></label>
			   			</div>
				        <input type="hidden" name="redirect_to" value="<?php echo get_permalink('265'); ?>" />
				        <button name="wp-submit" type="submit" class="not-g-recaptcha bouton_type_1" data-sitekey="6LcZZZAUAAAAAOjptb_yegk4ndoMe6V8WfRdROOz" data-callback="onSubmit">Envoyer</button>
				        <!-- <input type="submit" name="wp-submit" class="bouton_type_1" /> -->
				    </fieldset>
				</form>

				<script>
					function onSubmit(token) {
console.log(token);
						document.getElementById("registerform").submit();
					}
				</script>

				<?php 
				//echo 'MAIN ROLE : -'.get_current_user_main_role().'-';
				?>

			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




