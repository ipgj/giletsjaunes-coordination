<?php
/*
Template Name: AdA - Outils
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container">

		<?php include('templatechunk-ada-nav.php'); ?>

		<main class="clearfix container">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>

			<section class="content_page">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content-page-notitle', 'content-page-notitle' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>

				<?php
				if(have_rows('liste_outils_ada')) { ?>	

					<div class="indexblocs_container outils_ada row">

						<?php while(have_rows('liste_outils_ada')) : the_row(); 
							$img = get_sub_field('image');
							$titre = get_sub_field('titre');
							$texte = get_sub_field('description');
							$txt_bouton = get_sub_field('texte_bouton');
							$lien = get_sub_field('lien_bouton');
							?>

							<div class="col-lg-4">

								<div class="bloc_container shadowed_box">
									<div class="logo_container">
										<img class="logo" src="<?= $img['sizes']['medium'] ?>" title="<?= $img['title'] ?>" alt="<?= $img['alt'] ?>" />
									</div>
									<a href="<?= $lien; ?>"><h4 class="titre"><?= $titre ?></h4></a>
									<div class="description">
										<?= $texte ?>
									</div>
									<div class="text-center">
										<a href="<?= $lien; ?>" class="bouton bouton_type_1"><?= $txt_bouton; ?></a>
									</div>
								</div>
							</div>

						<?php endwhile; ?>

					</div>

				<?php } ?>

			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




