<?php
/*
Template Name: FAGJ - Informations V2
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container page_fagj">

		<?php include('templatechunk-fagj-nav.php'); ?>

		<main class="clearfix container">

			<section class="entete_page">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?= get_field('titre_affiche'); ?></h1>
					</header>
				</article>
				<?= get_field('texte_intro'); ?>
			</section>

			<section class="content_page">

				<div class="bloc_haut shadowed_box">
					<?= get_field('texte_haut_page') ?>
					<?php $texte_bas = get_field('texte_bas_de_page') ?>
				</div>

				<?php
				$args = array(
					'post_type'      => 'page-fagj',
					'posts_per_page' => -1,
					'post_parent'    => get_the_ID(),
					'order'          => 'ASC',
					'orderby'        => 'menu_order'
				);
				
				$pages_fagj = new WP_Query( $args );

				if ( $pages_fagj->have_posts() ) :
					while ( $pages_fagj->have_posts() ) : $pages_fagj->the_post(); 
						$titre = get_field('titre_fagj');
						?>

						<div class="bloc_fagj shadowed_box">
							<div class="row no-gutters">
								<div class="main_content col-lg-8">
									<a href="<?= get_the_permalink() ?>" class="titre_container">
										<h3 class="titre"><?= $titre ?></h3>
									</a>
									<div class="texte_container">
										<?= get_field('resume_page_infos') ?>
									</div>
									<div class="footer">
										<a href="<?= get_the_permalink() ?>" class="lien_souspage bouton_type_1">Tous les détails de cette FAGJ</a>
									</div>
								</div>
								<div class="sidebar col-lg-4">
									<h3 class="titre">
										<div class="triangle"></div>
										En bref
									</h3>
									<div class="sidebar_content">
									<div class="widget contacts_medias liste_fichiers">
											<h4 class="sidebar_title">CONTACTS / MÉDIAS</h4>
											<?php if(have_rows('liste_contacts')) : 
												while(have_rows('liste_contacts')) : the_row(); 
													$type = get_sub_field('type');
													$titre = get_sub_field('titre');
													$lien = get_sub_field('lien');

													switch ($type) {
														case 'facebook': $icon = '<i class="fab fa-facebook-square"></i>'; break;
														case 'youtube': $icon = '<i class="fab fa-youtube-square"></i>'; break;
														case 'twitch': $icon = '<i class="fab fa-twitch"></i>'; break;
														case 'email': $icon = '<i class="fas fa-envelope-square"></i>'; break;
														case 'site_web': $icon = '<i class="fas fa-window-maximize"></i>'; break;
														default: $icon = '<i class="fas fa-arrow-alt-circle-right"></i>'; break;
													}
													?>
													<a class="ligne" href="<?= $lien ?>" target="_blank">
														<div class="icon_container">
															<?= $icon ?>
														</div>
														<div class="label">
															<p class="titre"><?= $titre ?></p>
														</div>
													</a>
												<?php endwhile; ?>
											<?php else: ?>
												<div class="no_entry">
													<p>Aucuns contacts ou médias n'ont été renseignés pour cette FAGJ...</p>
													<p>Si vous avez des informations, n'hésitez pas à nous les transmettre !</p>
												</div>
											<?php endif; ?>
										</div>
										<div class="widget syntheses liste_fichiers">
											<h4 class="sidebar_title">DOCUMENTS / SYNTHÈSES</h4>
											<?php if(have_rows('liste_syntheses')) : 
												while(have_rows('liste_syntheses')) : the_row(); 
													$titre = get_sub_field('titre');
													$soustitre = get_sub_field('soustitre');
													$fichier = get_sub_field('fichier');
													?>
													<a class="ligne" href="<?= $fichier ?>" target="_blank" rel="noopener nofollow noreferrer">
														<div class="icon_container">
															<i class="far fa-file-alt"></i>
														</div>
														<div class="label">
															<p class="titre"><?= $titre ?></p>
															<p class="soustitre"><?= $soustitre ?></p>
														</div>
													</a>
												<?php endwhile; ?>
											<?php else: ?>
												<div class="no_entry">
													<p>Aucuns documents ou synthèses n'ont été renseignés pour cette FAGJ...</p>
													<p>Si vous avez des informations, n'hésitez pas à nous les transmettre !</p>
												</div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>

					<?php endwhile;
				endif; ?>

				<div class="bloc_bas shadowed_box">
					<?= $texte_bas ?>
				</div>

			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




