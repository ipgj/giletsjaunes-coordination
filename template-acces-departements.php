<?php
/*
Template Name: Accès départements (carte seule)
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

get_header(); ?>

	<div class="main_content_container">

		<div class="background_haut_page">
			<img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/fond.jpg"/>
		</div>

		<div class="container">
		
			<div class="entete_page_new shadowed_box">
				<h1><?= get_field('titre_affiche'); ?></h1>
				<div class="texte">
					<?= get_field('texte_intro'); ?>
				</div>
			</div>

			<main class="clearfix">

				<!-- <xsl:stylesheet -->
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
				<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.vmap.js" type="text/javascript"></script>
			    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.vmap.france.js" type="text/javascript"></script>
				<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.vmap.colorsFranceGJ.js" type="text/javascript"></script>

				<div id="carte-departements" class="francemap_container">
					<div class="francemap_title">SECTIONS DÉPARTEMENTALES</div>
					<div id="francemap"></div>
					<div class="francemap_footer">
						<div class="phrase">La carte ne fonctionne pas chez vous ? accédez à la liste textuelle des sections départementales.</div>
						<a class="bouton_type_1" href="<?= get_permalink(684).'#liste_textuelle'; ?>">Liste départements</a>
					</div>
				</div>

				<div id="liste_textuelle" class="francemap_container mt-5">
					<div class="francemap_title">SECTIONS DÉPARTEMENTALES - LISTE TEXTUELLE</div>
					<div class="liste_departements row">
						<?php $i = 0;
						if(have_rows('liens_departements_forums', 'options')) : 
							while(have_rows('liens_departements_forums', 'options')) : the_row(); 
								$i++;

								$page_dep = get_sub_field('page_principale_departementale');
								if($page_dep != '') {
									$titre = get_the_title($page_dep);
								} else {
									$titre = '';
								}
								
								$lien = get_the_permalink($page_dep);
								?>

								<?php 
								if($i%35 == 1) { echo '<div class="col-lg-4">'; } ?>
								<a href="<?= $lien ?>" class="ligne">
									<h4><?= $titre ?></h4>
								</a><br/>
								<?php if($i%35 == 0) { echo '</div>'; } ?>

							<?php endwhile; 
							echo '</div>';
						endif;
						?>
					</div>
					<!-- <div class="francemap_footer">
						<div class="phrase">Retour à la carte de France.</div>
						<a href="/#carte-departements" class="bouton_type_1">Page d'accueil</a>
					</div> -->
				</div>
			</main>

		</div> <!-- Fin container -->

	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




