<?php
/*
Template Name: Redirect to target
*/

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Gilet Jaune France
 */

wp_redirect(get_permalink(get_field('redirect_targetID')));
exit;

get_header(); ?>

	<div class="main_content_container">
		<main class="clearfix">

			<section class="entete_page">

				<?php
				$args = array(
				    'post_type'      => 'any',
				    'posts_per_page' => -1,
				    'post_parent'    => get_the_ID(),
				    'order'          => 'ASC',
				    'orderby'        => 'menu_order'
				 );

				$souspages = new WP_Query( $args );
				$current_page_ID = get_the_ID();

				if ( $souspages->have_posts() ) :
				    while ( $souspages->have_posts() ) : $souspages->the_post();
				    	$first_child_id = get_the_ID();
				    	break;
					endwhile;
				endif; wp_reset_postdata(); 

				?>

			</section>

		</main>
	</div>

<?php /*if ( is_active_sidebar('sidebar-why-spanninga') ) {
	dynamic_sidebar('sidebar-why-spanninga');
}*/ ?>

<?php get_footer(); ?>




