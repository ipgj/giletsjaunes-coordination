<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gilet_Jaune_France
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}

if(!empty($_POST['delete_comment_submit'])) {
	if(!empty($_POST['comment_id_to_delete'])) {

		$comment = get_comment($_POST['comment_id_to_delete']);
		$author_id = $comment->user_id;
		echo $author_id;

		if(get_current_user_id() == $author_id) {
			wp_delete_comment($_POST['comment_id_to_delete'], false);
			wp_redirect(the_permalink().'?delete_comment=true');
			exit;
		} else {
			wp_redirect(the_permalink().'?delete_comment=forbidden');
			exit;
		}
		
	}
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="titre_section">
			<?php $comments_number = get_comments_number();
			echo '<i class="fas fa-comments"></i> '.$comments_number; ?> commentaire<?php if($comments_number > 1) { echo 's'; } ?>
		</h2><!-- .comments-title -->

		<?php if($_GET['delete_comment'] != '') { ?>
			<div class="comment_infobox">
				<?php
				if($_GET['delete_comment'] == 'true') { echo 'Votre commentaire a bien été supprimé.'; }
				if($_GET['delete_comment'] == 'forbidden') { echo 'Vous n\'êtes pas autorisé à supprimer ce commentaire.'; }
				?>
			</div>
		<?php } ?>

		<?php the_comments_navigation(); ?>

		<ul class="comment-list">
			<?php
			function mytheme_comment($comment, $args, $depth) {

			    if ( 'div' === $args['style'] ) {
			        $tag       = 'div';
			        $add_below = 'comment';
			    } else {
			        $tag       = 'li';
			        $add_below = 'div-comment';
			    }

			    $author_id = $comment->user_id;
			    $author_main_role = get_user_main_role($author_id);
			    $author_departement = get_user_departement($author_id);
			    $author_role_name = get_user_role_name($author_id);
				if($author_main_role != 'subscriber') {
					$role_displayed = ' <span class="role">['.$author_role_name.']</span>';
				} else {
					$role_displayed = '';
				}
			    ?>

			    <<?php echo $tag; ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID() ?>">

			    <?php if ( 'div' != $args['style'] ) { ?><div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php } ?>

			        <div class="haut">
				        <div class="gauche">
				        	<?php  if ( $args['avatar_size'] != 0 ) {
				                echo get_avatar( $comment, $args['avatar_size'] ); 
				            } ?>
				        </div>

				        <div class="droite">
				        	<div class="nom">
				        		<?php printf( __( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); ?>
				        		<?php echo $role_displayed; ?>
				        		<?php if($user_departement != 'Aucun') { ?>
				        			<span class="departement">(<?php echo $author_departement; ?>)</span>
				        		<?php } ?>
				        		
				        	</div>
				        	<div class="date"><?php printf( __('%1$s at %2$s'), get_comment_date(), get_comment_time() ); ?></div>
				        </div>

				        <div class="comment_link_container">
				        	<a class="comment_link js-scrollTo" href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">Lien du commentaire</a>
				        </div>
				    </div>

				    <div class="bas">
				        <?php if ( $comment->comment_approved == '0' ) { ?>
				            <div class="comment-awaiting-moderation"><?php _e( 'Votre commentaire est en attente de modération.' ); ?></div>
				        <?php } ?>
				        <?php if (get_current_user_id() == $author_id AND 1 == 2) { ?>
				            <div class="supprimer_commentaire">
				            	<form method="post" id="delete_comment" action="<?php the_permalink(); ?>">
								    <input name="delete_comment_submit" type="submit" id="updateuser" class="submit" value="<?php _e('Supprimer mon commentaire.', 'textdomain'); ?>" />
								    <input type="hidden" name="comment_id_to_delete" value="<?php echo $comment->comment_ID; ?>">
								</form>
				            </div>
				        <?php } ?>
				        <div class="texte_commentaire">
				        	<?php comment_text(); ?>
				        </div>
				    </div>

<?php echo edit_comment_link( __( 'Edit Comment', 'textdomain' ), '<p>', '</p>' ); ?>


			        

			        <div class="reply">
			        	<i class="far fa-comment"></i>
			        	<?php comment_reply_link( 
			                    array_merge( 
			                        $args, 
			                        array( 
			                            'add_below' => $add_below, 
			                            'depth'     => $depth, 
			                            'max_depth' => $args['max_depth'] 
			                        ) 
			                    ) 
			                ); ?>
			        </div>
			        <?php if ( 'div' != $args['style'] ) { ?></div>
			        <?php }
			} ?>

			<?php wp_list_comments( 'type=comment&callback=mytheme_comment' ); ?>

		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Les commentaires sont fermés.', 'gilet-jaune-france' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	$args = array(
		'id_form'           => 'commentform',
		'class_form'      => 'comment-form',
		'id_submit'         => 'submit',
		'class_submit'      => 'submit bouton_type_1',
		'name_submit'       => 'submit',
		'title_reply'       => __( 'Leave a Reply' ),
		'title_reply_to'    => __( 'Leave a Reply to %s' ),
		'cancel_reply_link' => __( '<i class="far fa-times-circle"></i> Annuler la réponse' ),
		'label_submit'      => __( 'Post Comment' ),
		'format'            => 'xhtml',

		'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( 'Commentaire', 'noun' ) .
		'</label><textarea id="comment" name="comment" cols="45" rows="10" aria-required="true" placeholder="Votre commentaire...">' .
		'</textarea></p>',

		'must_log_in' => '<p class="must-log-in">' .
		sprintf(
		  __( 'Vous devez être <a href="%s">connecté</a> pour poster un commentaire.' ),
		  wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
		) . '</p>',

		'logged_in_as' => '<strong>En tant que :</strong> <a href="'.get_permalink(270).'" class="nom">'.$user_identity.'</a> (<span>'.get_current_user_departement().'</span>)',

		'comment_notes_before' => '<p class="comment-notes">' .
		__( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) .
		'</p>',

		'comment_notes_after' => '<p class="disclaimer">'.get_field('disclaimer_bienseance', 'option').'</p>',

		'fields' => apply_filters( 'comment_form_default_fields', $fields ),
	);
	comment_form( $args );

	?>

</div><!-- #comments -->
