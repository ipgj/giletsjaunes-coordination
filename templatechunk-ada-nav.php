
		<nav class="nav_departement_container">

			<div class="container">
				<div class="flex_container">

					<div class="nom_departement">
						<div class="left_filler"></div>
						<a href="<?= get_permalink(103932) ?>">ESPACE ADA</a>
					</div>

					<div class="separateur_departement"></div>

					<div class="bouton_menu_responsive">
						<div>Menu</div> <i class="fas fa-caret-down"></i>
					</div>

					<div class="nav_departement">

						<?php 
						wp_reset_postdata();
						
						$args = array(
						    'post_type'      => 'page-ada',
							'posts_per_page' => 10,
							'post__not_in' 	 => array(103932),
							'post_parent'    => 103932,
						    'order'          => 'ASC',
							'orderby'        => 'menu_order',
							'paged' => $paged,
						 );

						$actus_ada = new WP_Query( $args );

						if ( $actus_ada->have_posts() ) :
							while ( $actus_ada->have_posts() ) : $actus_ada->the_post(); 
							
								$page_link_ID = get_the_ID(); 
								if(!empty($post->post_password)){
									$password_protected_child_ID = $page_link_ID;
								}

								$pages_nav_dep[] = [
									'nom' => get_the_title(), 
									'lien' => get_the_permalink()
								];

								$current = '';
								if($page_link_ID == $current_page_ID) { $current = 'active'; } ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="<?php echo $current; ?>"><?php the_title(); ?></a>
								
							
						    <?php endwhile;
						endif; wp_reset_postdata(); ?>

						

					</div>

				</div>
			</div>

		</nav>